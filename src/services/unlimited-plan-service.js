import API from './api';
const customerUrl = localStorage.getItem('customerCode');
async function getUnlimitedPlans(accessToken, locationId) {
    try {
        // c0446134-b1fc-40a2-8a89-e2c7e14ac5fd
        return await API.get(`VIPAccountType/GetUnlimitedTypesForLocation?locationId=${locationId}`, {
            headers: {
                'X-Authentication-Token': accessToken,
                'X-Customer-Code': customerUrl
            }
        });
    } catch (error) {
        console.clear;
        return error;
    }
};

async function getCustomerProRateAmountMobile(accessToken, param) {
    try {
        return await API.get(`Customers/GetCustomerProRateAmountMobile?MonthlyAccountTypeId=${param.MonthlyAccountTypeId}&NewMonthlyAccountTypeId=${param.NewMonthlyAccountTypeId}&CustomerGuid=${param.CustomerGuid}&ConfigurationUniqId=${param.ConfigurationUniqId}`, {
            headers: {
                'X-Authentication-Token': accessToken,
                'X-Customer-Code': customerUrl
            }
        });
    } catch (error) {
        console.clear;
        return error;
    }
};

async function addNewUWCPlan(accessToken, reqBody) {
    try {
        return await API.post(`Customers/ChargeUnlimitedPlanWeb`, reqBody, {
            headers: {
                'X-Authentication-Token': accessToken,
                'X-Customer-Code': customerUrl
            }
        });
    } catch (error) {
        console.clear;
        return error;
    }
};

async function upgradeUWCPlan(accessToken, reqBody) {
    try {
        return await API.post(`Customers/CustomerPlanChangeMobile`, reqBody, {
            headers: {
                'X-Authentication-Token': accessToken,
                'X-Customer-Code': customerUrl
            }
        });
    } catch (error) {
        console.clear;
        return error;
    }
};

export {getUnlimitedPlans, getCustomerProRateAmountMobile, addNewUWCPlan, upgradeUWCPlan};
