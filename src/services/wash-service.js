import API from './api';

export const washServices = (accessToken, locationId) => {
    return API.get(`PrepaidBookTypes/GetTypesForMobile/${locationId}`, {headers: {
        'X-Authentication-Token': accessToken,
        'locationId': locationId
    }}).catch((error) => {
        console.clear();
        return error;
    });
};
export const buyPrepaidBook = (accessToken, locationId, reqBody) => {
    return API.post(`PrepaidBooks/BuyPrepaidBookForWeb/?locationid=${locationId}`, reqBody, {
        headers: {
            'X-Authentication-Token': accessToken
        }}).catch((error) => {
        console.clear();
        return error;
    });
};

