import API from './api';
const cancelMembershipList = (accessToken) => {
    return API.get('CustomerCancelReasons', {
        headers: {
            'X-Authentication-Token': accessToken
        }
    }).catch((error) => {
        console.clear();
        return error;
    });
};
const customerUrl = localStorage.getItem('customerCode');
const cancelMemberShip = async (accessToken, postData) => {
    return API.post('Customers/DeactivateCustomerAccountMobile', postData, {
        headers: {
            'X-Authentication-Token': accessToken,
            'X-Customer-Code': customerUrl
        }
    }).catch((error) => {
        console.clear();
        return error;
    });
};
export {cancelMembershipList, cancelMemberShip};
