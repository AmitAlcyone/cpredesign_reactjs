import API from './api';

async function getCustomerInfoByGuid(accessToken, Guid) {
    try {
        return await API.get(`Customers/GetCustomerByGuid/${Guid}`, {
            headers: {
                'X-Authentication-Token': accessToken
            }
        });
    } catch (error) {
        console.clear;
        return error;
    }
};

async function putCustomerInfoByGuid(accessToken, data) {
    try {
        return await API.post(`Customers/UpdateCustomerProfile`, data, {
            headers: {
                'X-Authentication-Token': accessToken
            }
        });
    } catch (error) {
        console.clear;
        return error;
    }
};
export {getCustomerInfoByGuid, putCustomerInfoByGuid};
