import API from './api';
const customerUrl = localStorage.getItem('customerCode');
export const getPromotionAvailable = (accessToken) => {
    return API.get(`Customers/GetPromotionAvailable?id=1`, {
        headers: {
            'X-Authentication-Token': accessToken,
            'X-Customer-Code': customerUrl
        }
    }).catch((error) => {
        console.clear();
        return error;
    });
};

export const saveCreditCard = async (accessToken, reqBody) => {
    try {
        return await API.post(`/CustomerIdentityCardOnFiles/AddPCICard`, reqBody, {
            headers: {
                'X-Authentication-Token': accessToken,
                'X-Customer-Code': customerUrl
            }
        });
    } catch (error) {
        console.clear();
        return error;
    }
};

export const getPCIDetails = async () => {
    try {
        return await API.get(`/MobilePCI/GetPCIDetail`, {
            headers: {
                'X-Customer-Code': customerUrl
            }
        });
    } catch (error) {
        console.clear();
        return error;
    }
};
