import API from '../api';
// import {toast} from 'react-toastify';

export const giftCardList = (customerCode) => {
    return API.get(`/QuickBuy/GetGiftDenominationChoices`, {headers: {
        'X-Customer-Code': customerCode
    }}).catch((error) => {
        console.clear();
        return error;
    });
};

export const buyGiftCard = async (customerCode, locationId, reqBody) => {
    try {
        return await API.post('/QuickBuy/QuickBuyGiftCard', reqBody, {
            headers: {
                'X-Customer-Code': customerCode,
                'X-Location-Id': locationId
            }
        });
    } catch (error) {
        console.log(error);
        // debugger;
        // if (error.response) {
        //     toast.error(error.response.data.message);
        // }
        return error;
    }
};
