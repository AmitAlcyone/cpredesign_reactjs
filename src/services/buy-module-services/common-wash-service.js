import API from './../api';

export const getCommonWashPageServices = async (customerCode, locationId, pageName) => {
    let url= '';
    if (pageName== 'wash_service') {
        url= 'QuickBuy/GetWashPlans';
    } else if (pageName== 'detail_service') {
        url= 'QuickBuy/GetDetailServices';
    } else if (pageName== 'prepaid_service') {
        url= 'QuickBuy/GetWashServices';
    }

    try {
        return await API.get(url, {
            headers: {
                'X-Customer-Code': customerCode,
                'X-Location-Id': locationId
            }
        });
    } catch (error) {
        console.clear();
        return error;
    }
};

export const buyCommonWashPageServices = async (customerCode, locationId, reqBody) => {
    try {
        return await API.post('QuickBuy/QuickBuyPrepaidBookWashService', reqBody, {
            headers: {
                'X-Customer-Code': customerCode,
                'X-Location-Id': locationId
            }
        });
    } catch (error) {
        console.log(error.response);
        // debugger;
        // if (error.response) {
        //     toast.error(error.response.data.message);
        // }
        return error;
    }
};
