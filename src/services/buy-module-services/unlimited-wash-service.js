import API from '../api';

export const unlimitedWashServices = (customerCode) => {
    return API.get('QuickBuy/GetUnlimitedPlans', {headers: {
        'X-Customer-Code': customerCode,
        'locationId': ''
    }}).catch((error) => {
        console.clear();
        // console.log(error.response);
        return error;
    });
};
export async function buyUnlimitedPlanQuickBuy(customerCode, locationId, reqBody) {
    try {
        return await API.post(`QuickBuy/QuickBuyCustomerUnlimitedPlan`, reqBody, {
            headers: {
                'X-Customer-Code': customerCode,
                'X-Location-Id': locationId
            }
        });
    } catch (error) {
        console.clear;
        // console.log(error.response);
        return error;
    }
};
