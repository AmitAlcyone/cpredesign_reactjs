import API from './api';
// GC is a alias of GiftCard

export const giftCardList = (accessToken, GUID) => {
    return API.get(`GiftCards/GetByCustomerGUID/${GUID}`, {headers: {
        'X-Authentication-Token': accessToken
    }}).catch((error) => {
        console.clear();
        return error;
    });
};
const customerUrl = localStorage.getItem('customerCode');
export const giftCardAmountList = (accessToken) => {
    return API.get('GiftCards/GetGiftCardList', {headers: {
        'X-Authentication-Token': accessToken,
        'X-Customer-Code': customerUrl
    }}).catch((error) => {
        console.clear();
        return error;
    });
};

export const mergeGiftCard = (accessToken, reqBody, customerCode) => {
    return API.post('GiftCards/MergeGiftcardMobile', reqBody, {headers: {
        'X-Authentication-Token': accessToken,
        'Content-Type': 'application/json',
        'X-Customer-Code': customerCode
    }}).catch((error) => {
        console.clear();
        return error;
    });
};
const addValueOnExistingGC = async (accessToken, location, reqBody) => {
    try {
        return await API.post('GiftCards/AddGiftCardFundsWeb', reqBody, {
            headers: {
                'X-Authentication-Token': accessToken,
                'X-Customer-Code': customerUrl,
                'X-Location-Id': location
            }
        });
    } catch (error) {
        console.clear();
        // console.log(error.response);
        return error;
    }
};

const getCalculatedGCTax = async (accessToken, reqBody) => {
    try {
        const url = `GiftCards/CalculateGiftCardTax?SurchargeId=${reqBody.SurchargeId}&Other=${reqBody.Other}&CustomerGuid=${reqBody.CustomerGuid}`;
        return await API.get(url, {
            headers: {
                'X-Authentication-Token': accessToken,
                'X-Customer-Code': customerUrl
            }
        });
    } catch (error) {
        console.clear();
        return error;
    }
};

const buyGiftCardOnline= async (accessToken, reqBody) => {
    try {
        return await API.post('GiftCards/BuyGiftCardOnlineWeb', reqBody, {
            headers: {
                'X-Authentication-Token': accessToken
            }
        });
    } catch (error) {
        console.clear();
        // console.log(error.response);
        return error;
    }
};

export {addValueOnExistingGC, getCalculatedGCTax, buyGiftCardOnline};
