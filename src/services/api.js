import axios from 'axios';
import {config} from '../utils/config/config';

export default axios.create({
    baseURL: config.apiUrl
});
