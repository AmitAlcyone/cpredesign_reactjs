import API from './api';
const customerUrl = localStorage.getItem('customerCode');
export const creditCardsList = (accessToken, id) => {
    return API.get(`CustomerIdentityCardOnFiles?id=${id}`, {headers: {
        'X-Authentication-Token': accessToken
    }}).catch((error) => {
        console.clear();
        return error;
    });
};

export const deleteCreditCard = async (accessToken, id) => {
    try {
        return await API.post('CustomerIdentityCardOnFiles/DeleteCard?CardId=' + id, null, {
            headers: {
                'X-Authentication-Token': accessToken
            }
        });
    } catch (error) {
        console.clear();
        return error;
    }
};

export const cancelMembershipList = (accessToken) => {
    return API.get('CustomerCancelReasons', {
        headers: {
            'X-Authentication-Token': accessToken
        }
    }).catch((error) => {
        console.clear();
        return error;
    });
};

export const updateCreditCardInfo = async (accessToken, reqBody) => {
    try {
        return await API.post('Customers/UpdateCreditCardInfoMobile', reqBody, {
            headers: {
                'X-Authentication-Token': accessToken,
                'X-Customer-Code': customerUrl
            }
        });
    } catch (error) {
        console.clear();
        return error;
    }
};
