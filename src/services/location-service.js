import API from './api';

export const location = (accessToken) => {
    return API.get(`Locations/LocationsForMobile`, {
        headers: {
            'X-Authentication-Token': accessToken
        }
    }).catch((error) => {
        console.clear();
        return error;
    });
};

export const locationDetail = (accessToken, reqBody) => {
    return API.post(`Locations/LocationDetailById`, reqBody, {
        headers: {
            'X-Authentication-Token': accessToken
        }
    }).catch((error) => {
        console.clear();
        return error;
    });
};

export const nearByLocation = (accessToken, reqBody) => {
    return API.post('Locations/SetNearByLocation', reqBody, {
        headers: {
            'X-Authentication-Token': accessToken
        }
    }).catch((error) => {
        console.clear();
        return error;
    });
};
export const getLocationsForQuickBuy = (customerCode) => {
    return API.get('QuickBuy/getLocationsForQuickBuy', {
        headers: {
            'X-Customer-Code': customerCode
        }
    }).catch((error) => {
        console.clear();
        return error;
    });
};

export const verifyZipcode = (zipcode) => {
    return API.get(`https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAW9jj6HnCRWDP6Xb9I8Pf2Mk8c0-LWgj4&components=postal_code:${zipcode}`, {
    }).catch((error) => {
        console.clear();
        return error;
    });
};
