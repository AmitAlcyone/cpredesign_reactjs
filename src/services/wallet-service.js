import API from './api';

// export const wallet = (accessToken, identityId) => {
//     return API.get(`PrepaidBooks/GetCustomerWalletMobile/?customerIdentityId=${identityId}`, {headers: {
//         'X-Authentication-Token': accessToken
//     }}).catch((error) => {
//         console.clear();
//         return error;
//     });
// };

async function wallet(accessToken, identityId) {
    try {
        return await API.get(`PrepaidBooks/GetCustomerWalletMobile/?customerIdentityId=${identityId}`, {
            headers: {
                'X-Authentication-Token': accessToken
            }
        });
    } catch (error) {
        console.clear;
        return error;
    }
};

async function sendToDevic(accessToken, data) {
    try {
        return await API.post(`Wallet/SendToDevice`, data, {
            headers: {
                'X-Authentication-Token': accessToken
            }
        });
    } catch (error) {
        console.clear;
        return error;
    }
};

async function walletBarcodeDetail(walletId) {
    try {
        const postModel = {value: walletId};
        return await API.post(`Wallet/WalletDetail`, postModel, {
        });
    } catch (error) {
        console.clear;
        console.log(error.response);
        return error;
    }
};

export {wallet, sendToDevic, walletBarcodeDetail};

