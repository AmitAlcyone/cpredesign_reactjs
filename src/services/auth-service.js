import API from './api';
const localStorageCustomerCode = localStorage.getItem('customerCode');
const headers = {
    'Content-Type': 'application/json',
    'X-Customer-Code': localStorageCustomerCode,
    'X-Location-Id': ''
};

export const loginApi = (phoneNumber) => {
    return API.get(`CustomerIdentities/LoginByPhone/?phone=${phoneNumber}`, {headers: {
        'Content-Type': 'application/json',
        'X-Customer-Code': localStorageCustomerCode,
        'X-Location-Id': ''
    }}).catch((error) => {
        console.clear();
        return error;
    });
};

export const otpVerification = (phoneNumber, otp) => {
    return API.get(`CustomerIdentities/LoginByPhone/ValidateToken/?phone=${phoneNumber}&otp=${otp}`, {headers}).catch((error) => {
        console.clear();
        return error;
    });
};

export const signupApi = (data) => {
    return API.post('CustomerIdentities', data, {headers: {
        'Content-Type': 'application/json',
        'X-Customer-Code': localStorageCustomerCode,
        'X-Location-Id': ''
    }}).catch((error) => {
        console.clear();
        return error;
    });
};
export const updateRegisterApi = (data) => {
    return API.post('CustomerIdentities/UpdateRegister', data, {headers}).catch((error) => {
        console.clear();
        return error;
    });
};
export const loginByPhoneV2Api = (phoneNumber) => {
    return API.get(`CustomerIdentities/LoginByPhoneV2/?phone=${phoneNumber}`, {headers}).catch((error) => {
        console.clear();
        return error;
    });
};

export const customizationApi = (customerCode) => {
    return API.get('PortalHome/GetPortalCustomizations', {
        headers: {
            'X-Customer-Code': customerCode,
            'X-Location-Id': ''
        }
    }).catch((error) => {
        console.clear();
        return error;
    });
};
export const barcodeApi = (reqBody) => {
    return API.get(`Login/CustomerPortalLogin/ValidateCustomerBybarcodeCCNumberWeb/${reqBody.Barcode}/${reqBody.CCNumber}`, {
        headers: {
            'X-Customer-Code': localStorageCustomerCode
        }}).catch((error) => {
        console.clear();
        return error;
    });
};
export const customerUrlApi = (customerCode) => {
    return API.get(`Portal/GetCustomerCode/?CustomerPortalUrl=${customerCode}`, {
        headers: {
            'CustomerPortalUrl': customerCode
        }
    }).catch((error) => {
        console.clear();
        return error;
    });
};

export const policyData = (customerCode) => {
    return API.get('PortalHome/GetPortalAboutUs', {
        headers: {
            'X-Customer-Code': customerCode
        }
    }).catch((error) => {
        console.clear();
        return error;
    });
};
export const checkoutJson = () => {
    return {
        TopHeaderText: '',
        ListItemText: '',
        ListItemDescription: '',
        IsCardSelected: false,
        IsNewCard: false,
        SelectedCard: {},
        IsCheckedTermAndCondition: false,
        IsShowPromocodeDiv: false,
        IsPromocodeApplied: false,
        IsRecieveSMSReciept: false,
        IsRecieveEmailReciept: false,
        IsSavePaymentInfoForFuturePurchase: false,
        IsUWCRequest: false,
        IsUpgradeULPlan: false, // UL for Unlimited plans
        termsId: 0,
        ULPlanData: {
            CurrentPlan: '',
            CurrentRecurringPayment: 0,
            NewSelectedPlan: '',
            NewRecuringPayment: ''
        },
        Promocode: '',
        TaxAmount: 0.0,
        DiscountAmount: 0.0,
        SubTotal: 0.0,
        GrandTotal: 0.0,
        Email: '',
        AdditionalPageData: null
    };
};
