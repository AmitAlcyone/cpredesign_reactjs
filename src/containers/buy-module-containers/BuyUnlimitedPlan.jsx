import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import LayoutBuy from '../../components/LayoutBuy';
import ServiceCardLoader from '../../utils/loaders/serviceCardLoader';
import {setServices} from '../../store/reducers/prepaid-wash-reducer';
import {useDispatch} from 'react-redux';
import {unlimitedWashServices, buyUnlimitedPlanQuickBuy} from '../../services/buy-module-services/unlimited-wash-service';
// import {buyPrepaidBook} from '../../services/wash-service';
import {useSelector} from 'react-redux';
import {toast} from 'react-toastify';
import {setSelectedPlan} from '../../store/reducers/order-reducer';
import {setBarcodeString} from '../../store/reducers/global-reducer';
import Checkout from '../../components/buy-module-components/checkout/checkout';
import CheckoutSuccess from '../../components/buy-module-components/checkout/checkout-success';
import {getPCIDetailsLocal, getNoLoginCheckoutJSON} from '../../utils/common-repo';

const BuyUnlimitedPlan = (props) => {
    toast.configure();
    const toastId = React.useRef(null);
    // const navigate = useNavigate();
    const dispatch = useDispatch();
    const services = useSelector((state) => state.prepaidWash.services);
    const selectedPlan= useSelector((state) => state.order.selectedPlan);
    const [productTypeId, setProductTypeId] = useState(selectedPlan!=null ? selectedPlan.productTypeId : -1);
    const [isLoading, setIsLoading] = useState(false);
    const orderJson= useState(getNoLoginCheckoutJSON());
    const [isCheckoutShow, setIsCheckoutShow] = useState(false);
    const [isBuyNow, setIsBuyNow] = useState(false);
    const customerCode = localStorage.getItem('customerCode');
    const [viewCheckoutSuccess, setViewCheckoutSuccess] = useState(false);
    const buylocationId = localStorage.getItem('buyLocationId');

    const PCIScript= getPCIDetailsLocal().PCIScript;
    useEffect(() => {
        const getServies = async () => {
            if (services.length === 0 || services.length == undefined) {
                setIsLoading(true);
                const response = await unlimitedWashServices(customerCode);
                if (response.status === 200) {
                    setIsLoading(false);
                    dispatch(setServices({services: response.data.products}));
                }
            }
        };
        getServies();
        const script = document.createElement('script');
        script.src = PCIScript;
        script.async = false;
        document.body.appendChild(script);
        return () => {
            document.body.removeChild(script);
        };
    }, []);

    const innerServicesList = (data) => {
        if (data) {
            const serviceArray = data.serviceList !== null && data.serviceList.length > 0 ? data.serviceList : [];
            return (
                <ul className='wc_service_list scroller'>
                    {serviceArray.map((value, index) => {
                        return <li key={index}>{value}</li>;
                    })}
                </ul>
            );
        }
    };

    const buyNow = (service) => {
        setIsBuyNow(true);
        orderJson[0].TopHeaderText= 'Buy Unlimited Wash Plan';
        orderJson[0].SubTotal= service.price;
        orderJson[0].ListItemText= 'Unlimited plan';
        orderJson[0].ListItemDescription= service.description;
        dispatch(setSelectedPlan({selectedPlan: service}));
        setProductTypeId(service.productTypeId);
    };

    const continueClick = async () => {
        orderJson[0].GrandTotal= parseFloat(orderJson[0].SubTotal).toFixed(2);
        orderJson[0].IsShowRecieveEmailReciept= false;
        orderJson[0].IsShowRecieveSMSReciept= false;
        orderJson[0].TermsId = 5;
        setIsCheckoutShow(true);
    };

    const buyUnlimitedPlanCheckout = async (order, ccDetails) => {
        if (order.IsCheckedTermAndCondition) {
            const postModel= {};
            postModel.productTypeId= selectedPlan.productTypeId;
            postModel.phoneNumber= ccDetails.phoneNumber;
            // eslint-disable-next-line camelcase
            postModel.temporary_token= ccDetails.temporary_token;
            postModel.unixTime= ccDetails.unixTimeStamp;
            postModel.amount= selectedPlan.price;
            postModel.creditCardNumber= '';
            postModel.cardHolderName= '';
            postModel.expiryDate= '';
            postModel.alias= '';
            postModel.LocationId = buylocationId;
            setIsBuyNow(true);
            const response = await buyUnlimitedPlanQuickBuy(customerCode, '', postModel);
            console.log(response);
            // console.log(response.response);
            setIsBuyNow(false);
            if (response.status === 200) {
                dispatch(setBarcodeString({barcodeString: response.data[0].sBarcode}));
                setViewCheckoutSuccess(true);
                setIsCheckoutShow(false);
                // return navigate('/order-success');
            } else {
                toast.error('Something went wrong.', {autoClose: 3000});
                return false;
            }
        } else if (!order.IsCheckedTermAndCondition) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please check term and condition.');
            }
            return false;
        } else if (!order.IsCardSelected) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please select credit card before checkout.');
            }
            return false;
        }
    };

    return (
        <LayoutBuy>
            {viewCheckoutSuccess ? <CheckoutSuccess Annomous={true} /> : isCheckoutShow ? <Checkout processCheckout={(o, cc) => buyUnlimitedPlanCheckout(o, cc)} hideCheckoutPage={() => {
                document.body.scrollTop = 0; // For Safari
                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                setIsCheckoutShow(false);
            }} order={orderJson} /> :
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="d-flex mb-4 mt-3 justify-content-between align-items-center flex-wrap">
                                <div className="inner_title d-flex align-items-start">
                                    <div className="progress-circle over50 p50">
                                        <span>1 of 2</span>
                                        <div className="left-half-clipper">
                                            <div className="first50-bar"></div>
                                            <div className="value-bar"></div>
                                        </div>
                                    </div>
                                    <div className="main_title">
                                        <h2>Buy a Unlimited Wash Service</h2>
                                        <p>Select a Wash Service</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12" style={{borderTop: '1px solid #e5e5e5'}}>
                            {isLoading ?
                                <div className='row wash_list'>
                                    <ServiceCardLoader></ServiceCardLoader>
                                    <ServiceCardLoader></ServiceCardLoader>
                                    <ServiceCardLoader></ServiceCardLoader>
                                </div> : ''
                            }
                            <div className="row wash_list">
                                {services.length > 0 && services.map((service, index) => {
                                    if (service.price !== 0) {
                                        return (
                                            <div className='col-md-4 col-sm-6 col-12 ' key={index}>
                                                <div className={`wc_box ${productTypeId === service.productTypeId ? 'wc_selected' : ''}`}>
                                                    <div className='wc_info'>
                                                        <h3>{service.description}</h3>
                                                        <span>${service.price.toFixed(2)} for a single Extreme wash</span>
                                                        {service.serviceList !== null && innerServicesList(service)}
                                                    </div>
                                                    <div className='wc_foot'>
                                                        <span className='wc_prices'>${service.price.toFixed(2)}</span>
                                                        <Link to='#' onClick={() => buyNow(service)}>{productTypeId === service.productTypeId ? 'Added!' : 'Buy Now'}</Link>
                                                    </div>
                                                </div>
                                            </div>
                                        );
                                    }
                                })}
                                {services.length === 0 && isLoading === false ? <p className='notFound'>Services not available.</p> : ''}
                            </div>
                            <div className='d-flex justify-content-end mt-3 pt-3 border-top'>
                                <div className='btn_grps d-flex flex-wrap'>
                                    <Link className='back_btn icon_btn me-2' to='/buy'><span className='icon arrow_left'></span> Back</Link>
                                    <button className='main_btn icon_btn ' disabled={isBuyNow ? false : true} onClick={() => continueClick()}>Continue<span className='icon arrow_right_white'></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </LayoutBuy>
    );
};

export default BuyUnlimitedPlan;
