import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import LayoutBuy from '../../components/LayoutBuy';
import {setIsForMySelf} from '../../store/reducers/auth-reducer';
import {connect} from 'react-redux';
import {getActiveQuickBuyServices} from './../../services/buy-module-services/buy-service';
import {getLocationsForQuickBuy} from '../../services/location-service';
import FullScreenLoader from '../../utils/loaders/FullScreenLoader';
import {getPCIDetailsLocal} from '../../utils/common-repo';
import {setBuyServices} from './../../store/reducers/global-reducer';
import store from './../../store/index';
import {fetchPCIDetailsFromServer} from '../../utils/common-repo';
import {setSelectedPlan} from '../../store/reducers/order-reducer';
import {setServices} from '../../store/reducers/prepaid-wash-reducer';
import {setBuyLocation} from '../../store/reducers/location-reducer';
class DashboardBuy extends Component {
    constructor(props) {
        super(props);
        fetchPCIDetailsFromServer();
        this.state = {
            customerCode: localStorage.getItem('customerCode'),
            services: store.getState().global.buyServices,
            isLoading: false,
            PCIConfig: getPCIDetailsLocal(),
            isNearByLocationOpen: false,
            locationData: store.getState().auth.locations,
            buyLocationData: store.getState().location.buyLocations
        };
        if (this.state.customerCode == null || this.state.customerCode == 'null' || this.state.customerCode == undefined) {
            return window.location.href = '/';
        } else {
            localStorage.setItem('allowAnnomous', true);
        }
        this.props.setSelectedPlan();
        this.props.setServicesNull();
    };

    componentDidMount() {
        if (localStorage.getItem('buyLocationId') === null || localStorage.getItem('buyLocationId') === undefined) {
            this.setState({isNearByLocationOpen: true});
        }
        if (this.state.buyLocationData.length === 0) {
            const getLocations = async () => {
                const response = await getLocationsForQuickBuy(this.state.customerCode);
                if (response.status === 200) {
                    this.setState({buyLocationData: response.data});
                    setTimeout(() => {
                        this.props.setBuyLocationData(response.data);
                    }, 1000);
                }
            };
            getLocations();
        }
        if (this.state.services.length === 0) {
            const getServies = async () => {
                this.setState({isLoading: true});
                const response = await getActiveQuickBuyServices(this.state.customerCode);
                if (response.status === 200) {
                    setTimeout(() => {
                        this.setState({
                            services: response.data.sort((a, b) => a.iOrderNumber > b.iOrderNumber ? 1 : -1),
                            isLoading: false
                        });
                        this.props.setBuyServices(response.data);
                    }, 1000);
                }
            };
            getServies();
        }
    }
    setNearByLocationByLocation = async (location) => {
        console.log(location.locationId);
        localStorage.setItem('buyLocationId', location.locationId);
        this.setState({isNearByLocationOpen: false});
    };
    render() {
        const {services, isLoading, isNearByLocationOpen, buyLocationData} = this.state;

        return (
            ( this.state.customerCode == null || this.state.customerCode == 'null' || this.state.customerCode == undefined) ? '' :
                <LayoutBuy>
                    {isLoading === true ? <FullScreenLoader /> : ''}
                    <div className='container'>
                        <div className='row'>
                            {!this.state.PCIConfig.isPaymentLive && <div className='col-12'>
                                <div className='alert alert-danger d-flex align-items-center' role='alert'>
                                    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='currentColor' className='bi bi-exclamation-triangle-fill flex-shrink-0 me-2' viewBox='0 0 16 16' role='img' aria-label='Warning:'>
                                        <path d='M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z' />
                                    </svg>
                                    <div>
                                    DEMO SITE ONLY, PAYMENTS ARE NOT LIVE
                                    </div>
                                </div>
                            </div>}
                            <div className='col-12'>
                                <div className='main_title mb-4 mt-3'>
                                    <h2>Hi ,Welcome to Car Wash !</h2>
                                    <p>We would happy to deliver to our best services to you!</p>
                                </div>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='col-12'>
                                <div className='services_list'>
                                    <div className='row'>
                                        {services.length > 0 ? services.map((service, index) => {
                                            if (service.type === 1 && service.isActive === true) {
                                                return (
                                                    <div className='col-md-4 col-sm-6 col-12' key={service.id}>
                                                        <Link to={`/${service.bSellOnQuickBuySection && 'buy/'}${service.targetId}`} onClick={()=> this.props.setIsForMySelf()}>
                                                            <div className='service_box'>
                                                                <div className='ser_img'><img src={service.imagePath} className='img-fluid' alt='' /></div>
                                                                <div className='ser_info'>
                                                                    <h3>{service.title}</h3>
                                                                    <span>{service.description}</span>
                                                                </div>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                );
                                            }
                                        }) : ''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={`modal fade cw_modal add_info_pop ${isNearByLocationOpen ? 'show d-block' : ''}`} id='locationInfo' data-bs-backdrop='static' data-bs-keyboard='false' tabIndex='-1' aria-labelledby='locationInfoLabel' aria-hidden='true'>
                        <div className='modal-dialog modal-dialog-centered '>
                            <div className='modal-content locationinfo'>
                                <div className='modal-header'>
                                    <h3 className='modal-title' id='locationInfoLabel'>Locations</h3>
                                </div>
                                <div className='modal-body'>
                                    {buyLocationData !== undefined && buyLocationData !== null && buyLocationData.map((service, index) => {
                                        return (
                                            <div className='select_loc_pop' key={service.locationId}>
                                                <div className='service_box'>
                                                    <div className='ser_info'>
                                                        <h3>{service.sName}</h3>
                                                        <span>{service.sAddress}</span>
                                                    </div>
                                                    <button type='button' className='main_btn' style={{float: 'right'}} onClick={() => this.setNearByLocationByLocation(service)}>Set Location</button>
                                                </div>
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                    {isNearByLocationOpen ? <div className="modal-backdrop fade show"></div> : ''}
                </LayoutBuy>
        );
    }
}

// export default Dashboard;
const mapDispatchToProps = (dispatch) => {
    return {
        setIsForMySelf: () => dispatch(setIsForMySelf({isForMySelf: true})),
        setBuyServices: (data) => dispatch(setBuyServices({buyServices: data})),
        setSelectedPlan: () => dispatch(setSelectedPlan({selectedPlan: {}})),
        setServicesNull: () => dispatch(setServices({services: {}})),
        setBuyLocationData: (data) => dispatch(setBuyLocation({buyLocations: data}))
    };
};

export default connect(null, mapDispatchToProps)(DashboardBuy);
