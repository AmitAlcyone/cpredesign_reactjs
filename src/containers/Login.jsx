import React, {useState, useEffect} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import Layout from '../components/Layout';
import loginPageImg from '../assets/images/auth_bg.png';
import loginSchema from '../validation-schemas/loginSchema';
import barcodeSchema from '../validation-schemas/barcodeSchema';
import {Formik} from 'formik';
import {setPhoneNumber, setBarcodeCC, setLocations, setCreditCardN, setUserInfo, setLoginStatus} from '../store/reducers/auth-reducer';
import {loginApi, barcodeApi} from '../services/auth-service';
import {useDispatch} from 'react-redux';
import {toast} from 'react-toastify';
import InputMask from 'react-input-mask';
import {useSelector} from 'react-redux';
import {fetchPCIDetailsFromServer} from '../utils/common-repo';
import {location} from './../services/location-service';
import {getCustomerInfoByGuid} from '../services/user-services';

const Login = (props) => {
    toast.configure();
    const toastId = React.useRef(null);
    delete localStorage['allowAnnomous'];
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);
    const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
    // const customizationsList = useState(useSelector((state) => state.global.portalCustomizationsList));
    const portalCustomizationsList = useSelector((state) => state.global.portalCustomizationsList);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const LoginBarcodeWording = localStorage.getItem('customizePage') == null | undefined ? {} : JSON.parse(localStorage.getItem('customizePage')).portalSettingsList.find((value) => value.name === 'LoginBarcodeWording');
    const customerCode= localStorage.getItem('customerCode');

    const setIsContinue = () => {
        localStorage.setItem('isContinue', false);
    };

    const handleSubmit = async (values) => {
        setLoading(true);
        const phoneNumber = (values.phone).replace(/([() ])+/g, '');
        const response = await loginApi(phoneNumber);
        if (response.status === 200) {
            setLoading(false);
            dispatch(setPhoneNumber({phoneNumber}));
            localStorage.setItem('phoneNumber', phoneNumber);
            toast.success('OTP sent on your registered number.', {autoClose: 3000});
            return navigate('/verify-otp');
        } else {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('You have entered invalid mobile number. Please try again.', {autoClose: 3000});
                setLoading(false);
            }
            setLoading(false);
        }
    };
    const handleBarcodeSubmit = async (param) => {
        setLoading(true);
        const reqBody = {
            Barcode: param.barCode,
            CCNumber: param.ccNumber
        };
        dispatch(setBarcodeCC({reqBody}));
        dispatch(setCreditCardN({reqBody}));
        const response = await barcodeApi(reqBody);
        console.log(response);
        if (response.data.accessToken !== null) {
            await getCustomerInfo(response.data);
            setLoading(false);
            const userData = response.data;
            userData['expiryTime'] = new Date(userData.expires).getTime();
            getlocations(userData.accessToken);
            await fetchPCIDetailsFromServer();
            dispatch(setUserInfo({userInfo: userData}));
            dispatch(setLoginStatus({isLoggedIn: true}));
            localStorage.setItem('userInfo', JSON.stringify(userData));
            return navigate('/dashboard');
        } else {
            setLoading(false);
            toast.error('Incorrect Barcode or CC.', {autoClose: 3000});
        }
    };
    const getCustomerInfo = async (userInfo) => {
        const response = await getCustomerInfoByGuid(userInfo.accessToken, userInfo.customerGuid);
        if (response.status == 200) {
            localStorage.setItem('customerDetail', JSON.stringify(response.data));
            localStorage.setItem('phoneNumber', response.data.customer.sPhone);
        } else if (response.status === (401 | 404 | 470)) {
            toast.warning(`${response.data.title}.`, {autoClose: 3000});
        } else {
            toast.error('Some error occurred.', {autoClose: 3000});
        }
    };
    const getlocations = async (accessToken) => {
        const response = await location(accessToken);
        if (response.status == 200) {
            dispatch(setLocations({locations: response.data}));
            localStorage.setItem('locations', JSON.stringify(response.data));
            localStorage.setItem('locationId', response.data[0].locationId);
        }
    };

    useEffect(() => {
        if (isLoggedIn) {
            return navigate('/dashboard');
        } else if (customerCode == null || customerCode == 'null' || customerCode == undefined) {
            return navigate('/error-page');
        }
    }, []);

    return (
        <>
            {
                (customerCode == null || customerCode == 'null' || customerCode == undefined) ? '' :
                    <Layout>
                        <div className="auth_page">
                            <div className="auth_wrap">
                                <div className="auth_left">
                                    <div className="auth_form_wrap pt-5">
                                        <div className="brand_logo mb-4">
                                            <Link to="/">
                                                {portalCustomizationsList.length && portalCustomizationsList.map((value, key) => {
                                                    if (value.sName === 'site_logo_url') {
                                                        return <img src={value.sValue} className="img-fluid" alt="logo" key={key}/>;
                                                    }
                                                })}
                                            </Link>
                                        </div>
                                        <div className="auth_form_sec">
                                            <h1 className="form_title mt-5 mb-5">Welcome!</h1>
                                            <ul className="nav nav-pills justify-content-between mb-3" id="pills-tab" role="tablist">
                                                <li className="nav-item" role="presentation">
                                                    <button className="nav-link active" id="pills-phone-tab" data-bs-toggle="pill" data-bs-target="#pills-phone" type="button" role="tab" aria-controls="pills-phone" aria-selected="true">Phone</button>
                                                </li>
                                                <li className="nav-item" role="presentation">
                                                    <button className="nav-link" id="pills-barcode-tab" data-bs-toggle="pill" data-bs-target="#pills-barcode" type="button" role="tab" aria-controls="pills-barcode" aria-selected="false">{LoginBarcodeWording.value === '' || LoginBarcodeWording.value === null ? 'Barcode or  License plate #' : LoginBarcodeWording.value}</button>
                                                </li>
                                            </ul>
                                            <div className="tab-content pt-3" id="pills-tabContent">
                                                <div className="tab-pane fade show active" id="pills-phone" role="tabpanel" aria-labelledby="pills-phone-tab">
                                                    <Formik
                                                        initialValues={{
                                                            phone: ''
                                                        }}
                                                        onSubmit={(values, {resetForm}) => {
                                                            handleSubmit(values);
                                                        }}
                                                        validationSchema={loginSchema}
                                                    >
                                                        {({values,
                                                            errors,
                                                            touched,
                                                            handleChange,
                                                            handleBlur,
                                                            handleSubmit,
                                                            isValid
                                                        }) => (
                                                            <form onSubmit={handleSubmit} className="auth_form mb-5">
                                                                <div className="mb-3">
                                                                    <label htmlFor="phone" className="form-label">Phone<sup className="text-danger">*</sup></label>
                                                                    <InputMask
                                                                        className="form-control form-control-lg"
                                                                        onChange={handleChange}
                                                                        onBlur={handleBlur}
                                                                        mask="(999) 999 9999"
                                                                        maskChar=""
                                                                        value={values.phone}
                                                                        name="phone"
                                                                        placeholder="(000) 000 0000"
                                                                    />
                                                                    {touched.phone && errors.phone ? (
                                                                        <small className="text-danger validationError">{errors.phone}</small>
                                                                    ) : ''}
                                                                </div>
                                                                <button type="submit" className="main_btn button" disabled={loading || !isValid || values.phone === ''} onClick={() => setIsContinue()}>
                                                                    {loading && <span className="spinner-border text-light btnLoader"></span>}<span>Submit</span>
                                                                </button>
                                                            </form>
                                                        )}
                                                    </Formik>
                                                </div>
                                                <div className="tab-pane fade" id="pills-barcode" role="tabpanel" aria-labelledby="pills-barcode-tab">
                                                    <Formik
                                                        initialValues={{
                                                            barCode: '',
                                                            ccNumber: ''
                                                        }}
                                                        onSubmit={(values, {resetForm}) => {
                                                            handleSubmit(values);
                                                        }}
                                                        validationSchema={barcodeSchema}
                                                    >
                                                        {({values,
                                                            errors,
                                                            touched,
                                                            handleChange,
                                                            handleBlur,
                                                            handleSubmit,
                                                            isValid
                                                        }) => (
                                                            <form className="auth_form mb-5" action="" onSubmit={handleSubmit}>
                                                                <div className="mb-3">
                                                                    <label htmlFor="barCode" className="form-label">{LoginBarcodeWording.value === '' || LoginBarcodeWording.value === null ? 'Barcode or  License plate #' : LoginBarcodeWording.value}<sup className="text-danger">*</sup></label>
                                                                    <input type="tel" className="form-control form-control-lg" id="barCode" placeholder=""
                                                                        name="barCode"
                                                                        onChange={handleChange}
                                                                        onBlur={handleBlur}
                                                                        value={values.barCode}
                                                                    />
                                                                    {touched.barCode && errors.barCode ? (
                                                                        <small className="text-danger validationError">{errors.barCode}</small>
                                                                    ) : ''}
                                                                </div>
                                                                <div className="mb-3">
                                                                    <label htmlFor="ccNumber" className="form-label">Last 4 of CC<sup className="text-danger">*</sup></label>
                                                                    <input type="tel" className="form-control form-control-lg" id="ccNumber" placeholder=""
                                                                        name="ccNumber"
                                                                        onChange={handleChange}
                                                                        onBlur={handleBlur}
                                                                        value={values.ccNumber}
                                                                    />
                                                                    {touched.ccNumber && errors.ccNumber ? (
                                                                        <small className="text-danger validationError">{errors.ccNumber}</small>
                                                                    ) : ''}
                                                                </div>
                                                                <button type="submit" className="main_btn" disabled={loading || !isValid || values.barCode === '' || values.ccNumber === ''}
                                                                    onClick={() => {
                                                                        handleBarcodeSubmit(values);
                                                                        setIsContinue();
                                                                    }}>{loading && <span className="spinner-border text-light btnLoader"></span>}<span>Continue</span></button>
                                                            </form>
                                                        )}
                                                    </Formik>
                                                </div>
                                                <div className="switch_form text-center">Not a member? <Link to="/signup">Create an account</Link></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="auth_right" style={{backgroundImage: `url(${loginPageImg})`}}></div>
                            </div>
                        </div>

                        <div className={`modal fade custom_pop error_pop ${error ? 'show d-block' : ''}`} id="errorModal" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="errorModalLabel" aria-hidden="true">
                            <div className="modal-dialog  modal-dialog-centered">
                                <div className="modal-content">
                                    <div className="modal-body">
                                        <h2>Error!</h2>
                                        <p>Invalid Number !!!</p>
                                    </div>
                                    <div className="modal-footer justify-content-center">
                                        <button type="button" className="main_btn" data-bs-dismiss="modal" onClick={() => setError(false)}>Okay</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {error ? <div className="modal-backdrop fade show"></div> : ''}
                    </Layout>
            }
        </>
    );
};

export default Login;
