import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import Layout from '../components/Layout';
import {setCustomizePage} from '../store/reducers/auth-reducer';
import {setPortalCustomizationsList} from '../store/reducers/global-reducer';
import {customerUrlApi, customizationApi, policyData} from '../services/auth-service';
import {useDispatch} from 'react-redux';
import {toast} from 'react-toastify';
import FullScreenLoader from '../utils/loaders/FullScreenLoader';
import CustomizationPage from '../assets/JSON/Customization_Page.json';

const Home = (props) => {
    toast.configure();
    const urlParam = useParams();
    const dispatch = useDispatch();
    const [Loader, setLoader] = useState(false);
    const changeThemeStyle = (data) => {
        for (let index = 0; index < data.length; index++) {
            const value = data[index];

            if (value.sName === 'site_primary_color') {
                document.documentElement.style.setProperty('--theme_color10', value.sValue);
            } else if (value.sName === 'site_secondary_color') {
                document.documentElement.style.setProperty('--theme_color20', value.sValue);
            } else if (value.sName === 'site_light_color') {
                document.documentElement.style.setProperty('--theme_color40', value.sValue);
            } else if (value.sName === 'site_text_color_dark') {
                document.documentElement.style.setProperty('--dark_text_color', value.sValue);
            } else if (value.sName === 'site_text_color_light') {
                document.documentElement.style.setProperty('--light_text_color', value.sValue);
            } else if (value.sName === 'site_border_color') {
                document.documentElement.style.setProperty('--border_color', value.sValue);
            } else if (value.sName === 'site_header_color') {
                document.documentElement.style.setProperty('--header_color', value.sValue);
            } else if (value.sName === 'site_footer_color') {
                document.documentElement.style.setProperty('--footer_color', value.sValue);
            } else if (value.sName === 'site_menu_header_color') {
                document.documentElement.style.setProperty('--main_menu_color', value.sValue);
            }
        }
    };

    useEffect(() => {
        const getCustomerCode = async () => {
            delete localStorage['allowAnnomous'];
            setLoader(true);
            const customerUrl= urlParam.id;
            const page= props.page == undefined ? '' : String(props.page).toLowerCase();
            if (customerUrl==='' || customerUrl===undefined) {
                if (page== 'buy') {
                    window.location.href = '/buy';
                }
                toast.error('Incorrect URL.', {autoClose: 3000});
                return window.location.href='/';
            }
            const customerCode = await customerUrlApi(customerUrl);
            if (customerCode.status === 200) {
                const policyDataResponse = await policyData(customerCode.data);
                if (policyDataResponse.status == 200) {
                    localStorage.setItem('policyData', JSON.stringify(policyDataResponse.data));
                } else {
                    localStorage.setItem('policyData', ' ');
                }
                localStorage.setItem('customerCode', customerCode.data);
                localStorage.setItem('customerUrl', customerUrl);
                const responce = await customizationApi(customerCode.data);
                if (responce.status == undefined) {
                    setLoader(false);
                    localStorage.setItem('customizePage', JSON.stringify(CustomizationPage));
                    dispatch(setCustomizePage({customizePage: CustomizationPage}));
                    dispatch(setPortalCustomizationsList({portalCustomizationsList: CustomizationPage.portalCustomizationsList}));
                    changeThemeStyle(CustomizationPage.portalCustomizationsList);
                    if (page == 'buy') {
                        localStorage.setItem('allowAnnomous', true);
                        return window.location.href = '/buy';
                    }
                    return window.location.href = '/';
                } else if (responce.status == 200) {
                    setLoader(false);
                    localStorage.setItem('customizePage', JSON.stringify(responce.data));
                    dispatch(setCustomizePage({customizePage: responce.data}));
                    dispatch(setPortalCustomizationsList({portalCustomizationsList: responce.data.portalCustomizationsList}));
                    // setCustomizationsList(responce.data.portalCustomizationsList);
                    changeThemeStyle(responce.data.portalCustomizationsList);
                    setLoader(false);
                    if (page == 'buy') {
                        localStorage.setItem('allowAnnomous', true);
                        return window.location.href='/buy';
                    }
                    return window.location.href='/';
                } else {
                    setLoader(false);
                    if (page == 'buy') {
                        localStorage.setItem('allowAnnomous', true);
                        return window.location.href='/buy';
                    }
                    return window.location.href='/';
                }
            } else {
                delete localStorage['customerCode'];
                setLoader(false);
                return window.location.href='/';
            }
        };
        getCustomerCode();
    }, []);

    return (
        <Layout>
            {Loader === true ? <FullScreenLoader /> : ''}
        </Layout>
    );
};

export default Home;
