import React from 'react';
import errImg from '../assets/images/errorPage.png';

const ErrorPage = (props) => {
    return (
        <div className='missing_page h-100 d-flex justify-content-center align-items-center text-center'>
            <div className='fourofour_wrap'>
                <img src={errImg} className='img-fluid mb-4' alt='404'/>
                <h5>Page Not Found.</h5>
                <p>Please check your URL and try again.</p>
            </div>
        </div>
    );
};
export default ErrorPage;
