import React, {useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import Layout from '../components/Layout';
import logo from '../assets/images/cw_logo.png';
import loginPageImg from '../assets/images/auth_bg.png';
import otpVerificationSchema from './../validation-schemas/otpVerificationSchema';
import {Formik} from 'formik';
import {otpVerification} from '../services/auth-service';
import {useDispatch} from 'react-redux';
import {toast} from 'react-toastify';
import {setUserInfo, setLoginStatus, setLocations} from './../store/reducers/auth-reducer';
import TermsModal from '../components/modals/TermsModal';
import {location} from './../services/location-service';
import {loginByPhoneV2Api} from '../services/auth-service';
import {fetchPCIDetailsFromServer} from '../utils/common-repo';
import {getCustomerInfoByGuid} from '../services/user-services';
import FullScreenLoader from '../utils/loaders/FullScreenLoader';

const Verification = (props) => {
    toast.configure();
    const toastId = React.useRef(null);
    const [loading, setLoading] = useState(false);
    const [FullLoader, setFullLoader] = useState(false);
    const [modal, setModal] = useState(false);
    const [modalP, setModalP] = useState(false);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const phoneNumber = localStorage.getItem('phoneNumber');

    const handleSubmit = async (values) => {
        setLoading(true);
        const varificationResponse = await otpVerification(phoneNumber, values.otp);
        if (varificationResponse.status === 200) {
            const userData = varificationResponse.data;
            await getCustomerInfo(userData);
            userData['expiryTime'] =new Date(userData.expires).getTime();
            getlocations(userData.accessToken);
            await fetchPCIDetailsFromServer();
            // settingData(userData.accessToken);
            dispatch(setUserInfo({userInfo: userData}));
            /* dispatch(setLoginStatus({isLoggedIn: true}));*/
            localStorage.setItem('userInfo', JSON.stringify(userData));
            setTimeout(() => {
                setLoading(false);
                dispatch(setLoginStatus({isLoggedIn: true}));
                return navigate('/dashboard');
            }, 3000);
        } else {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Some error occured.', {autoClose: 3000});
                setLoading(false);
            }
            setLoading(false);
        }
    };
    const getCustomerInfo = async (userInfo) => {
        const response = await getCustomerInfoByGuid(userInfo.accessToken, userInfo.customerGuid);
        if (response.status == 200) {
            localStorage.setItem('customerDetail', JSON.stringify(response.data));
        } else if (response.status === (401 | 404 | 470)) {
            toast.warning(`${response.data.title}.`, {autoClose: 3000});
        } else {
            toast.error('Some error occurred.', {autoClose: 3000});
        }
    };
    const getlocations = async (accessToken) => {
        const response = await location(accessToken);
        if (response.status == 200) {
            dispatch(setLocations({locations: response.data}));
            localStorage.setItem('locations', JSON.stringify(response.data));
            localStorage.setItem('locationId', response.data[0].locationId);
        }
    };
    const resendOtp = async (values) => {
        setFullLoader(true);
        const response = await loginByPhoneV2Api(phoneNumber);
        if (response.status === 200) {
            setFullLoader(false);
            localStorage.setItem('phoneNumber', phoneNumber);
            toast.success('OTP resend successfully.', {autoClose: 3000});
            // return navigate('/verify-otp');
        } else {
            toast.error('You have entered invalid mobile number. Please try again.', {autoClose: 3000});
            setFullLoader(false);
        }
    };

    return (
        <Layout>
            {modal ? <TermsModal id={0} closeModal={() => setModal(false)}/> : '' }
            {modalP ? <TermsModal id={1} closeModal={() => setModalP(false)}/> : '' }
            {FullLoader === true ? <FullScreenLoader /> : ''}
            <div className="auth_page">
                <div className="auth_wrap">
                    <div className="auth_left">
                        <div className="auth_form_wrap pt-5">
                            <div className="brand_logo mb-4">
                                <Link to="/"><img src={logo} className="img-fluid" alt="" /></Link>
                            </div>
                            <div className="auth_form_sec">
                                <h1 className="form_title mt-5">Verification Code</h1>
                                <p className=" mb-5">To verify your phone, we have sent as a One Time Password (OTP) to {phoneNumber}</p>
                                <Formik
                                    initialValues={{
                                        otp: ''
                                    }}
                                    onSubmit={(values) => {
                                        handleSubmit(values);
                                    }}
                                    validationSchema={otpVerificationSchema}
                                >
                                    {({values,
                                        errors,
                                        touched,
                                        handleChange,
                                        handleBlur,
                                        handleSubmit,
                                        isValid
                                    }) => (
                                        <form onSubmit={handleSubmit} className="auth_form mb-5">
                                            <div className="mb-3">
                                                <label htmlFor="otp" className="form-label">OTP<sup className="text-danger">*</sup></label>
                                                <input type="text" className="form-control form-control-lg" id="otp" aria-describedby="nameHelp"
                                                    name="otp"
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.otp}
                                                />
                                                {touched.otp && errors.otp ? (
                                                    <small className="text-danger">{errors.otp}</small>
                                                ) : null}
                                            </div>
                                            <button type="submit" className="main_btn button" disabled={loading || !isValid}>
                                                {loading && <span className="spinner-border text-light btnLoader"></span>}<span>Submit</span>
                                            </button>
                                        </form>
                                    )}
                                </Formik>
                                <div className="switch_form text-center mb-5 cursor_pointer" onClick={() => resendOtp()}><strong>(Resend OTP)</strong></div>
                                <div className="switch_form text-center">Not a registered number? <Link to="/">Change Number!</Link></div><br/>
                                <div className="auth_policy_text text-center mt-3">
                                    <p>By creating your account, you agree to <br />
                                        <Link to="#" onClick={() => setModal(true)}>Conditions of Use,</Link>&nbsp;&nbsp;
                                        <Link to="#" onClick={() => setModalP(true)}> Privacy Notice</Link></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="auth_right" style={{backgroundImage: `url(${loginPageImg})`}}></div>
                </div>
            </div>
        </Layout>
    );
};

export default Verification;
