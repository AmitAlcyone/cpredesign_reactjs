import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Layout from '../components/Layout';
import {setIsForMySelf, setPreviousPage} from '../store/reducers/auth-reducer';
import {connect} from 'react-redux';
import store from '../store/index';
import {getPCIDetailsLocal} from '../utils/common-repo';
import {nearByLocation, verifyZipcode} from './../services/location-service';
import {setIsLoading} from './../store/reducers/global-reducer';
import {setUserInfo} from './../store/reducers/auth-reducer';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {toast} from 'react-toastify';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            PCIConfig: getPCIDetailsLocal(),
            isContinue: JSON.parse(localStorage.getItem('isContinue')),
            count: 0,
            userInfo: store.getState().auth.userInfo,
            phoneNumber: localStorage.getItem('phoneNumber'),
            isZipModalOpen: false,
            isNearByLocationOpen: false,
            locationData: store.getState().auth.locations,
            portalCustomizationsList: JSON.parse(localStorage.getItem('customizePage')).portalCustomizationsList
        };
    };

    changeThemeStyle(data) {
        for (let index = 0; index < data.length; index++) {
            const value = data[index];
            if (value.sName === 'site_primary_color') {
                document.documentElement.style.setProperty('--theme_color10', value.sValue);
            } else if (value.sName === 'site_secondary_color') {
                document.documentElement.style.setProperty('--theme_color20', value.sValue);
            } else if (value.sName === 'site_light_color') {
                document.documentElement.style.setProperty('--theme_color40', value.sValue);
            } else if (value.sName === 'site_text_color_dark') {
                document.documentElement.style.setProperty('--dark_text_color', value.sValue);
            } else if (value.sName === 'site_text_color_light') {
                document.documentElement.style.setProperty('--light_text_color', value.sValue);
            } else if (value.sName === 'site_border_color') {
                document.documentElement.style.setProperty('--border_color', value.sValue);
            } else if (value.sName === 'site_header_color') {
                document.documentElement.style.setProperty('--header_color', value.sValue);
            } else if (value.sName === 'site_footer_color') {
                document.documentElement.style.setProperty('--footer_color', value.sValue);
            } else if (value.sName === 'site_menu_header_color') {
                document.documentElement.style.setProperty('--main_menu_color', value.sValue);
            }
        }
    };

    componentDidMount() {
        toast.configure();
        const customSetState = (data) => this.setState(data);
        this.changeThemeStyle(this.state.portalCustomizationsList);
        const getLocation = () => {
            const success = (position) => console.log(position.coords);
            const permissionDeny = this.permissionDeny;
            const setNearBuyLocation = this.setNearBuyLocation;
            // const error = () => alert('Please use another browser.');

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(success); // If we need to show geolocation error then we can pass error function in it.
            } else {
                // console.log('Geolocation is not supported by this browser.');
            }

            navigator.permissions.query({name: 'geolocation'}).then(function(permissionStatus) {
                permissionStatus.onchange = function() {
                    if (permissionStatus.state == 'granted') {
                        setNearBuyLocation(permissionStatus.state, false, false);
                    } else if (permissionStatus.state == 'denied') {
                        permissionDeny();
                    }
                };
            });
        };

        if (this.state.userInfo.preferedHomeLocation === null) {
            getLocation();
            navigator.geolocation.watchPosition(function(position) {
                // console.log('i"m tracking you!');
            },
            function(error) {
                if (error.code == error.PERMISSION_DENIED) customSetState({isZipModalOpen: true});
            });
        }
    }

    // On location allow
    setNearBuyLocation = async (data, viaZipcode, viaLocation) => {
        const userInfo = {...this.state.userInfo};
        (viaZipcode || viaLocation) && this.props.setIsLoading(true);
        const response = await nearByLocation(userInfo.accessToken, {
            lat: data.latitude,
            lng: data.longitude,
            phoneNumber: this.state.phoneNumber,
            customerIdentityId: userInfo.customerIdentityID
        });
        if (response.status === 200) {
            setTimeout(() => {
                userInfo.preferedHomeLocation = response.data.preferedHomeLocation;
                this.props.setUserInfo(userInfo);
                localStorage.setItem('userInfo', JSON.stringify(userInfo));
                this.props.setIsLoading(false);
                if (viaZipcode) {
                    this.setState({isZipModalOpen: false});
                } else if (viaLocation) {
                    this.setState({isNearByLocationOpen: false});
                }
                (viaZipcode || viaLocation) && toast.success(response.data.message, {autoClose: 3000});
            }, 1000);
        } else {
            this.props.setIsLoading(false);
        }
    };

    // Location permission deny
    permissionDeny = () => this.setState({isZipModalOpen: true});

    setIsContinue = () => {
        JSON.stringify(localStorage.setItem('isContinue', false));
        this.setState({isContinue: false});
    };

    hideContinue = () => {
        this.setState({count: this.state.count + 1});
        if (this.state.count === 3) {
            JSON.stringify(localStorage.setItem('isContinue', false));
            this.setState({isContinue: false});
        }
    };

    setNearByLocationByZip = async (values) => {
        this.props.setIsLoading(true);
        const response = await verifyZipcode(values.zipcode);
        if (response.status === 200) {
            const resData = response.data;
            if (resData.status === 'OK' && resData.results.length > 0) {
                const data = {
                    latitude: resData.results[0].geometry.location.lat,
                    longitude: resData.results[0].geometry.location.lng
                };
                this.setNearBuyLocation(data, true, false);
            } else {
                toast.warning('Wrong ZIP code.', {autoClose: 3000});
            }
            setTimeout(() => {
                this.props.setIsLoading(false);
            }, 1000);
        } else {
            this.props.setIsLoading(false);
        }
    };

    setNearByLocationByLocation = async (location) => {
        const data = {
            latitude: location.sLatitude,
            longitude: location.sLongitude
        };
        this.setNearBuyLocation(data, false, true);
    };

    managePreviousPage = (route) => {
        if (route === 'unlimited_wash_club_box') {
            this.props.setPreviousPage();
        }
        this.props.setIsForMySelf();
    };

    render() {
        const {isContinue, isZipModalOpen, isNearByLocationOpen, locationData} = this.state;
        const customizedData = JSON.parse(localStorage.getItem('customizePage'));

        return (
            <Layout>
                <div className='container'>
                    <div className='row'>
                        {!this.state.PCIConfig.isPaymentLive ? <div className='col-12'>
                            <div className='alert alert-danger d-flex align-items-center' role='alert'>
                                <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='currentColor' className='bi bi-exclamation-triangle-fill flex-shrink-0 me-2' viewBox='0 0 16 16' role='img' aria-label='Warning:'>
                                    <path d='M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z' />
                                </svg>
                                <div>
                                    DEMO SITE ONLY, PAYMENTS ARE NOT LIVE
                                </div>
                            </div>
                        </div> : ''}
                        <div className='col-12'>
                            <div className='main_title mb-4 mt-3'>
                                <h2>Hi ,Welcome to Car Wash !</h2>
                                <p>We would happy to deliver to our best services to you!</p>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-12'>
                            <div className='services_list'>
                                <div className='row'>
                                    {customizedData.portalScreenOptionsList.sort((a, b) => a.iOrderNumber > b.iOrderNumber ? 1 : -1).map((service, index) => {
                                        if (service.type === 1 && service.isActive===true) {
                                            return (
                                                <div className='col-md-4 col-sm-6 col-12' key={service.id}>
                                                    <Link to={`/${service.targetId}`} onClick={() => this.managePreviousPage(service.targetId)}>
                                                        <div className='service_box'>
                                                            <div className='ser_img'><img src={service.imagePath} className='img-fluid' alt='' /></div>
                                                            <div className='ser_info'>
                                                                <h3>{service.title}</h3>
                                                                <span>{service.description}</span>
                                                            </div>
                                                        </div>
                                                    </Link>
                                                </div>
                                            );
                                        }
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`modal fade cw_modal add_info_pop ${isNearByLocationOpen ? 'show d-block' : ''}`} id='locationInfo' data-bs-backdrop='static' data-bs-keyboard='false' tabIndex='-1' aria-labelledby='locationInfoLabel' aria-hidden='true'>
                    <div className='modal-dialog modal-dialog-centered '>
                        <div className='modal-content locationinfo'>
                            <div className='modal-header'>
                                <h3 className='modal-title' id='locationInfoLabel'>Locations</h3>
                            </div>
                            <div className='modal-body'>
                                {locationData !== undefined && locationData !== null && locationData.map((service, index) => {
                                    return (
                                        <div className='select_loc_pop' key={service.locationId}>
                                            <div className='service_box'>
                                                <div className='ser_info'>
                                                    <h3>{service.sName}</h3>
                                                    <span>{service.sAddress}</span>
                                                </div>
                                                <button type='button' className='main_btn' style={{float: 'right'}} onClick={() => this.setNearByLocationByLocation(service)}>Set Location</button>
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`modal fade cw_modal add_card_pop ${isZipModalOpen ? 'show d-block' : ''}`} id='addCardpop' data-bs-backdrop='static' data-bs-keyboard='false' tabIndex='-1' aria-labelledby='add_new_card_popLabel' aria-hidden='true'>
                    <div className='modal-dialog modal-dialog-centered'>
                        <div className='modal-content'>
                            <Formik
                                initialValues={{zipcode: ''}}
                                onSubmit={(values) => this.setNearByLocationByZip(values)}
                                validationSchema={Yup.object().shape({
                                    zipcode: Yup.string().required('Zipcode is required.').min(5, 'Zipcode must be 5 digits.').max(5, 'Zipcode must be 5 digits.')
                                })}
                            >
                                {({values,
                                    errors,
                                    touched,
                                    handleChange,
                                    handleBlur,
                                    handleSubmit,
                                    isValid
                                }) => (
                                    <form onSubmit={handleSubmit}>
                                        <div className='modal-body zip_input_modal pt-4 pb-4'>
                                            <input
                                                type='number'
                                                name='zipcode'
                                                id='zipcode'
                                                className='form-control form-control-lg'
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.zipcode}
                                                placeholder='ZIP Code'
                                            />
                                            {touched.zipcode && errors.zipcode ? (
                                                <small className="text-danger">{errors.zipcode}</small>
                                            ) : null}
                                        </div>
                                        <div className='modal-footer justify-content-center'>
                                            <button type='submit' className='main_btn' disabled={!isValid}>Save</button>
                                            {locationData !== undefined && locationData !== null && locationData.length > 0 ?
                                                <button type='button' className='sec_btn' onClick={() => this.setState({isZipModalOpen: false, isNearByLocationOpen: true})}>Cancel</button> : ''
                                            }
                                        </div>
                                    </form>
                                )}
                            </Formik>
                        </div>
                    </div>
                </div>
                {/* continue modal */}
                {isContinue ?
                    <div className="conatiner">
                        <div className="washes-pop-up">
                            <div className={`modal fade show`} id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel"
                                aria-hidden="true" style={{display: 'block'}}>
                                <div className="modal-dialog">
                                    <div className="modal-content">
                                        <div className="modal-body">
                                            <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="carousel">
                                                <div className="carousel-indicators">
                                                    <button type="button" data-bs-target="#carouselExampleIndicators"
                                                        data-bs-slide-to="0" className="active" aria-current="true"
                                                        aria-label="Slide 1"></button>
                                                    <button type="button" data-bs-target="#carouselExampleIndicators"
                                                        data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                    <button type="button" data-bs-target="#carouselExampleIndicators"
                                                        data-bs-slide-to="2" aria-label="Slide 3"></button>
                                                    <button type="button" data-bs-target="#carouselExampleIndicators"
                                                        data-bs-slide-to="3" aria-label="Slide 4"></button>
                                                </div>
                                                <div className="carousel-inner">
                                                    {customizedData.portalScreenOptionsList.sort((a, b) => a.iOrderNumber > b.iOrderNumber ? 1 : -1).map((info, index) => {
                                                        if (info.type === 2 && info.isActive === true) {
                                                            return (
                                                                <div className={`carousel-item ${info.iOrderNumber == 1 ? 'active' : ''}`} key={info.id}>
                                                                    <img src="assets/images/user.png" alt="" />
                                                                    <h2>{info.title}</h2>
                                                                    <p>{info.description}</p>
                                                                </div>
                                                            );
                                                        }
                                                    })}
                                                </div>
                                                <button className="carousel-control-prev" type="button"
                                                    data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span className="visually-hidden">Previous</span>
                                                </button>
                                                <button className="carousel-control-next" type="button"
                                                    data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span className="visually-hidden">Next</span>
                                                </button>
                                            </div>
                                        </div>
                                        <div className="modal-footer">

                                            <button type="button" className="btn carousel-control-next"
                                                data-bs-target="#carouselExampleIndicators" data-bs-slide="next" onClick={() => this.hideContinue()}>
                                                Continue<i className="fa-solid fa-arrow-right"></i></button>

                                            <button type="button" className="btn" data-bs-dismiss="modal" onClick={() => this.setIsContinue()}>Skip</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> : ''
                }

                {isContinue || isZipModalOpen || isNearByLocationOpen ? <div className="modal-backdrop fade show"></div> : ''}
            </Layout>
        );
    }
}

// export default Dashboard
const mapDispatchToProps = (dispatch) => {
    return {
        setIsForMySelf: () => dispatch(setIsForMySelf({isForMySelf: true})),
        setIsLoading: (param) => dispatch(setIsLoading({isLoading: param})),
        setUserInfo: (param) => dispatch(setUserInfo({userInfo: param})),
        setPreviousPage: () => dispatch(setPreviousPage({previousPage: '/dashboard'}))
    };
};

export default connect(null, mapDispatchToProps)(Dashboard);
