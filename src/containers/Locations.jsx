import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import Layout from '../components/Layout';
import {setServices} from '../store/reducers/location-reducer';
import {setSelectedLocation} from '../store/reducers/location-reducer';
import {useDispatch} from 'react-redux';
import {location} from './../services/location-service';
import {nearByLocation, locationDetail} from './../services/location-service';
import {useSelector} from 'react-redux';
import Logo from './../assets/images/cw_logo.png';
import {setIsLoading} from './../store/reducers/global-reducer';
import {toast} from 'react-toastify';
import SkeletonLocationList from '../utils/skeletons/SkeletonLocationList';
import {setUserInfo} from './../store/reducers/auth-reducer';
import MapContainer from '../components/map/LocationMap';

const Locations = (props) => {
    const userInfo = useSelector((state) => state.auth.userInfo);
    const services = useSelector((state) => state.location.services);
    const daytime = useSelector((state) => state.location.selectedLocation);
    const dispatch = useDispatch();
    const [flag, setFlage] = useState(false);
    useEffect(() => {
        const getServies = async () => {
            if (services.length === 0) {
                const response = await location(userInfo.accessToken);
                if (response.status === 200) {
                    setTimeout(() => {
                        dispatch(setServices({services: response.data}));
                    }, 1000);
                }
            }
        };
        getServies();
    }, []);
    const skeletonLoader = () => {
        const content = [];
        for (let index = 0; index < 5; index++) {
            content.push(<SkeletonLocationList key={index} />);
        }
        return content;
    };
    const addLocation = async (data) => {
        const userInfoUpdated = JSON.parse(localStorage.getItem('userInfo'));
        dispatch(setIsLoading({isLoading: true}));
        const response = await nearByLocation(userInfo.accessToken, {
            lat: data.sLatitude,
            lng: data.sLongitude,
            phoneNumber: localStorage.getItem('phoneNumber'),
            customerIdentityId: userInfo.customerIdentityID
        });
        if (response.status === 200) {
            setTimeout(() => {
                userInfoUpdated.preferedHomeLocation = response.data.preferedHomeLocation;
                dispatch(setUserInfo({userInfo: userInfoUpdated}));
                localStorage.setItem('userInfo', JSON.stringify(userInfoUpdated));
                dispatch(setIsLoading(false));
                toast.success('Your active location has been change Successfully.', {autoClose: 3000});
            }, 1000);
        } else {
            dispatch(setIsLoading(false));
            toast.error('Something went wrong.', {autoClose: 3000});
        }
    };
    const getLocationDetail = async (data) => {
        if (!flag) {
            dispatch(setIsLoading({isLoading: true}));
            const response = await locationDetail(userInfo.accessToken, {
                lat: data.sLatitude,
                lng: data.sLongitude,
                locationId: localStorage.getItem('locationId'),
                customerIdentityId: userInfo.customerIdentityID
            });
            if (response.status === 200) {
                setFlage(true);
                setTimeout(() => {
                    dispatch(setIsLoading(false));
                    dispatch(setSelectedLocation({selectedLocation: response.data}));
                }, 1000);
            } else {
                dispatch(setIsLoading(false));
                toast.error('Something went wrong.', {autoClose: 3000});
            }
        }
    };
    const weekDaysList = (data) => {
        if (data) {
            const serviceArray = data.locationDetailDayHours;
            return (
                <ul className='timings'>
                    {serviceArray.map((value, index) => {
                        return <li key={index}>{value.day}{'                  '}{value.hours}</li>;
                    })}
                </ul>
            );
        }
    };
    return (
        <Layout>
            <div className='container'>
                <div className='row'>
                    <div className='col-12'>
                        <Link to='/' className='back_link icon_btn'><span className='icon arrow_left'></span> Back</Link>
                    </div>
                    <div className='col-12'>
                        <div className='d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap'>
                            <div className='main_title'>
                                <h2>Our Locations</h2>
                                <p>Manage your preffered location.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-lg-6'>
                        <div className='accordion loc_list' id='accordionFlushExample'>
                            {services.length===0 && skeletonLoader()}
                            {services.map((service, index) => {
                                return (
                                    <div className='accordion-item' key={index}>
                                        <h2 className='accordion-header' id='flush-headingOne'>
                                            <button className='accordion-button collapsed' type='button' data-bs-toggle='collapse' data-bs-target={`#loc_one_${index}`} aria-expanded='false' aria-controls={`#loc_one_${index}`} onClick={() => getLocationDetail(service)}>
                                                <div className='loc_box d-flex align-items-center'>
                                                    <span className='icon mappin_icon me-3'></span>
                                                    <div className='loc_info'>
                                                        <div className='loc_name'>{service.sName}</div>
                                                        <div className='loc_address'>{service.sAddress}</div>
                                                    </div>
                                                </div>
                                            </button>
                                        </h2>
                                        <div id={`loc_one_${index}`} className='accordion-collapse collapse' aria-labelledby='flush-headingOne' data-bs-parent='#accordionFlushExample'>
                                            <div className='accordion-body'>
                                                <div className='loc_details'>
                                                    <div className='row'>
                                                        <div className='col-lg-6'>
                                                            <div className='loc_img'><img src={Logo} className='img-fluid' alt='' /></div>

                                                        </div>
                                                        <div className='col-lg-6'>
                                                            <strong>Timings</strong>
                                                            {weekDaysList(daytime)}
                                                        </div>
                                                        <div className='col-lg-6'>
                                                            <div className='add_fav_btn mt-3 text-center'><Link to='' className='main_btn mx-auto' onClick={() => addLocation(service)}>Add as Favorite Location</Link></div>
                                                        </div>
                                                        <div className='col-lg-6'>
                                                            <ul className='contact_info'>
                                                                <li><div><span className='icon phone_icon'></span> Phone</div><span>{service.sPhone}</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                    <div className='col-lg-6'>
                        <div className='loc_map'>
                            <MapContainer />
                        </div>
                    </div>
                </div>

            </div>
        </Layout>
    );
};

export default Locations;
