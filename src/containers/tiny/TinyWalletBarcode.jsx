import React, {useState, useEffect} from 'react';
import QRCode from 'react-qr-code';
import Barcode from 'react-barcode';
import {walletBarcodeDetail} from '../../services/wallet-service';
import calendarImg from './../../assets/images/calendar.png';
import {toast} from 'react-toastify';
import {useParams} from 'react-router-dom';
import FullScreenLoader from '../../utils/loaders/FullScreenLoader';
import dateFormatter from '../../utils/dateFormatter';


const TinyWalletBarcode = (props) => {
    toast.configure();
    const [walletDetail, setWalletDetail] = useState([]);
    const urlParam = useParams();
    const [Loader, setLoader] = useState(false);

    const handleWalletBarcodeDetail = async () => {
        setLoader(true);
        const walletId= urlParam.id;
        // const walletId= window.location.pathname.split('/')[3];
        const response = await walletBarcodeDetail(walletId);
        if (response.status === 200) {
            setLoader(false);
            setWalletDetail(response.data);
            console.log(response.data);
        } else {
            setLoader(false);
        }
    };
    useEffect(() => {
        handleWalletBarcodeDetail();
    }, []);

    const barcodeString = walletDetail.barcode;
    const barcodeStr = barcodeString !== '' && barcodeString !== undefined ? barcodeString : 'DemoBarcode';
    // const barcodeStr ='5MINTAYCWQ';

    return (
        <>
            {Loader === true ? <FullScreenLoader /> : ''}
            <div className='container mt-5'>
                <div className='row payment_method_wrap'>
                    <div className='col-lg-7 mx-auto'>
                        <div className='barcode_header mb-4 mt-5 justify-content-center'>
                            <div className='text-center'>
                                <p>{walletDetail.description ?? ''}<span className={walletDetail.active ? 'badge bg-success ms-2' : 'badge bg-success ms-2'}>{walletDetail.active ? 'Active' : 'Redeemed'}</span>
                                </p>
                                <p>{dateFormatter(walletDetail.walletDate ?? '')}</p>
                            </div>
                        </div>
                        <div className='barcode_img mx-auto text-center'>
                            {walletDetail.active ? '' :
                                <div className='text-center redeem_text'>
                                    {walletDetail.active ? '' : 'Redeemed'}
                                </div>
                            }
                            <div className={walletDetail.active ? '' : 'opacity-25'}>
                                <Barcode value={barcodeStr} width={2} height={60} />
                            </div>
                        </div>
                        <div className='barcode_msg pt-4 pb-3 mt-3 mb-3 border-top text-center'>
                            <p>Make sure to show this code to the attendant or, scan the bar code in the machine</p>
                            <div className='bcm_date'>
                                <img src={calendarImg} alt='calendar' className='calendar' />&nbsp;
                                {dateFormatter(walletDetail.walletDate)}
                            </div>
                        </div>
                        <div className='barcode_img mx-auto text-center mt-4'>
                            {walletDetail.active ? '' :
                                <div className='text-center redeem_text'>
                                    {walletDetail.active ? '' : 'Redeemed'}
                                </div>
                            }
                            <div className={walletDetail.active ? '' : 'opacity-25'}>
                                <QRCode value={barcodeStr} size={200} />
                            </div>
                        </div>
                        <div className='barcode_number text-center p-4'>
                            <small>Your Redeem Code:</small>
                            <h3 className='mt-2'>{barcodeStr}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default TinyWalletBarcode;
