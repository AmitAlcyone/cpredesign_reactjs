import React, {Component} from 'react';
import {Link} from 'react-router-dom';
// import Loader from './../utils/loaders/buttonLoader';
import Layout from '../components/Layout';
import washImg from '../assets/images/icons/wash.svg';
import walletImg from '../assets/images/icons/wallet.svg';
import manageAccountImg from '../assets/images/icons/manage_account.svg';
import policiesImg from '../assets/images/icons/policies.svg';
import {setPreviousPage} from '../store/reducers/auth-reducer';
import {connect} from 'react-redux';

class ManageAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
    }
    managePreviousPage = () => {
        this.props.setPreviousPage();
    };
    render() {
        const {loading} = this.state;

        return (
            <div>
                {!loading &&
                    <Layout>
                        <div className='container'>
                            <div className='row'>
                                <div className='col-12'>
                                    <Link to='/dashboard' className='back_link icon_btn'><span className='icon arrow_left'></span> Back</Link>
                                </div>
                                <div className='col-12'>
                                    <div className='main_title mb-4 mt-3'>
                                        <h2>Your Account</h2>
                                        <p>Manage your account, orders and much more...</p>
                                    </div>
                                </div>
                            </div>
                            <div className='row'>
                                <div className='col-12'>
                                    <div className='services_list'>
                                        <div className='row'>
                                            <div className='col-md-4 col-sm-6 col-12'>
                                                <Link to='/order-history'>
                                                    <div className='service_box'>
                                                        <div className='ser_img'><img src={washImg} className='img-fluid' alt='' /></div>
                                                        <div className='ser_info'>
                                                            <h3>Order History</h3>
                                                            <span>Checkout your orderlist</span>
                                                        </div>
                                                    </div>
                                                </Link>
                                            </div>
                                            <div className='col-md-4 col-sm-6 col-12'>
                                                <Link to='/my-account' onClick={() => this.managePreviousPage()}>
                                                    <div className='service_box'>
                                                        <div className='ser_img'><img src={walletImg} className='img-fluid' alt=''/></div>
                                                        <div className='ser_info'>
                                                            <h3>My Account</h3>
                                                            <span>Manage your account info</span>
                                                        </div>
                                                    </div>
                                                </Link>
                                            </div>
                                            <div className='col-md-4 col-sm-6 col-12'>
                                                <Link to='/payment-options'>
                                                    <div className='service_box'>
                                                        <div className='ser_img'><img src={manageAccountImg} className='img-fluid' alt=''/></div>
                                                        <div className='ser_info'>
                                                            <h3>Payment Options</h3>
                                                            <span>Edit or add payment methods</span>
                                                        </div>
                                                    </div>
                                                </Link>
                                            </div>
                                            <div className='col-md-4 col-sm-6 col-12'>
                                                <Link to='/terms-policies' onClick={() => this.managePreviousPage()}>
                                                    <div className='service_box'>
                                                        <div className='ser_img'><img src={policiesImg} className='img-fluid' alt=''/></div>
                                                        <div className='ser_info'>
                                                            <h3>Policy Program</h3>
                                                            <span>Terms of use, Privacy policy</span>
                                                        </div>
                                                    </div>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Layout>
                }
                {/* {loading && <Loader />} */}
            </div>
        );
    }
}

// export default Dashboard
const mapDispatchToProps = (dispatch) => {
    return {
        setPreviousPage: () => dispatch(setPreviousPage({previousPage: '/manage_account'}))
    };
};

export default connect(null, mapDispatchToProps)(ManageAccount);
