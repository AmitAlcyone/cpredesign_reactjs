import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Layout from '../components/Layout';

class UnlimitedPlan extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <Layout>
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <div className='main_title mb-4 mt-3'>
                                <h2>My Unlimited Wash Program</h2>
                                <p>Your unlimited code: XXXXXXXXXXXXX</p>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-12'>
                            <div className='when_no_ultd_plan'>
                                <div className='no_plan_info'>
                                    <p>You don't have any unlimited wash plan associated with your account.</p>
                                    <div className='btns mt-4'>
                                        <Link to='/add-new-ultd-plan' className='main_btn m-1'>Add New Plan</Link>
                                        <Link to='' className='main_btn m-1' data-bs-toggle='modal' data-bs-target='#addExistingPlan'>Link Existing Plan</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='modal fade cw_modal add_card_pop' id='addExistingPlan' data-bs-backdrop='static' data-bs-keyboard='false' tabIndex='-1' aria-labelledby='addExistingPlanLabel' aria-hidden='true'>
                    <div className='modal-dialog modal-dialog-centered'>
                        <div className='modal-content'>
                            <div className='modal-header'>
                                <h5 className='modal-title' id='addExistingPlanLabel'>Link Existing Plan</h5>
                            </div>
                            <div className='modal-body'>
                                <form action='#'>
                                    <div className='row'>
                                        <div className='col-12'>
                                            <div className='input_wrap mb-4'>
                                                <label htmlFor='fName' className='form-label'>Barcode or License Plate # and CC* <sup className='text-danger'>*</sup></label>
                                                <input type='text' id='fName' className='form-control form-control-lg' placeholder='Enter Code' />
                                            </div>
                                        </div>
                                        <div className='col-12'>
                                            <div className='input_wrap'>
                                                <label htmlFor='lName' className='form-label'>Card Number (Last four digits)* <sup className='text-danger'>*</sup></label>
                                                <input type='text' id='lName' className='form-control form-control-lg' placeholder='0000' />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className='modal-footer justify-content-start'>
                                <button type='button' className='main_btn'>Continue</button>
                                <button type='button' className='sec_btn' data-bs-dismiss='modal'>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        );
    }
}

export default UnlimitedPlan;
