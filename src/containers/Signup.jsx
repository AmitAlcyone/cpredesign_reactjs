import React, {useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import Layout from '../components/Layout';
import logo from '../assets/images/cw_logo.png';
import loginPageImg from '../assets/images/auth_bg.png';
import {toast} from 'react-toastify';
import {useFormik} from 'formik';
import signupSchema from '../validation-schemas/signupSchema';
import InputMask from 'react-input-mask';
import {signupApi, updateRegisterApi, loginByPhoneV2Api} from './../services/auth-service';
import TermsModal from '../components/modals/TermsModal';

const Signup = (props) => {
    toast.configure();
    const [loading, setLoading] = useState(false);
    const [modal, setModal] = useState(false);
    const [modalP, setModalP] = useState(false);
    const navigate = useNavigate();
    const setIsContinue = () => {
        localStorage.setItem('isContinue', true);
    };
    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            phoneNumber: ''
        },
        enableReinitialize: true,
        validationSchema: signupSchema,
        onSubmit: async (values, {resetForm}) => {
            setLoading(true);
            values.phoneNumber = (values.phoneNumber).replace(/([() ])+/g, '');
            values.preferedHomeLocation = '';
            values.preferedWorkLocation = '';
            const response = await signupApi(values);
            if (response.status === 200) {
                if (response.data.message === 'Customer record already exist') {
                    setLoading(false);
                    resetForm();
                    toast.warning(`${response.data.message}.`, {autoClose: 3000});
                }
            } else if (response.status === 201) {
                const reqBody={
                    Firstname: response.data.firstname,
                    Lastname: response.data.lastname,
                    PhoneNumber: response.data.phoneNumber,
                    CustomerIdentityID: response.data.id
                };
                const res = await updateRegisterApi(reqBody);
                if (res.status === 201) {
                    const reqLoginByPhone = await loginByPhoneV2Api(values.phoneNumber);
                    if (reqLoginByPhone.data === undefined); {
                        setLoading(false);
                    }
                    if (reqLoginByPhone.data.message==='OTP Sent') {
                        setLoading(false);
                        localStorage.setItem('phoneNumber', values.phoneNumber);
                        return navigate('/verify-otp');
                    }
                }
            } else {
                toast.error('Some error occurred.', {autoClose: 3000});
                setLoading(false);
            }
        }
    });

    return (
        <Layout>
            {modal ? <TermsModal id={0} closeModal={() => setModal(false)}/> : '' }
            {modalP ? <TermsModal id={1} closeModal={() => setModalP(false)}/> : '' }
            <div className="auth_page">
                <div className="auth_wrap">
                    <div className="auth_left">
                        <div className="auth_form_wrap pt-5">
                            <div className="brand_logo mb-4">
                                <Link to="/"><img src={logo} className="img-fluid" alt="cw-logo" /></Link>
                            </div>
                            <div className="auth_form_sec">
                                <h1 className="form_title mt-5 mb-5">Lets create your account!</h1>
                                <form className="auth_form mb-5" onSubmit={formik.handleSubmit}>
                                    <div className="row">
                                        <div className="col">
                                            <div className="mb-3">
                                                <label htmlFor="firstName" className="form-label">First Name<sup className="text-danger">*</sup></label>
                                                <input type="text" className="form-control form-control-lg" name="firstName" id="firstName"
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    value={formik.values.firstName}
                                                />
                                                {formik.touched.firstName && formik.errors.firstName ? (
                                                    <small className="text-danger validationError">{formik.errors.firstName}</small>
                                                ) : null}
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="mb-3">
                                                <label htmlFor="lastName" className="form-label">Last Name<sup className="text-danger">*</sup></label>
                                                <input type="text" className="form-control form-control-lg" name="lastName" id="lastName"
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    value={formik.values.lastName}
                                                />
                                                {formik.touched.lastName && formik.errors.lastName ? (
                                                    <small className="text-danger validationError">{formik.errors.lastName}</small>
                                                ) : null}
                                            </div>
                                        </div>
                                        <div className="col-12">
                                            <div className="mb-3">
                                                <label htmlFor="phoneNumber" className="form-label">Phone<sup className="text-danger">*</sup></label>
                                                {/* <input type="tel" className="form-control form-control-lg" name="phoneNumber" placeholder="(000) 000 0000"
                                                    id="phoneNumber"
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    value={formik.values.phoneNumber}
                                                /> */}
                                                <InputMask
                                                    className="form-control form-control-lg"
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    mask="(999) 999 9999"
                                                    maskChar=""
                                                    value={formik.values.phoneNumber}
                                                    name="phoneNumber"
                                                    placeholder="(000) 000 0000"
                                                />
                                                {formik.touched.phoneNumber && formik.errors.phoneNumber ? (
                                                    <small className="text-danger validationError">{formik.errors.phoneNumber}</small>
                                                ) : null}
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" className="main_btn" disabled={loading || !formik.isValid} onClick={() => setIsContinue()}>
                                        {loading && <span className="spinner-border text-light btnLoader"></span>}<span>Submit</span>
                                    </button>
                                </form>
                                <div className="switch_form text-center">Already a member? <Link to="/">Login Now!</Link></div>
                                <div className="auth_policy_text text-center mt-3">
                                    <p>By creating your account, you agree to <br />
                                        <Link to="#" onClick={() => setModal(true)}>Conditions of Use,</Link>&nbsp;&nbsp;
                                        <Link to="#" onClick={() => setModalP(true)}>  Privacy Notice</Link></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="auth_right" style={{backgroundImage: `url(${loginPageImg})`}}></div>
                </div>
            </div>
        </Layout>
    );
};

export default Signup;
