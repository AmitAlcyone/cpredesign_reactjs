import React, {useEffect, useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import Layout from '../components/Layout';
import ServiceCardLoader from './../utils/loaders/serviceCardLoader';
import washCardSchema from './../validation-schemas/washCardSchema';
import {Formik} from 'formik';
import {setServices} from '../store/reducers/prepaid-wash-reducer';
import {useDispatch} from 'react-redux';
import {washServices, buyPrepaidBook} from './../services/wash-service';
import {useSelector} from 'react-redux';
import {setFormOtherData, setIsForMySelf} from '../store/reducers/auth-reducer';
import OrderCheckout from '../components/order/order-checkout';
import {checkoutJson} from './../services/auth-service';
import {toast} from 'react-toastify';
import {setBarcodeString} from './../store/reducers/global-reducer';

const WashServices = (props) => {
    toast.configure();
    const navigate = useNavigate();
    const toastId = React.useRef(null);
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const dispatch = useDispatch();
    const services = useSelector((state) => state.prepaidWash.services);
    const washInfo = useSelector((state) => state.prepaidWash.selectedPrepaidCard);
    const isForMySelf = useSelector((state) => state.auth.pageCommonTab.isForMySelf);
    const [lBookTypeId, setlBookTypeId] = useState('');
    const [isLoading, setIsLoading] = useState(services.length ? false : true);
    const orderJson= useState(checkoutJson());
    const [isCheckoutShow, setIsCheckoutShow] = useState(false);
    const [checkAvalableService, setCheckAvalableService] = useState(true);
    const userInfo = useSelector((state) => state.auth.userInfo);
    const phoneNumber = localStorage.getItem('phoneNumber');
    const locationId = localStorage.getItem('locationId');
    const [isBuyNow, setIsBuyNow] = useState(false);
    const [firstName, SetFirstName] = useState('');
    const [lastName, SetLasttName] = useState('');
    const [email, SetEmail] = useState('');
    const customerInfo = JSON.parse(localStorage.getItem('customerDetail'));


    // common tab functionality
    const commonTabs = useSelector((state) => state.auth.pageCommonTab);
    const forOtherTabJson= {
        FirstName: commonTabs.forOther.FirstName,
        LastName: commonTabs.forOther.LastName,
        RecipientEmail: commonTabs.forOther.RecipientEmail,
        Message: commonTabs.forOther.Message
    };

    useEffect(() => {
        const getServies = async () => {
            setCheckAvalableService(true);
            if (services.length === 0) {
                const response = await washServices(userInfo.accessToken, locationId);
                debugger;
                if (response.status === 200) {
                    setTimeout(() => {
                        dispatch(setServices({services: response.data}));
                        setIsLoading(false);
                    }, 1000);
                } else {
                    setCheckAvalableService(false);
                }
            }
        };
        getServies();
        if (washInfo.length === undefined) {
            setlBookTypeId(washInfo.lBookTypeId);
            // setSelectedService(washInfo);
        }
    }, []);

    const innerServicesList = (data) => {
        if (data) {
            const serviceArray = data.sServiceList !== null ? data.sServiceList.split(',') : [];
            return (
                <ul className='wc_service_list scroller'>
                    {serviceArray.map((value, index) => {
                        return <li key={index}>{value}</li>;
                    })}
                </ul>
            );
        }
    };

    const buyNow = (service) => {
        if (!commonTabs.isForMySelf) {
            if (commonTabs.forOther.FirstName.length == 0) {
                if (!toast.isActive(toastId.current)) {
                    toastId.current = toast.error('First name is mandatory.');
                }
                return false;
            } else if (commonTabs.forOther.LastName.length == 0) {
                if (!toast.isActive(toastId.current)) {
                    toastId.current = toast.error('Last name is mandatory.');
                }
                return false;
            } else if (commonTabs.forOther.RecipientEmail.length == 0) {
                if (!toast.isActive(toastId.current)) {
                    toastId.current = toast.error('Recipient email is mandatory.');
                }
                return false;
            } else if (!emailRegex.test(commonTabs.forOther.RecipientEmail)) {
                if (!toast.isActive(toastId.current)) {
                    toastId.current = toast.error('Recipient email should be a valid email.');
                }
                return false;
            }
        }
        setIsBuyNow(true);
        orderJson[0].TopHeaderText= 'Buy a Wash card';
        orderJson[0].SubTotal= service.dblBookPrice;
        orderJson[0].ListItemText= 'Wash Card';
        orderJson[0].ListItemDescription= service.sBookDescription;
        setlBookTypeId(service.lBookTypeId);
    };

    const handleSubmit = (values, {resetForm}) => {
        console.log(values);
    };
    const continueClick = async () => {
        orderJson[0].termsId= 3;
        orderJson[0].GrandTotal= parseFloat(orderJson[0].SubTotal).toFixed(2);
        orderJson[0].Email= '';
        setIsCheckoutShow(true);
    };

    const handleForOtherInputChange = (event, seqNo) =>{
        if (seqNo== 1) {
            forOtherTabJson.FirstName= event.target.value;
        } else if (seqNo== 2) {
            forOtherTabJson.LastName= event.target.value;
        } else if (seqNo== 3) {
            forOtherTabJson.RecipientEmail= event.target.value;
        } else {
            forOtherTabJson.Message= event.target.value;
        }
        dispatch(setFormOtherData({forOther: forOtherTabJson}));
    };

    const washCardCheckout = async (order) =>{
        if (order.IsCheckedTermAndCondition && order.IsCardSelected) {
            const postModel= {};
            postModel.Amount= order.GrandTotal;
            postModel.Alias= order.IsNewCard ? '' : order.SelectedCard.sPayerIdentifier;
            // postModel.Alias = order.IsNewCard ? '' : 'gHIBrcksvL';
            postModel.CreditCardNumber= order.SelectedCard.id;
            postModel.CardHolderName= order.IsNewCard ? order.SelectedCard.CardHolderName : '';
            postModel.ExpiryDate= order.SelectedCard.sExpiredMonth + order.SelectedCard.sExpireYear;
            postModel.CustomerGuid = userInfo.customerGuid;
            postModel.isExisting= !order.IsNewCard;
            postModel.cardType= '';
            postModel.sCompany= '';
            postModel.bSendWash= !commonTabs.isForMySelf;
            postModel.CustomerIdentityId = userInfo.customerIdentityID;
            postModel.BookTypeId = lBookTypeId;
            postModel.sPhone = phoneNumber;
            postModel.sEmail = order.Email !== undefined ? order.Email : customerInfo.customer.sEmail;
            postModel.RecipientEmail = commonTabs.forOther.RecipientEmail;
            postModel.isSendMail = order.IsRecieveEmailReciept;
            postModel.isSendSMS = order.IsRecieveSMSReciept;
            postModel.LocationId = locationId;
            postModel.isDiscountApplied = false;
            setIsLoading(true);
            const response = await buyPrepaidBook(userInfo.accessToken, locationId, postModel);
            setIsLoading(false);
            if (response.status === 200) {
                dispatch(setBarcodeString({barcodeString: response.data.sBarcode}));
                return navigate('/order-success');
            } else {
                toast.error('Something went wrong.', {autoClose: 3000});
                return false;
            }
        } else if (!order.IsCheckedTermAndCondition) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please check term and condition.');
            }
            return false;
        } else if (!order.IsCardSelected) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please select credit card.');
            }
            return false;
        }
    };
    return (
        <Layout>
            {isCheckoutShow ? <OrderCheckout processCheckout={(data) => washCardCheckout(data)} hideCheckoutPage={() => setIsCheckoutShow(false)} order={orderJson} /> :
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <div className='d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap'>
                                <div className='inner_title d-flex align-items-start'>
                                    <div className='progress-circle over50 p50'>
                                        <span>1 of 2</span>
                                        <div className='left-half-clipper'>
                                            <div className='first50-bar'></div>
                                            <div className='value-bar'></div>
                                        </div>
                                    </div>
                                    <div className='main_title'>
                                        <h2>Buy a Wash Services</h2>
                                        <p>Select a Wash Services</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-12'>
                            <ul className='nav custom_tabs mb-3' id='pills-tab' role='tablist'>
                                <li className='nav-item' role='presentation'>
                                    <button className={`nav-link ${commonTabs.isForMySelf ? 'active' : {}}`} id='pills-myself-tab' data-bs-toggle='pill' data-bs-target='#pills-myself' type='button' role='tab' aria-controls='pills-myself' aria-selected='true' onClick={() => {
                                        dispatch(setIsForMySelf({isForMySelf: true}));
                                    }}>For Myself</button>
                                </li>
                                <li className='nav-item' role='presentation'>
                                    <button className={`nav-link ${commonTabs.isForMySelf ? {} : 'active'}`} id='pills-others-tab' data-bs-toggle='pill' data-bs-target='#pills-others' type='button' role='tab' aria-controls='pills-others' aria-selected='false' onClick={() => {
                                        dispatch(setIsForMySelf({isForMySelf: false}));
                                    }}>For Others</button>
                                </li>
                            </ul>
                            <div className='tab-content' id='pills-tabContent' style={{minHeight: 'auto'}}>
                                <div className={`tab-pane fade ${commonTabs.isForMySelf ? 'show active' : {}}`} aria-labelledby='pills-myself-tab'></div>
                                <div className={`tab-pane fade ${commonTabs.isForMySelf ? {} : 'show active'}`} id='pills-others' role='tabpanel' aria-labelledby='pills-others-tab'>
                                    <div className='for_other_form p-3 mb-3'>
                                        <Formik
                                            initialValues={{
                                                firstname: commonTabs.forOther.FirstName,
                                                lastname: commonTabs.forOther.LastName,
                                                email: commonTabs.forOther.RecipientEmail
                                            }}
                                            onSubmit={(values, {resetForm}) => {
                                                handleSubmit(values, {resetForm});
                                            }}
                                            validationSchema={washCardSchema}
                                        >
                                            {({values,
                                                errors,
                                                touched,
                                                handleChange,
                                                handleBlur,
                                                handleSubmit,
                                                isValid
                                            }) => (
                                                <form action="#" onSubmit={handleSubmit}>
                                                    <div className='row'>
                                                        <div className='col-12 col-sm-6 col-md-2'>
                                                            <div className='input_wrap'>
                                                                <label htmlFor='fName' className='form-label'>First Name <sup className='text-danger'>*</sup></label>
                                                                <input type='text' id='fName' className='form-control form-control-lg' placeholder=''
                                                                    name="firstname"
                                                                    onChange={(event) => {
                                                                        handleChange(event);
                                                                        handleForOtherInputChange(event, 1);
                                                                        // eslint-disable-next-line new-cap
                                                                        SetFirstName(event.target.value);
                                                                    }}
                                                                    onBlur={handleBlur}
                                                                    value={forOtherTabJson.FirstName}
                                                                />
                                                                {touched.firstname && errors.firstname ? (
                                                                    <small className="text-danger">{errors.firstname}</small>
                                                                ) : null}
                                                            </div>
                                                        </div>
                                                        <div className='col-12 col-sm-6 col-md-2'>
                                                            <div className='input_wrap'>
                                                                <label htmlFor='lName' className='form-label'>Last Name <sup className='text-danger'>*</sup></label>
                                                                <input type='text' id='lName' className='form-control form-control-lg' placeholder=''
                                                                    name="lastname"
                                                                    onChange={(event) => {
                                                                        handleChange(event), handleForOtherInputChange(event, 2);
                                                                        // eslint-disable-next-line new-cap
                                                                        SetLasttName(event.target.value);
                                                                    }}
                                                                    onBlur={handleBlur}
                                                                    value={forOtherTabJson.LastName}
                                                                />
                                                                {touched.lastname && errors.lastname ? (
                                                                    <small className="text-danger">{errors.lastname}</small>
                                                                ) : null}
                                                            </div>
                                                        </div>
                                                        <div className='col-12 col-sm-6 col-md-4'>
                                                            <div className='input_wrap'>
                                                                <label htmlFor='repEmail' className='form-label'>Recipient Email <sup className='text-danger'>*</sup></label>
                                                                <input type='email' id='repEmail' className='form-control form-control-lg' placeholder=''
                                                                    name="email"
                                                                    onChange={(event) => {
                                                                        handleChange(event), handleForOtherInputChange(event, 3);
                                                                        // eslint-disable-next-line new-cap
                                                                        SetEmail(event.target.value);
                                                                    }}
                                                                    onBlur={handleBlur}
                                                                    value={forOtherTabJson.RecipientEmail}
                                                                />
                                                                {touched.email && errors.email ? (
                                                                    <small className="text-danger">{errors.email}</small>
                                                                ) : null}
                                                            </div>
                                                        </div>
                                                        <div className='col-12 col-sm-6 col-md-4'>
                                                            <div className='input_wrap'>
                                                                <label htmlFor='message' className='form-label'>Your Message</label>
                                                                <input type='email' id='message' className='form-control form-control-lg' placeholder='' value={forOtherTabJson.Message} onChange={(event) => {
                                                                    handleForOtherInputChange(event, 4);
                                                                }}/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            )}
                                        </Formik>
                                    </div>
                                </div>
                            </div>
                            {isLoading &&
                        <div className='row wash_list'>
                            <ServiceCardLoader></ServiceCardLoader>
                            <ServiceCardLoader></ServiceCardLoader>
                            <ServiceCardLoader></ServiceCardLoader>
                        </div>
                            }
                            <div className='row wash_list'>
                                {services.map((service, index) => {
                                    debugger;
                                    if (service.iWashQty === 1 && service.dblBookPrice !== 0) {
                                        return (
                                            <div className='col-md-4 col-sm-6 col-12 ' key={service.lBookTypeId}>
                                                <div className={`wc_box ${lBookTypeId === service.lBookTypeId ? 'wc_selected' : ''}`}>
                                                    <div className='wc_info '>
                                                        <h3>{service.sBookDescription}</h3>
                                                        <span>{service.sSubBookDescription != null ? service.sSubBookDescription : ''} </span>
                                                        {/* <span>${service.dblBookPrice.toFixed(2)} for a single Extreme wash</span> */}
                                                        {innerServicesList(service)}
                                                    </div>
                                                    <div className='wc_foot'>
                                                        <span className='wc_prices'>${service.dblBookPrice.toFixed(2)}</span>
                                                        <Link to='#' onClick={() => buyNow(service)}>{lBookTypeId === service.lBookTypeId ? 'Added!' : 'Buy Now'}</Link>
                                                    </div>
                                                </div>
                                            </div>
                                        );
                                    }
                                })}
                                {!checkAvalableService && isLoading === false ? <p className='notFound'>Services not available.</p> : ''}
                            </div>
                            <div className='d-flex justify-content-end mt-3 pt-3 border-top'>
                                <div className='btn_grps d-flex flex-wrap'>
                                    <Link className='back_btn icon_btn me-2' to='/'><span className='icon arrow_left'></span> Back</Link>
                                    {isForMySelf ?
                                        <button className='main_btn icon_btn ' disabled={isBuyNow ? false : true} onClick={() => continueClick()}>Continue<span className='icon arrow_right_white'></span></button> :
                                        <button className='main_btn icon_btn ' disabled={isBuyNow && firstName !== '' && lastName !== '' && email !== '' && emailRegex.test(email) ? false : true} onClick={() => continueClick()}>Continue<span className='icon arrow_right_white'></span></button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </Layout>
    );
};

export default WashServices;
