import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import Layout from '../components/Layout';
import walletImg from './../assets/images/icons/wallet.svg';
import calendarImg from './../assets/images/calendar.png';
import ServiceCardLoader from './../utils/loaders/serviceCardLoader';
import {setwalletServices, setSelectedWallet} from '../store/reducers/wallet-reducer';
import {useDispatch} from 'react-redux';
import {wallet} from './../services/wallet-service';
import {useSelector} from 'react-redux';
import {toast} from 'react-toastify';
import dateFormatter from '../utils/dateFormatter';


const Wallet = (props) => {
    const userInfo = useSelector((state) => state.auth.userInfo);
    const services = useSelector((state) => state.wallet.services);
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(services.length ? false : true);
    const [selectMyself, setSelectedMyself] = useState(false);
    const [selectForSomeone, setSelectedForSomeone] = useState(false);
    const [pageServices, setPageServices] = useState([]);
    const [filterType, setFilterType] = useState('All');
    useEffect(() => {
        const getServices = async () => {
            setPageServices(services);
            if (services.length === 0) {
                const response = await wallet(userInfo.accessToken, userInfo.customerIdentityID);
                // console.log(response);
                if (response.status === 200) {
                    setTimeout(() => {
                        setIsLoading(false);
                        const shortWalletData= response.data.sort((a, b) => new Date(b.walletDate) - new Date(a.walletDate));
                        dispatch(setwalletServices({services: shortWalletData}));
                        setPageServices(shortWalletData);
                    }, 1000);
                } else {
                    setIsLoading(false);
                    toast.error('Something went wrong.', {autoClose: 3000});
                }
            }
        };
        getServices();
    }, []);

    const handleFilters = (filter, cardFor) => {
        if (filter == '') {
            filter = filterType;
        }
        setSelectedMyself(false);
        setSelectedForSomeone(false);
        if (filter == 'All') {
            setFilterType('All');
            if (cardFor == '') {
                const filteredServices = services;
                setPageServices(filteredServices);
            }
            if (cardFor == 'mySelf') {
                setSelectedMyself(true);
                setSelectedForSomeone(false);
                const filteredServices = services.filter((curElem) => {
                    return curElem.forSomeOneElse === false;
                });
                setPageServices(filteredServices);
                return;
            } else if (cardFor == 'someOneElse') {
                setSelectedMyself(false);
                setSelectedForSomeone(true);
                const filteredServices = services.filter((curElem) => {
                    return curElem.forSomeOneElse === true;
                });
                setPageServices(filteredServices);
                return;
            }
        } else if (filter == 'WashCard') {
            setFilterType('WashCard');
            if (cardFor == '') {
                const filteredServices = services.filter((curElem) => {
                    return curElem.type === 0;
                });
                setPageServices(filteredServices);
                return;
            }
            if (cardFor == 'mySelf') {
                setSelectedMyself(true);
                setSelectedForSomeone(false);
                const filteredServices = services.filter((curElem) => {
                    return (curElem.type === 0 && curElem.forSomeOneElse === false);
                });
                setPageServices(filteredServices);
                return;
            } else if (cardFor == 'someOneElse') {
                setSelectedMyself(false);
                setSelectedForSomeone(true);
                const filteredServices = services.filter((curElem) => {
                    return (curElem.type === 0 && curElem.forSomeOneElse === true);
                });
                setPageServices(filteredServices);
                return;
            }
        } else {
            setFilterType('GiftCard');
            if (cardFor == '') {
                const filteredServices = services.filter((curElem) => {
                    return curElem.type === 1;
                });
                setPageServices(filteredServices);
                return;
            }
            if (cardFor == 'mySelf') {
                setSelectedMyself(true);
                setSelectedForSomeone(false);
                const filteredServices = services.filter((curElem) => {
                    return (curElem.type === 1 && curElem.forSomeOneElse === false);
                });
                setPageServices(filteredServices);
                return;
            } else if (cardFor == 'someOneElse') {
                setSelectedMyself(false);
                setSelectedForSomeone(true);
                const filteredServices = services.filter((curElem) => {
                    return (curElem.type === 1 && curElem.forSomeOneElse === true);
                });
                setPageServices(filteredServices);
                return;
            }
        }
    };

    const handleSelectedWallet = (wallet) => {
        const upDatedWallet={...wallet, pageBackUrl: '/wallet'};
        dispatch(setSelectedWallet({selectedWallet: upDatedWallet}));
    };

    return (
        <Layout>
            <div className='container'>
                <div className='row'>
                    <div className='col-12'>
                        <Link to='/dashboard' className='back_link icon_btn'><span className='icon arrow_left'></span> Back</Link>
                    </div>
                    <div className='col-12'>
                        <div className='d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap'>
                            <div className='main_title'>
                                <h2>Wallet</h2>
                                <p>Manage your wallet activity</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12'>
                        <ul className='nav custom_tabs mb-3' id='pills-tab' role='tablist'>
                            <li className='nav-item' role='presentation'>
                                <button className='nav-link active' id='pills-all-tab' data-bs-toggle='pill' data-bs-target='#pills-all' type='button' role='tab' aria-controls='pills-all' aria-selected='true' onClick={() => handleFilters('All', '')}>All</button>
                            </li>
                            <li className='nav-item' role='presentation'>
                                <button className='nav-link' id='pills-washcard-tab' data-bs-toggle='pill' data-bs-target='#pills-washcard' type='button' role='tab' aria-controls='pills-washcard' aria-selected='true' onClick={() => handleFilters('WashCard', '')}>Wash Card</button>
                            </li>
                            <li className='nav-item' role='presentation'>
                                <button className='nav-link' id='pills-giftcard-tab' data-bs-toggle='pill' data-bs-target='#pills-giftcard' type='button' role='tab' aria-controls='pills-giftcard' aria-selected='false' onClick={() => handleFilters('GiftCard', '')}>Gift Card</button>
                            </li>
                            <li className='nav-item ms-auto'>
                                <div className='sw_btn_grp d-flex'>
                                    {/* <Link to='#' className='nav-link'>My Self</Link>*/}
                                    {/* <Link to='#' className='nav-link'>For someone else</Link>*/}
                                    <button className={`nav-link ${selectMyself ? 'active' : ''}`} to='#' onClick={() => handleFilters('', 'mySelf')}>For Myself</button>
                                    <button className={`nav-link ${selectForSomeone ? 'active' : ''}`} to='#' onClick={() => handleFilters('', 'someOneElse')}>For Others</button>
                                </div>
                            </li>
                        </ul>
                        <div className='tab-content' id='pills-tabContent' style={{minHeight: 'auto'}}>
                            {services.length === 0 && isLoading === false ? <p className='notFound'>Services not available.</p> : ''}
                            <div className='tab-pane fade show active' id='pills-all' role='tabpanel' aria-labelledby='pills-all-tab'></div>
                            <div className='tab-pane fade' id='pills-washcard' role='tabpanel' aria-labelledby='pills-washcard-tab'></div>
                            <div className='tab-pane fade' id='pills-giftcard' role='tabpanel' aria-labelledby='pills-giftcard-tab'></div>
                        </div>
                        {isLoading &&
                            <div className='row wash_list'>
                                <ServiceCardLoader></ServiceCardLoader>
                                <ServiceCardLoader></ServiceCardLoader>
                                <ServiceCardLoader></ServiceCardLoader>
                            </div>
                        }
                        <div className='col-lg-12'>
                            <div className='row wallet_data'>
                                {
                                    pageServices.map((service, index) => {
                                        // debugger;
                                        // const [date, time] = (service.walletDate !== null ? service.walletDate.split('T') : '');
                                        return (
                                            <div className='col-md-4 col-sm-6 col-12' key={index}>
                                                <Link to='/wallet-barcode' onClick={() => handleSelectedWallet(service)}>
                                                    <div className={`service_box wallet_item status_badge ${service.active === true ? 'st_active' : 'st_redeemed'}`}>
                                                        <div className='ser_img'><img src={walletImg} className='img-fluid' alt='' />
                                                        </div>
                                                        <div className='ser_info'>
                                                            <h3>{service.description}</h3>
                                                            <img src={calendarImg} alt='calendar' className='calendar' />&nbsp;
                                                            {/* {date}&nbsp;&nbsp;{time != null ? time.slice(0, 5) : ''} */}
                                                            {dateFormatter(service.walletDate)}
                                                        </div>
                                                    </div>
                                                </Link>
                                            </div>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default Wallet;
