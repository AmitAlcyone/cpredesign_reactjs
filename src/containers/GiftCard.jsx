import React, {useState, useEffect} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import Layout from '../components/Layout';
import SkeletonGiftCardTable from '../utils/skeletons/SkeletonGiftCardTable';
import {useDispatch, useSelector} from 'react-redux';
import {setSelectedGiftCard} from '../store/reducers/gift-card-reducer';
import dateFormatter from '../utils/dateFormatter';
import {giftCardList} from './../services/gift-card-service';
import {setGiftCards, setGiftCardTab} from './../store/reducers/gift-card-reducer';
import {setIsForMySelf} from '../store/reducers/auth-reducer';
import {isNumeric} from '../utils/common-repo';

const GiftCard = (props) => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const userInfo = useSelector((state) => state.auth.userInfo);
    const giftCards = useSelector((state) => state.giftCard.giftCards);
    const [isLoading, setIsLoading] = useState(giftCards.length ? false : true);
    const [giftCardServices, setgiftCardServices] = useState([]);
    // set redux to init state
    const addGiftCardTabData= {
        isForMySelf: true,
        forOther: {
            FirstName: '',
            LastName: '',
            RecipientEmail: '',
            Message: ''
        }
    };
    const contentLoader = () => {
        const content = [];
        for (let index = 0; index < 10; index++) {
            content.push(<SkeletonGiftCardTable key={index} page={{GiftCardTable: true}} />);
        }
        return content;
    };

    const viewCard = (param) => {
        const selectedGiftCard = {
            dblBalance: param.dblBalance,
            sGiftCardNumber: param.sGiftCard_Number,
            dtCreated: dateFormatter(param.dtCreated),
            giftCardGuid: param.giftCardGUID
        };
        dispatch(setSelectedGiftCard({selectedGiftCard}));
        return navigate('/gift-card-details');
    };

    useEffect(() => {
        const getGiftCardList = async () => {
            setgiftCardServices(giftCards);
            if (giftCards.length === 0) {
                const response = await giftCardList(userInfo.accessToken, userInfo.customerGuid);
                if (response.status === 200) {
                    setTimeout(() => {
                        setIsLoading(false);
                        dispatch(setGiftCards({giftCards: response.data}));
                        setgiftCardServices(response.data);
                    }, 1000);
                } else {
                    setIsLoading(false);
                }
            }
        };
        getGiftCardList();
    }, []);
    const filterAllItem = () => {
        const filteredGiftCard = giftCards;
        setgiftCardServices(filteredGiftCard);
        setGiftCards(filteredGiftCard);
    };

    const filterMyselfItem = (bSendasGift) => {
        const filteredGiftCard = giftCards.filter((curElem) => {
            return curElem.bSendasGift === bSendasGift;
        });
        setgiftCardServices(filteredGiftCard);
        setGiftCards(filteredGiftCard);
    };
    const filterOthersItem = (bSendasGift) => {
        debugger;
        const filteredGiftCard = giftCards.filter((curElem) => {
            return curElem.bSendasGift === bSendasGift;
        });
        setgiftCardServices(filteredGiftCard);
        setGiftCards(filteredGiftCard);
    };
    return (
        <Layout>
            <div className='container'>
                <div className='row'>
                    <div className='col-12'>
                        <Link to='/dashboard' className='back_link icon_btn'><span className='icon arrow_left'></span> Back</Link>
                    </div>
                    <div className='col-12'>
                        <div className='d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap'>
                            <div className='main_title'>
                                <h2>Gift Cards</h2>
                                <p>Manage your gift card activity</p>
                            </div>
                            <Link to='/add-gift-card' className='main_btn' onClick={() => dispatch(setGiftCardTab({giftCardTabs: addGiftCardTabData}))}>+ Add a New Card</Link>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12'>
                        <ul className='nav custom_tabs mb-3' id='pills-tab' role='tablist'>
                            <li className='nav-item' role='presentation'>
                                <button className='nav-link active' id='pills-all-tab' data-bs-toggle='pill' data-bs-target='#pills-all' type='button' role='tab' aria-controls='pills-all' aria-selected='true' onClick={()=>filterAllItem()}>All</button>
                            </li>
                            <li className='nav-item' role='presentation'>
                                <button className='nav-link' id='pills-myself-tab' data-bs-toggle='pill' data-bs-target='#pills-myself' type='button' role='tab' aria-controls='pills-myself' aria-selected='true' onClick={()=>filterMyselfItem(false)}>For Myself</button>
                            </li>
                            <li className='nav-item' role='presentation'>
                                <button className='nav-link' id='pills-others-tab' data-bs-toggle='pill' data-bs-target='#pills-others' type='button' role='tab' aria-controls='pills-others' aria-selected='false' onClick={()=>filterOthersItem(true)}>For Others</button>
                            </li>
                        </ul>
                        <div className='tab-content' id='pills-tabContent' style={{minHeight: 'auto'}}>
                            <div className='tab-pane fade show active' id='pills-all' role='tabpanel' aria-labelledby='pills-all-tab'></div>
                            <div className='tab-pane fade ' id='pills-myself' role='tabpanel' aria-labelledby='pills-myself-tab'></div>
                            <div className='tab-pane fade ' id='pills-others' role='tabpanel' aria-labelledby='pills-others-tab'></div>
                        </div>
                        <div className='row'>
                            <div className='col-lg-12'>
                                <div className='pay_method_list'>
                                    <div className='table-responsive border-bottom'>
                                        <table className='table ccards_table table-borderless'>
                                            <thead>
                                                <tr>
                                                    <th>Gift card details</th>
                                                    <th>Card Info</th>
                                                    <th>Date</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {isLoading && contentLoader()}
                                                {giftCardServices.map((cardItem, index) => {
                                                    // const [date, time] = (cardItem.dtCreated !== null ? cardItem.dtCreated.split('T') : '');
                                                    return (
                                                        <tr key={index} >
                                                            <td><div className="gift_card_name" >${isNumeric(cardItem.dblBalance) ? (cardItem.dblBalance).toFixed(2) : cardItem.dblBalance} - Gift Card</div></td>
                                                            <td><div className="noc">Ending in {cardItem.sGiftCard_Number.substr(- 4)}</div></td>
                                                            {/* <td><div className="exp_date">{date}&nbsp;&nbsp;{time != null ? time.slice(0, 5) : ''}</div></td>*/}
                                                            <td><div className="exp_date">{dateFormatter(cardItem.dtCreated)}</div></td>
                                                            <td><span to='#' onClick={() => {
                                                                dispatch(setIsForMySelf({isForMySelf: true}));
                                                                viewCard(cardItem);
                                                            }} className="view_btn"><span className="icon eye_open_icon" title="Card Details"></span></span></td>
                                                        </tr>
                                                    );
                                                }) }
                                            </tbody>
                                        </table>
                                        {giftCards.length === 0 && isLoading === false ? <p className='notFound'>Gift cards not available.</p> : ''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default GiftCard;
