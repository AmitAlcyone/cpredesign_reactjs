import React from 'react';
import {Link} from 'react-router-dom';
import Layout from '../components/Layout';

const NewWashCard = (props) => {
    return (
        <Layout>
            <div className='container'>
                <div className='row'>
                    <div className='col-12'>
                        <Link to='/dashboard' className='back_link icon_btn'><span className='icon arrow_left'></span> Back</Link>
                    </div>
                    <div className='col-12'>
                        <div className='d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap'>
                            <div className='main_title'>
                                <h2>Wash Cards</h2>
                                <p>Manage your wash card activity</p>
                            </div>
                            <Link to='/wash-card' className='main_btn'>+ Add a New Wash Card</Link>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12'>
                        <ul className='nav custom_tabs mb-3' id='pills-tab' role='tablist'>
                            <li className='nav-item' role='presentation'>
                                <button className='nav-link active' id='pills-all-tab' data-bs-toggle='pill' data-bs-target='#pills-all' type='button' role='tab' aria-controls='pills-all' aria-selected='true'>All</button>
                            </li>
                            <li className='nav-item' role='presentation'>
                                <button className='nav-link' id='pills-myself-tab' data-bs-toggle='pill' data-bs-target='#pills-myself' type='button' role='tab' aria-controls='pills-myself' aria-selected='true'>For Myself</button>
                            </li>
                            <li className='nav-item' role='presentation'>
                                <button className='nav-link' id='pills-others-tab' data-bs-toggle='pill' data-bs-target='#pills-others' type='button' role='tab' aria-controls='pills-others' aria-selected='false'>For Others</button>
                            </li>
                        </ul>
                        <div className='tab-content' id='pills-tabContent' style={{minHeight: 'auto'}}>
                            <div className='tab-pane fade show active' id='pills-all' role='tabpanel' aria-labelledby='pills-all-tab'></div>
                            <div className='tab-pane fade ' id='pills-myself' role='tabpanel' aria-labelledby='pills-myself-tab'></div>
                            <div className='tab-pane fade ' id='pills-others' role='tabpanel' aria-labelledby='pills-others-tab'></div>
                        </div>
                        <div className='row'>
                            <div className='col-lg-12'>
                                <div className='pay_method_list'>
                                    <div className='table-responsive border-bottom'>
                                        <table className='table ccards_table table-borderless'>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default NewWashCard;
