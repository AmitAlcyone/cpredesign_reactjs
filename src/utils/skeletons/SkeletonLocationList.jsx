import React from 'react';
import SkeletonElement from './SkeletonElement';

const SkeletonLocationList = () => {
    return (
        <div className="accordion table-row-skeleton">
            <div className='accordion-item'>
                <h2 className='accordion-header'>
                    <button className='accordion-button collapsed' type='button'>
                        <div className='loc_box d-flex align-items-center'>
                            <span className='icon mappin_icon me-3'></span>
                            <div className='loc_info'>
                                <div className='loc_name'>Location's Loading....</div><SkeletonElement />
                            </div>
                        </div>
                    </button>
                </h2>
            </div>
        </div>
    );
};

export default SkeletonLocationList;
