import React from 'react';
import './../../assets/css/skeleton.css';
import 'react-loading-skeleton/dist/skeleton.css';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';

const SkeletonElement = ({type}) => {
    const classes = `skeleton ${type}`;

    return (
        <SkeletonTheme highlightColor="#e0dfdf">
            <div className={classes}>
                <Skeleton />
            </div>
        </SkeletonTheme>
    );
};

export default SkeletonElement;
