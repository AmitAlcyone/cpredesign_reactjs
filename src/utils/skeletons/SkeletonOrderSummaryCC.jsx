import React from 'react';
import SkeletonElement from './SkeletonElement';

const SkeletonOrderSummaryCC = () => {
    return (
        <tr className="gift-card-table table-row-skeleton payment-method-cc">
            {/* <td><SkeletonElement type="text" /></td> */}
            <td><SkeletonElement type="text" /></td>
            <td><SkeletonElement type="text" /></td>
        </tr>
    );
};

export default SkeletonOrderSummaryCC;
