import React from 'react';
import loaderImg from './../../assets/images/inkling_spinner.jpg';

const ButtonLoader = () => {
    const imgStyle = {
        width: '60px',
        height: '60px'
    };

    return (
        <span><img style={imgStyle} src={loaderImg} alt='loading' />Loading</span>
    );
};

export default ButtonLoader;
