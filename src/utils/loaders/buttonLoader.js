import React from 'react';
import LoaderImg from './../../assets/images/inkling_spinner.jpg';

const ButtonLoader = () => {
    return (
        <span><img style={{width: '60px', height: '60px'}} src={LoaderImg} alt='loading' />  Loading</span>
    );
};

export default ButtonLoader;
