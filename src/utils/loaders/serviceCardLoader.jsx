import React from 'react';
import ContentLoader from 'react-content-loader';

const ServiceCardLoader = (props) => {
    if (props!= undefined) {
        const width= props.data== undefined ? 360 : props.data.width;
        const height= props.data== undefined ? 160 : props.data.height;
        const isApplyWCBoxClass= props.data == undefined ? true : props.data.wcBox;
        return (
            <div className={`${props.data == undefined ? 'col-md-4 col-sm-6 col-12' : props.data.class}`} style={{display: props.data == undefined ? 'block' : props.data.isLoad ? 'block' : 'none'}}>
                <div className={`${isApplyWCBoxClass ? 'wc_box' : ''}`}>
                    <ContentLoader
                        speed={2}
                        width={width}
                        height={height}
                        viewBox="0 0 360 160"
                        backgroundColor="#f3f3f3"
                        foregroundColor="#e0e0e0"
                        {...props}
                    >
                        <rect x="15" y="15" rx="6" ry="6" width="150" height="8" />
                        <rect x="15" y="60" rx="3" ry="3" width="88" height="6" />
                        <rect x="15" y="32" rx="6" ry="6" width="250" height="8" />
                        <rect x="15" y="72" rx="3" ry="3" width="88" height="6" />
                        <rect x="15" y="84" rx="3" ry="3" width="88" height="6" />
                        <rect x="15" y="132" rx="4" ry="4" width="88" height="7" />
                        <rect x="247" y="132" rx="4" ry="4" width="88" height="7" />
                    </ContentLoader>
                </div>
            </div>
        );
    } else {
        return (
            <div className='col-md-4 col-sm-6 col-12' style='block'>
                <div className='wc_box'>
                    <ContentLoader
                        speed={2}
                        width={360}
                        height={160}
                        viewBox="0 0 360 160"
                        backgroundColor="#f3f3f3"
                        foregroundColor="#e0e0e0"
                        {...props}
                    >
                        <rect x="15" y="15" rx="6" ry="6" width="150" height="8" />
                        <rect x="15" y="60" rx="3" ry="3" width="88" height="6" />
                        <rect x="15" y="32" rx="6" ry="6" width="250" height="8" />
                        <rect x="15" y="72" rx="3" ry="3" width="88" height="6" />
                        <rect x="15" y="84" rx="3" ry="3" width="88" height="6" />
                        <rect x="15" y="132" rx="4" ry="4" width="88" height="7" />
                        <rect x="247" y="132" rx="4" ry="4" width="88" height="7" />
                    </ContentLoader>
                </div>
            </div>
        );
    }
};

export default ServiceCardLoader;
