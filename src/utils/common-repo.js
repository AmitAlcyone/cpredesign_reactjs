import {getPCIDetails} from '../services/order-service';

function GetCreditCardIcon(card) {
    if (card.sBin!= null && card.sBin !='') {
        switch (card.sBin) {
        case '3':
            // return 'AMEX';
            return {name: 'American Express', icon: ''};
        case '4':
            return {name: 'Visa', icon: 'visa_icon'};
        case '5':
            return {name: 'Master', icon: 'mastercard_icon'};
        case '6':
            return {name: 'Discover', icon: 'discover_icon'};
        default:
            return {name: 'Visa', icon: 'visa_icon'};
        }
    } else {
        return {name: 'Visa', icon: 'visa_icon'};
    }
}

const fetchPCIDetailsFromServer = async (toast) => {
    const response = await getPCIDetails();
    if (response.status == 200) {
        localStorage.setItem('pciDetails', JSON.stringify(response.data));
    } else {
        delete localStorage['pciDetails'];
        toast.error('Something went wrong, When fetching PCI details.');
    }
};

function isStringNullOrEmpty(str) {
    return (str == null || str === '' || str.length === 0);
}

function getPCIDetailsLocal() {
    const PCIJson= {
        PCIScript: '',
        XGPApiKey: '',
        Enviornment: '',
        isPaymentLive: false
    };
    if (localStorage.getItem('pciDetails') != null && localStorage.getItem('pciDetails') != undefined && localStorage.getItem('pciDetails') !='null') {
        const details= JSON.parse(localStorage.getItem('pciDetails'));
        PCIJson.PCIScript= details.PCIScript;
        PCIJson.XGPApiKey= details.XGPApiKey;
        PCIJson.Enviornment= details.Enviornment;
        PCIJson.isPaymentLive= details.isPaymentLive;
    }
    return PCIJson;
}

function getNoLoginCheckoutJSON() {
    return {
        TopHeaderText: '',
        ListItemText: '',
        ListItemDescription: '',
        // SelectedCard: {},
        IsCheckedTermAndCondition: false,
        IsShowPromocodeDiv: false,
        IsPromocodeApplied: false,
        IsRecieveSMSReciept: false,
        IsShowRecieveSMSReciept: true,
        IsRecieveEmailReciept: false,
        IsShowRecieveEmailReciept: true,
        IsSavePaymentInfoForFuturePurchase: false,
        MobileNo: '',
        termsId: 0,
        // IsUWCRequest: false,
        // IsUpgradeULPlan: false, // UL for Unlimited plans
        // ULPlanData: {
        //     CurrentPlan: '',
        //     CurrentRecurringPayment: 0,
        //     NewSelectedPlan: '',
        //     NewRecuringPayment: ''
        // },
        Promocode: '',
        TaxAmount: 0.0,
        DiscountAmount: 0.0,
        SubTotal: 0.0,
        GrandTotal: 0.0,
        AdditionalPageData: null
    };
};

function quickBuyGiftCardModel(isSelected, isTaxable, price, productTypeId, description, taxAmount) {
    const jsonData = {};
    jsonData.IsSelected = isSelected;
    jsonData.IsTaxable = isTaxable;
    jsonData.Amount =parseFloat(price) > 0 ? parseFloat(price).toFixed(2) : '';
    jsonData.TaxAmount =parseFloat(taxAmount).toFixed(2);
    jsonData.ProductTypeId = productTypeId;
    jsonData.Description = description;

    return jsonData;
}

function giftCardModel(isSelected, isTaxable, price, surchargeId, description, other, customerGuid) {
    const jsonData = {};
    jsonData.IsSelected = isSelected;
    jsonData.IsTaxable = isTaxable;
    jsonData.Other = parseFloat(other) > 0 ? parseFloat(other).toFixed(2) : '';
    jsonData.Amount =parseFloat(price) > 0 ? parseFloat(price).toFixed(2) : '';
    jsonData.SurchargeId = surchargeId;
    jsonData.CustomerGuid = customerGuid;
    jsonData.Description = description;

    return jsonData;
}

function isNumeric(str) {
    return /^-?\d+$/.test(str);
}

export {GetCreditCardIcon, getPCIDetailsLocal, fetchPCIDetailsFromServer, getNoLoginCheckoutJSON, quickBuyGiftCardModel, giftCardModel, isNumeric, isStringNullOrEmpty};
