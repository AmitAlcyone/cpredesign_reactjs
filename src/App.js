import './assets/css/main.css';
import './assets/css/media.css';
import 'react-toastify/dist/ReactToastify.css';
import {Route, Routes} from 'react-router-dom';
import GiftCard from './containers/GiftCard';
import Locations from './containers/Locations';
import Dashboard from './containers/Dashboard';
import UnlimitedPlan from './components/manageAccount/MyAccount';
import WashServices from './containers/WashServices';
import ManageAccount from './containers/ManageAccount';
import Wallet from './containers/Wallet';
import Login from './containers/Login';
import Verification from './containers/Verification';
import WashCard from './containers/WashCard';
import NewWashCard from './containers/NewWashCard';
import PrepaidWashPlans from './containers/PrepaidWashPlans';
import DetailingServices from './containers/DetailingServices';
import PrivateRoute from './utils/routes/PrivateRoute';
import Signup from './containers/Signup';
import OrderHistory from './components/manageAccount/OrderHistory';
import MyAccount from './components/manageAccount/MyAccount';
import PaymentOptions from './components/manageAccount/PaymentOptions';
import Policies from './components/manageAccount/Policies';
import CancelMembership from './components/manageAccount/CancelMembership';
import AddGiftCard from './components/giftCard/AddGiftCard';
import GiftCardDetails from './components/giftCard/GiftCardDetails';
import MergeGiftCard from './components/giftCard/MergeGiftCard';
import OrderSummmary from './components/order/OrderSummary';
import OrderSuccess from './components/order/OrderSuccess';
import WalletBarcode from './components/order/WalletBarcode';
import ErrorPage from './containers/ErrorPage';

const App = () => {
    return (
        <Routes>
            <Route path="/verify-otp" element={<Verification />} />
            <Route path="/signup" element={<Signup />} />
            <Route path="/error-page" element={<ErrorPage />} />
            <Route path="/dashboard" element={
                <PrivateRoute>
                    <Dashboard />
                </PrivateRoute>
            } />
            <Route path="/wash_services" element={
                <PrivateRoute>
                    <WashServices />
                </PrivateRoute>
            } />
            <Route path="/gift_card" element={
                <PrivateRoute>
                    <GiftCard />
                </PrivateRoute>
            } />
            <Route path="/unlimited_wash_club_box" element={
                <PrivateRoute>
                    <UnlimitedPlan />
                </PrivateRoute>
            } />
            <Route path="/find_location_btn" element={
                <PrivateRoute>
                    <Locations />
                </PrivateRoute>
            } />
            <Route path="/manage_account" element={
                <PrivateRoute>
                    <ManageAccount />
                </PrivateRoute>
            } />
            <Route path="/wallet" element={
                <PrivateRoute>
                    <Wallet />
                </PrivateRoute>
            } />
            <Route path="/wash-card" element={
                <PrivateRoute>
                    <WashCard />
                </PrivateRoute>
            } />
            <Route path="/new-wash-card" element={
                <PrivateRoute>
                    <NewWashCard />
                </PrivateRoute>
            } />
            <Route path="/prepaid_wash_service" element={
                <PrivateRoute>
                    <PrepaidWashPlans />
                </PrivateRoute>
            } />
            <Route path="/detailing_services" element={
                <PrivateRoute>
                    <DetailingServices />
                </PrivateRoute>
            } />
            <Route path="/order-history" element={
                <PrivateRoute>
                    <OrderHistory />
                </PrivateRoute>
            }/>
            <Route path="/my-account" element={
                <PrivateRoute>
                    <MyAccount />
                </PrivateRoute>
            }/>
            <Route path="/payment-options" element={
                <PrivateRoute>
                    <PaymentOptions />
                </PrivateRoute>
            }/>
            <Route path="/terms" element={
                <PrivateRoute>
                    <Policies id={0} />
                </PrivateRoute>
            }/>
            <Route path="/policies" element={
                <PrivateRoute>
                    <Policies id={1}/>
                </PrivateRoute>
            }/>
            <Route path="/terms-policies" element={
                <PrivateRoute>
                    <Policies id={0} id1={1}/>
                </PrivateRoute>
            }/>
            <Route path="/cancel-membership" element={
                <PrivateRoute>
                    <CancelMembership />
                </PrivateRoute>
            } />
            <Route path="/add-gift-card/:type/:gcGuid" element={
                <PrivateRoute>
                    <AddGiftCard />
                </PrivateRoute>
            } />
            <Route path="/add-gift-card" element={
                <PrivateRoute>
                    <AddGiftCard />
                </PrivateRoute>
            } />
            <Route path="/gift-card-details" element={
                <PrivateRoute>
                    <GiftCardDetails />
                </PrivateRoute>
            } />
            <Route path="/merge-gift-card" element={
                <PrivateRoute>
                    <MergeGiftCard />
                </PrivateRoute>
            } />
            <Route path="/order-summary" element={
                <PrivateRoute>
                    <OrderSummmary />
                </PrivateRoute>
            } />
            <Route path="/order-success" element={
                <PrivateRoute>
                    <OrderSuccess />
                </PrivateRoute>
            } />
            <Route path="/wallet-barcode" element={
                <PrivateRoute>
                    <WalletBarcode />
                </PrivateRoute>
            } />
            <Route path="/" element={<Login />} />
        </Routes>
    );
};

export default App;
