import React from 'react';
import {NavLink} from 'react-router-dom';

const Menu = (props) => {
    const customizedData = JSON.parse(localStorage.getItem('customizePage'));
    return (
        <div className="main_menu">
            <div className="container">
                <div className="row">
                    <div className="col">
                        <nav className="navbar navbar-expand-lg navbar-light p-0">
                            <div className="container-fluid">
                                <button className="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#mobileMenu" aria-controls="mobileMenu">
                                    <span className="navbar-toggler-icon"></span>
                                </button>
                                <div className="offcanvas offcanvas-start" id="mobileMenu" aria-labelledby="mobileMenuLabel">
                                    <div className="offcanvas-header">
                                        <button type="button" className="btn-close text-reset ms-auto" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                                    </div>
                                    <div className="offcanvas-body">
                                        <div className="navbar-collapse" id="navbarSupportedContent">
                                            <ul className="navbar-nav mx-auto" >
                                                {customizedData.portalNavMenuList.sort((a, b) => a.iOrderNumber > b.iOrderNumber ? 1 : -1).map((service, index) => {
                                                    if (service.type === 1 && service.isActive === true) {
                                                        if (service.isFeed == true) {
                                                            return (
                                                                <li className="nav-item" key={service.id}>
                                                                    <a className="nav-link font_color" href={service.targetUrl} target='#'><span className={`service.icon`}></span>{service.title}</a>
                                                                </li>
                                                            );
                                                        } else {
                                                            switch (service.actionName) {
                                                            case 'Home':
                                                                service.actionName = 'dashboard';
                                                                break;
                                                            case 'WashCard':
                                                                service.actionName = 'wash-card';
                                                                break;
                                                            case 'Wallet':
                                                                service.actionName = 'wallet';
                                                                break;
                                                            case 'GiftCard':
                                                                service.actionName = 'gift_card';
                                                                break;
                                                            case 'Menu':
                                                                service.actionName = 'manage_account';
                                                                break;
                                                            case '':
                                                                service.actionName = '#';
                                                                break;
                                                            }
                                                            return (
                                                                <li className="nav-item" key={service.id}>
                                                                    <NavLink className={({isActive}) => (isActive ? 'nav-link active' : 'nav-link')} aria-current="page" to={`/${service.actionName}`}><span className={`service.icon`}></span>{service.title}</NavLink>
                                                                </li>
                                                            );
                                                        }
                                                    }
                                                })}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Menu;
