import React from 'react';
import {Link} from 'react-router-dom';
import {useSelector} from 'react-redux';
import {useEffect} from 'react';
import {authVerification} from '../../helper/auth-verification';
import {isStringNullOrEmpty} from '../../utils/common-repo';

const Header = (props) => {
    let allowAnnomous = localStorage.getItem('allowAnnomous');
    if (allowAnnomous == 'true') {
        allowAnnomous = true;
    }
    const customerDetail = JSON.parse(localStorage.getItem('customerDetail'));
    const userInfo = JSON.parse(localStorage.getItem('userInfo'));
    let firstName = '';
    let lastName = '';
    if (!allowAnnomous) {
        if (customerDetail !== null) {
            firstName = customerDetail.customer.sFirstName;
            lastName = customerDetail.customer.sLastName;
        }
        if (isStringNullOrEmpty(firstName) && !isStringNullOrEmpty(userInfo.firstname)) {
            firstName = userInfo.firstname;
        }
        if (isStringNullOrEmpty(lastName) && isStringNullOrEmpty(userInfo.lastname)) {
            lastName = userInfo.lastName;
        }
    }
    const portalCustomizationsList = useSelector((state) => state.global.portalCustomizationsList);
    const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
    const phoneNumber = localStorage.getItem('phoneNumber');
    const customerCode = localStorage.getItem('customerCode');
    const customerUrl = localStorage.getItem('customerUrl');
    const logout = () => {
        localStorage.clear();
        localStorage.setItem('customerCode', customerCode);
        localStorage.setItem('customerUrl', customerUrl);
        window.location.href = `/${customerUrl}`;
    };
    useEffect(()=>{
        authVerification();
    }, []);

    return (
        <><header className="header">
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="header_logo">
                                <Link to={isLoggedIn ? '/' : '/buy'}>
                                    {portalCustomizationsList.length && portalCustomizationsList.map((value, key) => {
                                        if (value.sName === 'site_logo_url') {
                                            return <img src={value.sValue} className="img-fluid" alt="logo" key={key} />;
                                        }
                                    })}
                                </Link>
                            </div>
                            {(!allowAnnomous) ?
                                <div className="user_acc_head d-flex justify-content-end h-100">
                                    {Boolean(isLoggedIn) ? <div className="btn-group dropdown align-items-end align-self-center">
                                        <span className="user_name_i">{(firstName != '' && lastName !== '') ? (firstName).charAt(0) + (lastName).charAt(0) : ''}</span>
                                        <div className="uah_info">
                                            <div className="uah_name">{(firstName != '' && lastName !== '') ? firstName + ' ' + lastName : ''}</div>
                                            <small>{phoneNumber}</small>
                                        </div>
                                        <button className="ms-2" type="button" id="userHeaddrop" data-bs-toggle="dropdown" aria-expanded="false">
                                            <span className="icon arrow_down"></span>
                                        </button>
                                        <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="userHeaddrop">
                                            <li><Link className="dropdown-item" to="#" onClick={() => logout()}><span className="icon power_icon"></span>Logout</Link></li>
                                        </ul>
                                    </div> : ''}
                                </div> :
                                ''}

                        </div>
                    </div>
                </div>
            </div>
        </header>
        </>
    );
}; export default Header;

