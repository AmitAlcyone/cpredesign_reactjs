import React, {useState} from 'react';

const ServiceInfoCard = (props) => {
    const [selected, setSelected] = useState(false);
    const selectHandle = (e) => {
        e.preventDefault();
        setSelected(true);

        const cardInfo = {
            washName: service.cardInfo.WashName,
            washPrice: service.cardInfo.WashPrice,
        };
        service.onSelectCard(cardInfo);
        // localStorage.setItem( 'selectedcardInfo', JSON.stringify(cardInfo));
    };

    const demo = (data) => {
        if (data && data.length) {
            return (
                <ul className='wc_service_list scroller'>
                    {data.map((value, index) => {
                        return <li key={index}>{value.Service}</li>;
                    })}
                </ul>
            );
        }
    };

    return (
        <div className='col-md-4 col-sm-6 col-12'>
            <div className={selected ? 'wc_box wc_selected' : 'wc_box'}>
                <div className='wc_info'>
                    <h3>{service.cardInfo.WashName}</h3>
                    <span>{service.cardInfo.WashInfo}</span>
                    {demo(service.cardInfo.Services)}
                </div>
                <div className='wc_foot'>
                    <span className='wc_prices'>{service.cardInfo.WashPrice}</span>
                    <span onClick={selectHandle}>{selected ? 'Added!' : 'Buy Now!'}</span>
                </div>
            </div>
        </div>
    );
};

export default ServiceInfoCard;
