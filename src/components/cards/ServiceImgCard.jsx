import React, {useState} from 'react';

const ServiceImgCard = (props) => {
    const [selected, isSelected] = useState(false);

    const selectHandle = (e) => {
        e.preventDefault();
        isSelected(true);
    };

    return (
        <div className='col-md-4 col-sm-6 col-12' >
            <div className={selected ? 'wc_ox wc_selected' : 'wc_box'}>
                <div className='wc_info'>
                    <div className='wc_img'>
                        <img src={service.cardInfo.ImgURL} className='img-fluid' alt=''/>
                    </div>
                </div>
                <div className='wc_foot justify-content-center'>
                    <span onClick={selectHandle}>{selected ? 'Added!' : 'Buy Now!'}</span>
                </div>
            </div>
        </div>
    );
};

export default ServiceImgCard;
