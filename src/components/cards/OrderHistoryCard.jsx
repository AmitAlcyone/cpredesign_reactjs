import React from 'react';
import Serone from '../../assets/images/icons/wash.svg';

const OrderHistoryCard = (props) => {
    return (
        <div className="col-md-4 col-sm-6 col-12">
            <div className="order_box">
                <div className="od_head">
                    <b className="od_date">Nov. 21, 2021</b>
                    <span>Qty: <b>2</b></span>
                </div>
                <div className="od_item_info d-flex align-items-center">
                    <div className="ser_img"><img src={Serone} className="img-fluid" alt=""/></div>
                    <div className="ser_info">
                        <h3>Extreme Rain Check</h3>
                    </div>
                </div>
                <div className="od_trans_info">
                    <div>
                        <small>Total</small>
                        <span>$59.92</span>
                    </div>
                    <div>
                        <small>Order Number</small>
                        <span>1ywr6hvurw</span>
                    </div>
                </div>
                <div className="od_foot"><a href="#" className="main_btn">View Receipt</a></div>
            </div>
        </div>
    );
};

export default OrderHistoryCard;
