import React from 'react';
import {Link} from 'react-router-dom';

const InfoCard = (props) => {
    const planName = props.itemInfo.ServiceName;
    const word = 'Unlimited';
    return (
        <div className='col-md-4 col-sm-6 col-12'>
            <Link to={props.itemInfo.ServiceLink}>
                <div className={`${planName.includes(word) ? 'service_box recomm' : 'service_box'}`}>
                    <div className='ser_img'><img src={props.itemInfo.ServiceImage} className='img-fluid' alt='' /></div>
                    <div className='ser_info'>
                        <h3>{props.itemInfo.ServiceName}</h3>
                        <span>{props.itemInfo.Description}</span>
                    </div>
                </div>
            </Link>
        </div>
    );
};
export default InfoCard;
