import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import {getUnlimitedPlans, getCustomerProRateAmountMobile} from './../../services/unlimited-plan-service';
import {toast} from 'react-toastify';
import AlertModalModule from '../modals/AlertModal';
import {checkoutJson} from '../../services/auth-service';
import ServiceCardLoader from '../../utils/loaders/serviceCardLoader';

// import {getCustomerInfoByGuid} from '../../services/user-services';

const UnlimitedPlanListModule=(props)=>{
    toast.configure();
    const [modal, setModal] = useState(false);
    // const accessToken = useSelector((state) => state.auth.authInfo.accessToken);
    const currentCustomerULPlan = props.custData.vipAccountType;
    const userInfo = useSelector((state) => state.auth.userInfo);
    const locationId = useSelector((state) => state.auth.userInfo.preferedHomeLocation);
    if (locationId == null) {
        toast.error('Location id not found.');
    }
    const accessToken = userInfo.accessToken;
    const orderJson= useState(checkoutJson());

    const [unlimitedPlanData, setUnlimitedPlanData]= useState([]);
    const [isLoadUlPlan, setIsLoadUlPlan]= useState(false);

    const customerData= props.custData;
    // for selected unlimited plan
    // const [vipAccountTypeId, setVipAccountTypeId]= useState(0);
    const vipAccountTypeId= currentCustomerULPlan != null ? currentCustomerULPlan.lAccountType : 0;
    const IsMonthlyActive= props.custData.customerDetail.bMonthlyActive!=null && props.custData.customerDetail.bMonthlyActive;
    // if (props.custData.customerDetail.bMonthlyActive!=null && props.custData.customerDetail.bMonthlyActive) {
    //     setIsMonthlyActive(true);
    // }
    // if (currentCustomerULPlan != null) {
    //     setVipAccountTypeId(currentCustomerULPlan.lAccountType);
    // }
    const getUnlimitedPlanList = async () => {
        try {
            setIsLoadUlPlan(true);
            const response = await getUnlimitedPlans(accessToken, locationId);
            setIsLoadUlPlan(false);
            if (response.status === 200) {
                setUnlimitedPlanData(response.data);
            } else if (response.status === (401 | 404 | 470)) {
                toast.warning(`${response.data.title}.`, {autoClose: 3000});
            } else {
                toast.error('Some error occurred.', {autoClose: 3000});
            }
        } catch (ex) {
            toast.error('Some error occurred. When fetching unlimited plans.', {autoClose: 3000});
        }
    };
    useEffect(()=>{
        getUnlimitedPlanList();
    }, []);

    const [selectedULPlan, setSelectedULPlan]= useState('');
    const [alertModalData, setAlertModalData]= useState({});

    const showULPlanCheckoutView = async () => {
        orderJson[0].TopHeaderText = vipAccountTypeId > 0 ? 'Upgrade Unlimited plan' : 'Purchase Unlimited plan';
        orderJson[0].SubTotal= selectedULPlan.dblAmount;
        orderJson[0].ListItemText= 'New Selected Plan';
        orderJson[0].ListItemDescription= selectedULPlan.sDescription;
        orderJson[0].IsUWCRequest= true;
        if (IsMonthlyActive > 0) {
            orderJson[0].TaxAmount= 0;
            orderJson[0].IsUpgradeULPlan= true;
            orderJson[0].ULPlanData= {
                CurrentPlan: currentCustomerULPlan.sDescription,
                CurrentRecurringPayment: currentCustomerULPlan.dblAmount,
                NewSelectedPlan: selectedULPlan.sDescription,
                NewRecuringPayment: selectedULPlan.dblAmount
            };
        } else {
            orderJson[0].IsUpgradeULPlan= false;
            orderJson[0].TaxAmount= parseFloat((selectedULPlan.dblAmount * selectedULPlan.dblTaxRate)/100).toFixed(2);
        }
        orderJson[0].GrandTotal= parseFloat(parseFloat(orderJson[0].SubTotal) + parseFloat(orderJson[0].TaxAmount)).toFixed(2);
        orderJson[0].Email= '';
        orderJson[0].AdditionalPageData= {
            postJson: {
                CustomerIdentityId: userInfo.customerIdentityID,
                CustomerGuid: userInfo.customerGuid,
                ConfigurationUniqId: customerData.customer.configurationUniqId,
                MonthlyAccountTypeId: vipAccountTypeId,
                NewMonthlyAccountTypeId: selectedULPlan.lAccountType,
                Alias: '',
                ccType: '',
                Amount: selectedULPlan.dblAmount,
                CreditCardNumber: null,
                ExpiryDate: null,
                Price: selectedULPlan.dblAmount,
                Tax: selectedULPlan.dblTaxRate,
                // eslint-disable-next-line camelcase
                Discount_ID: 1,
                discountAmount: 5,
                VipAccountAmount: 50,
                VipAccountTax: 2,
                DiscountTax: 3,
                isDiscountApplied: true,
                // eslint-disable-next-line camelcase
                approval_code: ''
            },
            IsMonthlyActive: IsMonthlyActive,
            vipAccountTypeId: vipAccountTypeId,
            selectedULPlan: selectedULPlan,
            accessToken: accessToken
        };

        if (IsMonthlyActive) {
            // calculate pro rata amount
            const response = await getCustomerProRateAmountMobile(accessToken, {
                CustomerGuid: userInfo.customerGuid,
                ConfigurationUniqId: customerData.customer.configurationUniqId,
                MonthlyAccountTypeId: vipAccountTypeId,
                NewMonthlyAccountTypeId: selectedULPlan.lAccountType});

            if (response.status == 200) {
                orderJson[0].AdditionalPageData.postJson.Price= response.data;
                props.goToUWCCheckout(orderJson);
            } else if (response.status === (401 | 404 | 470)) {
                toast.warning(`${response.data.title}.`, {autoClose: 3000});
            } else {
                toast.error('Some error occurred.', {autoClose: 3000});
            }
        } else {
            props.goToUWCCheckout(orderJson);
        }
    };

    // const upgradeUnlimitedPlan = () =>{
    //     // const vipAccountTypeId= 0;
    //     // const IsMonthlyActive= false;
    //     // if (vipAccountTypeId) {
    //     //     IsMonthlyActive= true;
    //     // }
    //     // if (customerData.vipAccountType!= null) {
    //     //     vipAccountTypeId=customerData.vipAccountType.lAccountType;
    //     // }
    //     // for upgrade
    //     if (IsMonthlyActive) {

    //     } else {

    //     }
    //     setModal(false);
    // };
    const showUPConfimationPopup = (newSelectedPlan) => {
        orderJson[0].termsId= 5;
        if (IsMonthlyActive) {
            setAlertModalData({
                modalClass: 'warning_pop',
                title: 'Upgrade Unlimited plan',
                message: 'Do you want to upgrade current unlimited plan ' + currentCustomerULPlan.sDescription + ' to ' + newSelectedPlan.sDescription + '?',
                btnSuccessText: 'Yes',
                btnCancelText: 'No',
                btnPrimary: true
            });
            setModal(true);
        } else {
            debugger;
            setAlertModalData({
                modalClass: 'warning_pop',
                title: 'Purchase Unlimited plan',
                message: 'Do you want to purchase ' + newSelectedPlan.sDescription + ' plan? ',
                btnSuccessText: 'Yes',
                btnCancelText: 'No',
                btnPrimary: true
            });
            setModal(true);
        }
        setSelectedULPlan(newSelectedPlan);
    };
    return (
        <div>
            <div>
                {modal ? <AlertModalModule data={alertModalData} targetEvent={() => showULPlanCheckoutView()} closeModal={() => setModal(false)} /> : '' }
                <div className='more_plans'>
                    <ServiceCardLoader data={{height: 120, isLoad: isLoadUlPlan, wcBox: false, class: 'service_box', width: 250}}></ServiceCardLoader>
                    {unlimitedPlanData!= null && !isLoadUlPlan ? unlimitedPlanData.map((plan, index) => {
                        if (currentCustomerULPlan!= null && plan.lAccountType == currentCustomerULPlan.lAccountType && IsMonthlyActive) {
                            return (
                                // eslint-disable-next-line react/jsx-key
                                <div className='mp_info curnt_plan' style={{cursor: 'not-allowed'}} key={index}>
                                    <div className='mp_name'>{plan.sDescription} <span className='best_value'>Best Value</span> </div>
                                    <div className='mp_price'>${plan.dblAmount}</div>
                                </div>
                            );
                        } else {
                            return (
                                // eslint-disable-next-line react/jsx-key
                                <div className='mp_info' onClick={() => showUPConfimationPopup(plan)} key={index}>
                                    <div className='mp_name'>{plan.sDescription}</div>
                                    <div className='mp_price'>${plan.dblAmount}</div>
                                </div>
                            );
                        }
                    }) : ''}
                </div>
            </div>
        </div>
    );
};

export default UnlimitedPlanListModule;
