import React from 'react';
import {Link} from 'react-router-dom';
import Layout from '../../Layout';

const PlanPay = (props) => {
    return (
        <Layout>
            <div className='container'>
                <div className='row'>

                    <div className='col-12'>
                        <div className='d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap border-bottom'>
                            <div className='inner_title d-flex align-items-start'>
                                <div className='progress-circle over50 p100'>
                                    <span>2 of 2</span>
                                    <div className='left-half-clipper'>
                                        <div className='first50-bar'></div>
                                        <div className='value-bar'></div>
                                    </div>
                                </div>
                                <div className='main_title'>
                                    <h2>Update Your Unlimited Program</h2>
                                    <p>Please provide your credit card details</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className='row payment_method_wrap'>
                    <div className='col-lg-8'>
                        <div className='pay_method_list'>
                            <h5>Select a payment method</h5>
                            <div className='table-responsive'>
                                <table className='table ccards_table  table-borderless'>
                                    <thead>
                                        <tr>
                                            <th>Your credit card details</th>
                                            <th>Name on Card</th>
                                            <th>Expires on</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr className='rc_card'>
                                            <td>
                                                <div className='card_info'>
                                                    <div className='form-check'>
                                                        <input className='form-check-input' type='radio' name='flexRadioDefault' id='cardOne' selected />
                                                        <label className='form-check-label' htmlFor='cardOne'>
                                                            <b>Visa</b> <span>ending in 2451</span> <div className='card_type'><span className='card_icon visa_icon'></span></div>
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><div className='noc'>Glenn Martin</div></td>
                                            <td><div className='exp_date'>06/2023</div></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className='col-lg-4'>
                        <div className='order_summ_wrap'>
                            <h5>Order Summary</h5>
                            <div className='summary_card'>
                                <ul className='list_items os_user_plan'>
                                    <li>
                                        <span>Customer</span> <b>Glenn</b>
                                    </li>
                                    <li>
                                        <span>Wash:</span> <b>Extreme 90 Days Unlimited</b>
                                    </li>
                                </ul>
                                <ul className='list_items order_details dash_bdr_top mt-4 pt-3'>
                                    <li>
                                        <span>Subtotal:</span> <span>0.00</span>
                                    </li>
                                    <li>
                                        <span>Discount:</span> <span>0.00</span>
                                    </li>
                                    <li>
                                        <span>Tax:</span> <span>0.00</span>
                                    </li>
                                    <li>
                                        <b>Total:</b> <b>0.00</b>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-12'>

                        <div className='d-flex justify-content-end mt-3 pt-3 border-top'>
                            <div className='btn_grps d-flex flex-wrap'>
                                <Link className='back_btn icon_btn me-2' to='/add_new_ultd_plan'><span className='icon arrow_left'></span> Back</Link>
                                <Link className='main_btn icon_btn' to='/success'>Checkout <span className='icon arrow_right_white'></span></Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};
export default PlanPay;
