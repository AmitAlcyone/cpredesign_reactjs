import React from 'react';
import {Link} from 'react-router-dom';
import Layout from '../../Layout';
import addNewdPlan from '../../assets/JSON/NewUltdPlan.json';

const AddNewPlan = (props) => {
    return (
        <Layout>
            <div className='container'>
                <div className='row'>
                    <div className='col-12'>
                        <div className='d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap border-bottom'>
                            <div className='inner_title d-flex align-items-start'>
                                <div className='progress-circle over50 p50'>
                                    <span>1 of 2</span>
                                    <div className='left-half-clipper'>
                                        <div className='first50-bar'></div>
                                        <div className='value-bar'></div>
                                    </div>
                                </div>
                                <div className='main_title'>
                                    <h2>Change Unlimited Program</h2>
                                    <p>Select a new plan to update your membership.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row mb-5'>
                    <div className='col-lg-8'>
                        <div className='row wash_list'>
                            {
                                addNewdPlan.UnlimitedPlan.map((service, index) => {
                                    return (
                                        <div className='col-md-6 col-sm-6 col-12' key={index}>
                                            <div className='wc_box'>
                                                <div className='wc_info'>
                                                    <h3>{service.Description}</h3>
                                                    <span>{service.Amount}</span>
                                                    <ul className='wc_service_list scroller'>
                                                        {
                                                            service.Services.map((name) => (
                                                                // eslint-disable-next-line react/jsx-key
                                                                <li>
                                                                    {name}
                                                                </li>
                                                            ))
                                                        }

                                                    </ul>
                                                </div>
                                                <div className='wc_foot'>
                                                    <span className='wc_prices'>{service.Price}</span>
                                                    <Link to=''>{service.Buyadd}</Link>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })
                            }
                        </div>
                    </div>
                    <div className='col-lg-4'>
                        <div className='order_summ_wrap'>
                            <div className='summary_card'>
                                <ul className='list_items os_user_plan'>
                                    <li>
                                        <div className='current_plan'>
                                            <small>Current Plan</small>
                                            <div className='cp_name'>No Unlimited Plan</div>
                                        </div>
                                    </li>
                                    <li>
                                        <span>Current Recurring Payment:</span> <b>$19.99</b>
                                    </li>
                                </ul>
                                <ul className='list_items order_details dash_bdr_top mt-4 pt-3'>
                                    <li>
                                        <div className='current_plan'>
                                            <small>New Selected Plan</small>
                                            <div className='cp_name'>Ultimate Unlimited</div>
                                        </div>
                                    </li>
                                    <li>
                                        <span>Old Recurring Payment:</span> <span>$39.99</span>
                                    </li>
                                    <li>
                                        <span>New Recurring Payment:</span> <span>$00.00</span>
                                    </li>
                                    <li>
                                        <b>Amount to be Paid:</b> <b>$39.99</b>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12'>
                        <div className='d-flex justify-content-end mt-3 pt-3 border-top'>
                            <div className='btn_grps d-flex flex-wrap'>
                                <Link className='back_btn icon_btn me-2' to='/unlimited_plans'><span className='icon arrow_left'></span> Back</Link>
                                <Link className='main_btn icon_btn' to='/unlimited_plans_payment'>Continue <span className='icon arrow_right_white'></span></Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};
export default AddNewPlan;
