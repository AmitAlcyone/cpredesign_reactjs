import React, {useState, useEffect} from 'react';
import Layout from './../Layout';
import QRCode from 'react-qr-code';
import Barcode from 'react-barcode';
import {useSelector} from 'react-redux';
import {sendToDevic} from './../../services/wallet-service';
import calendarImg from './../../assets/images/calendar.png';
import {toast} from 'react-toastify';
import {Link} from 'react-router-dom';
import dateFormatter from '../../utils/dateFormatter';


const WalletBarcode = (props) => {
    toast.configure();
    const userInfo= useSelector((state) => state.auth.userInfo);
    const selectedWallet = useSelector((state) => state.wallet.selectedWallet);
    const [loading, setLoading] = useState(false);

    const handleSendToDevice = async () => {
        setLoading(true);
        const reqBody = {
            type: selectedWallet.type,
            referencedGUID: selectedWallet.referencedGUID,
            customerIdentityId: userInfo.customerIdentityID
        };
        const response = await sendToDevic(userInfo.accessToken, reqBody);
        if (response.status === 200) {
            setLoading(false);
            setTimeout(() => {
                toast.success(response.data, {autoClose: 3000});
            }, 1000);
        } else {
            setLoading(false);
        }
    };
    useEffect(() => {
        // console.log(selectedWallet);
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }, []);

    const barcodeString = selectedWallet.barcode;
    const barcodeStr = barcodeString !== '' ? barcodeString : 'DemoBarcode';
    // const [date, time] = (selectedWallet.walletDate !== null ? selectedWallet.walletDate.split('T') : '');

    return (
        <Layout>
            <div className='container'>
                <div className='barcode_header mb-4 mt-5'>
                    <Link to={selectedWallet.pageBackUrl ? selectedWallet.pageBackUrl : '/wallet'} className='back_link icon_btn'><span className='icon arrow_left'></span> Back</Link>
                    <div className='text-center'>
                        <p>{selectedWallet.description ?? ''}</p>
                        {/* <p>{date}&nbsp;&nbsp;{time != null ? time.slice(0, 5) : ''}</p> */}
                        <p>{dateFormatter(selectedWallet.walletDate)}</p>
                    </div>
                    <button className={selectedWallet.active ? 'main_btn' : 'btn btn-secondary'} disabled={selectedWallet.active ? loading : true} onClick={() => handleSendToDevice()}>{loading && <span className="spinner-border text-light btnLoader"></span>}{selectedWallet.active ? 'Send To Device' : 'Redeemed'}</button>
                </div>
                <div className='row payment_method_wrap'>
                    <div className='col-lg-7 mx-auto text-center'>
                        <div className='barcode_img mx-auto text-center'>
                            <div className={selectedWallet.active ? '' : 'opacity-25'}>
                                <Barcode value={barcodeStr} width={2} height={60} />
                            </div>
                        </div>
                        <div className='barcode_msg pt-4 pb-3 mt-3 mb-3 border-top'>
                            <p>Make sure to show this code to the attendant or, scan the bar code in the machine</p>
                            <div className='bcm_date'>
                                <img src={calendarImg} alt='calendar' className='calendar' />&nbsp;
                                {/* {date}&nbsp;&nbsp;{time != null ? time.slice(0, 5) : ''} */}
                                {dateFormatter(selectedWallet.walletDate)}
                            </div>
                        </div>
                        <div className='barcode_img mx-auto text-center mt-4'>
                            <div className={selectedWallet.active ? '' : 'opacity-25'}>
                                <QRCode value={barcodeStr} size={200} />
                            </div>
                        </div>
                        <div className='barcode_number text-center p-4'>
                            <small>Your Redeem Code:</small>
                            <h3 className='mt-2'>{barcodeStr}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default WalletBarcode;
