import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import {Link, useNavigate} from 'react-router-dom';
import Layout from '../Layout';
import {creditCardsList} from './../../services/manage-account-service';
import {buyPrepaidBook} from './../../services/wash-service';
import {useDispatch} from 'react-redux';
import {setCreditCards} from '../../store/reducers/manage-account-reducer';
import TermsModal from '../modals/TermsModal';
import {getPromotionAvailable} from './../../services/order-service';
import {toast} from 'react-toastify';
import {setIsLoading} from './../../store/reducers/global-reducer';

const OrderSummary = (props) => {
    const [modal, setModal] = useState(false);
    toast.configure();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const userInfo = useSelector((state) => state.auth.userInfo);
    const washInfo = useSelector((state) => state.prepaidWash.selectedPrepaidCard);
    const creditCards = useSelector((state) => state.manageAccount.creditCards);
    const customerData = useSelector((state) => state.customer.customersInfoByGuid);
    const locationID = useSelector((state) => state.customer.customersInfoByGuid.location);
    const custPhone = useSelector((state) => state.customer.customersInfoByGuid.customer);
    const custEmail = useSelector((state) => state.customer.customersInfoByGuid.customer);
    const previous = useSelector((state) => state.auth.previousPage);
    const [promotioncode, setpromotioncode] = useState('');
    const [apply, setapply] = useState(false);
    const [subtotal] = useState(washInfo.dblBookPrice);
    const [discount, setdiscount] = useState(0);
    const [tax] = useState(0);
    const [total, settotal] = useState((subtotal + discount + tax).toFixed(2));
    const [selectCard, setselectCard] = useState(false);
    const [termAndCondtion, setTermAndCondition] = useState(false);
    const [aliass, setAlias]= useState('');

    useEffect(() => {
        const getCreditCardsList = async () => {
            if (creditCards.length === 0) {
                const response = await creditCardsList(userInfo.accessToken, userInfo.customerIdentityID);
                setAlias(response.data[0].sPayerIdentifier);
                if (response.status === 200) {
                    setTimeout(() => {
                        dispatch(setCreditCards({creditCards: response.data}));
                    }, 1000);
                } else {
                }
            }
        };
        getCreditCardsList();
    }, []);
    const checkout = async () => {
        dispatch(setIsLoading({isLoading: true}));
        if (selectCard !== false) {
            if (termAndCondtion !== false) {
                const reqBody = {
                    bookTypeId: washInfo.lBookTypeId,
                    customerIdentityId: userInfo.customerIdentityID,
                    Alias: aliass,
                    sPhone: custPhone.sPhone,
                    sEmail: custEmail.sEmail,
                    Amount: washInfo.dblBookPrice,
                    LocationId: locationID.locationId,
                    CustomerGuid: userInfo.customerGuid,
                    isDiscountApplied: false
                };
                const response = await buyPrepaidBook(userInfo.accessToken, 'F37130F2-EEDF-4739-852C-007A3B396ADD', reqBody);
                if (response.status === 200) {
                    dispatch(setIsLoading(false));
                    toast.success('CheckOut successfully.', {autoClose: 3000});
                    return navigate('/order-success');
                } else {
                    dispatch(setIsLoading(false));
                    toast.error('Something went wrong.', {autoClose: 3000});
                }
            } else {
                dispatch(setIsLoading(false));
                toast.error('Please Terms and Conditions.');
            }
        } else {
            dispatch(setIsLoading(false));
            toast.error('Please Select Card.');
        }
    };

    const applyPromotionCode = async () => {
        setapply(true);
        const response = await getPromotionAvailable(userInfo.accessToken);
        if (response.status === 200) {
            if (response.data[0].discountAmount > 0) {
                setdiscount(response.data[0].discountAmount);
                settotal(subtotal - discount);
            } else {
                toast.warning('Discount not available on this code.', {autoClose: 3000});
            }
        } else {
            dispatch(setIsLoading(false));
            toast.error('Something went wrong.', {autoClose: 3000});
        }
    };
    const handleChangeCards = (e) => {
        setselectCard(e.target.checked);
    };
    const handleChangeTerm = (e) => {
        setTermAndCondition(e.target.checked);
    };

    return (
        <Layout>
            {modal ? <TermsModal closeModal={() => setModal(false)} /> : ''}
            <div className='container'>
                <div className='row'>
                    <div className='col-12'>
                        <div className='d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap border-bottom'>
                            <div className='inner_title d-flex align-items-start'>
                                <div className='progress-circle over50 p100'>
                                    <span>2 of 2</span>
                                    <div className='left-half-clipper'>
                                        <div className='first50-bar'></div>
                                        <div className='value-bar'></div>
                                    </div>
                                </div>
                                <div className='main_title'>
                                    <h2>Buy a Wash Service</h2>
                                    <p>Order Summary</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form>
                    <div className='row payment_method_wrap'>
                        <div className='col-lg-8'>
                            <div className='pay_method_list'>
                                <h5>Select a payment method</h5>
                                <div className='table-responsive'>
                                    <table className='table ccards_table  table-borderless'>
                                        <thead>
                                            <tr>
                                                <th>Your credit card details</th>
                                                <th>Name on Card</th>
                                                <th>Expires on</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {/* {isLoading && contentLoader()} */}
                                            {creditCards.length > 0 && creditCards.map((service, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td>
                                                            <div className='card_info'>
                                                                <div className='form-check'>
                                                                    <input className='form-check-input' type='radio' name='flexRadioDefault' id='cardOne' {...(selectCard ? 'checked' : '')} onChange={() => handleChangeCards(event)}/>
                                                                    <label className='form-check-label' htmlFor='cardOne'>
                                                                        <b>Visa</b> <span>ending in {service.sSpan}</span>
                                                                        <div className='card_type'>
                                                                            <span className='card_icon visa_icon'></span>
                                                                        </div>
                                                                        <div className='rc_card_badge ms-2'>Recurring Card</div>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><div className='noc'>Glenn Martin</div></td>
                                                        <td><div className='exp_date'>{service.sExpiredMonth}/{service.sExpireYear}</div></td>
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    </table>
                                </div>

                                <div className='new_card_add'>
                                    <button className='btn btn-outline-secondary w-100' type='button' data-bs-toggle='collapse' data-bs-target='#add_new_card_from' aria-expanded='false' aria-controls='add_new_card_from'>+ add a new card</button>
                                    <div className='collapse add_cc_form' id='add_new_card_from'>
                                        <div className='row'>
                                            <div className='col-12 col-sm-6 col-md-4'>
                                                <div className='input_wrap'>
                                                    <label htmlFor='fName' className='form-label'>Card Holder's Name <sup className='text-danger'>*</sup></label>
                                                    <input type='text' id='fName' className='form-control form-control-lg' placeholder='cardholder name' />
                                                </div>
                                            </div>
                                            <div className='col-12 col-sm-6 col-md-4'>
                                                <div className='input_wrap'>
                                                    <label htmlFor='lName' className='form-label'>Credit Card Number <sup className='text-danger'>*</sup></label>
                                                    <input type='text' id='lName' className='form-control form-control-lg' placeholder='' />
                                                </div>
                                            </div>
                                            <div className='col-7 col-sm-6 col-md-2'>
                                                <div className='input_wrap'>
                                                    <label htmlFor='repEmail' className='form-label'>Expiry <sup className='text-danger'>*</sup></label>
                                                    <input type='text' id='repEmail' className='form-control form-control-lg' placeholder='MM/YYYY' />
                                                </div>
                                            </div>
                                            <div className='col-5 col-sm-6 col-md-2'>
                                                <div className='input_wrap'>
                                                    <label htmlFor='message' className='form-label'>CVV <sup className='text-danger'>*</sup></label>
                                                    <input type='text' id='message' className='form-control form-control-lg' placeholder='' />
                                                </div>
                                            </div>
                                            <div className='col-12 mt-3'>
                                                <div className='form-check'>
                                                    <input className='form-check-input' type='checkbox' id='saveCardCheck' />
                                                    <label className='form-check-label' htmlFor='saveCardCheck'>
                                                        Save Payment Info for Future Purchases
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-lg-4'>
                            <div className='order_summ_wrap'>
                                <h5>Order Summary</h5>
                                <div className='summary_card'>
                                    <ul className='list_items os_user_plan'>
                                        <li>
                                            <span>Customer</span> <b>{`${customerData.customer.sFirstName} ${customerData.customer.sLastName}`}</b>
                                        </li>
                                        <li>
                                            <span>Wash:</span> <b>{washInfo.sBookDescription}</b>
                                        </li>
                                    </ul>
                                    <div className='promo_form'>
                                        <div className='form-group d-flex mt-3 mb-3'>
                                            <input type='text' className='form-control promo_code me-2 form-control-lg  id="myInput' placeholder='Enter Promotion Code' value={promotioncode} onChange={(event) => setpromotioncode(event.target.value)} />
                                            <button type='button' className='main_btn' onClick={() => applyPromotionCode()}>Apply</button>
                                        </div>
                                        {promotioncode === '' && apply ? <p className='text-danger'>Please Enter Promotion Code</p>:''}
                                    </div>

                                    <ul className='list_items order_details'>
                                        <li>
                                            <span>Subtotal:</span> <span>${subtotal.toFixed(2)}</span>
                                        </li>
                                        <li>
                                            <span>Discount:</span> <span>${discount.toFixed(2)}</span>
                                        </li>
                                        <li>
                                            <span>Tax:</span> <span>${tax.toFixed(2)}</span>
                                        </li>
                                        <li>
                                            <b>Total:</b> <b>${total}</b>
                                        </li>
                                    </ul>
                                </div>
                                <div className='condi_checks pt-4'>
                                    <div className='form-check'>
                                        <input className='form-check-input' type='checkbox' id='rEmail' />
                                        <label className='form-check-label' htmlFor='rEmail'>
                                            Receive email receipt
                                        </label>
                                    </div>
                                    <div className='form-check'>
                                        <input className='form-check-input' type='checkbox' id='rSMS' />
                                        <label className='form-check-label' htmlFor='rSMS'>
                                            Receive SMS receipt
                                        </label>
                                    </div>
                                    <div className='form-check'>
                                        <input className='form-check-input' type='checkbox' id='termcondi' {...(termAndCondtion ? 'checked' : '')} onChange={() => handleChangeTerm(event)}/>
                                        <label className='form-check-label' htmlFor='termcondi'>
                                            I Agree to the <Link to='#' className='termandcondition_link' onClick={() => setModal(true)}> Terms and Conditions* </Link>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-12'>
                            <div className='d-flex justify-content-end mt-3 pt-3 border-top'>
                                <div className='btn_grps d-flex flex-wrap'>
                                    <Link className='back_btn icon_btn me-2' to={previous}><span className='icon arrow_left'></span> Back</Link>
                                    {/* <Link className='main_btn icon_btn' to="#" onClick={() => checkout(value)}>Checkout <span className='icon arrow_right_white'></span></Link>*/}
                                    <span className='main_btn icon_btn' to='#' onClick={() => checkout()} >Checkout<span className='icon arrow_right_white'></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </Layout>
    );
};
export default OrderSummary;

