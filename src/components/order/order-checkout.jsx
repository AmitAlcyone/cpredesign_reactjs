import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import {Link} from 'react-router-dom';
import {creditCardsList} from '../../services/manage-account-service';
import {useDispatch} from 'react-redux';
import {setCreditCards} from '../../store/reducers/manage-account-reducer';
import TermsModal from '../modals/TermsModal';
import {getPromotionAvailable} from '../../services/order-service';
import {toast} from 'react-toastify';
// import {saveCreditCard} from './../../services/manage-account-service';
import {setIsLoading} from '../../store/reducers/global-reducer';
import {addNewUWCPlan, upgradeUWCPlan} from '../../services/unlimited-plan-service';
import SkeletonCClistOnCheckout from '../../utils/skeletons/SkeletonCClistOnCheckout';
import {setwalletServices} from '../../store/reducers/wallet-reducer';
import {setFormOtherData} from '../../store/reducers/auth-reducer';
import {setGiftCards, setformOtherData} from '../../store/reducers/gift-card-reducer';
import PaymentCard from './PCI/payment-card';
import {GetCreditCardIcon as getCCIcon, getPCIDetailsLocal} from '../../utils/common-repo';
// import {InitializePCI as intializePCIGatway} from '../../utils/PCI/global-payment';
import FullScreenLoader from '../../utils/loaders/FullScreenLoader';
import {putCustomerInfoByGuid} from '../../services/user-services';

const OrderSummary = (props) => {
    const [modal, setModal] = useState(false);
    toast.configure();
    const toastId = React.useRef(null);

    const dispatch = useDispatch();

    const [loading, setLoading] = useState(false);
    const [viewCardForm, setViewCardForm] = useState(false);
    const [showEmailPopup, setShowEmailPopup] = useState(false);
    const [userEmail, setUserEmail] = useState('');

    const [customerInfo, setCustomerInfo] = useState(JSON.parse(localStorage.getItem('customerDetail')));
    const newCustomerInfo = JSON.parse(localStorage.getItem('customerDetail'));
    // const locationID = useSelector((state) => state.customer.customersInfoByGuid.location);
    //     ----- Order Property Names -----
    // TopHeaderText string
    // IsCardSelected bool
    // IsCheckedTermAndCondition bool
    // IsShowPromcode bool
    // Promocode string
    // IsPromocodeApplied bool
    // TaxAmount decimal
    // DiscountAmount decimal
    // SubTotal decimal
    // GrandTotal decimal
    const [order, setOrder]=useState(props.order[0]);
    const creditCards = useSelector((state) => state.manageAccount.creditCards);
    if (customerInfo !=null && customerInfo.customerDetail!=null && customerInfo.customerDetail.sPayerIdentifier !=null && creditCards.length > 0) {
        const card= creditCards.find((d) => d.sPayerIdentifier === customerInfo.customerDetail.sPayerIdentifier && !order.IsCardSelected);
        if (card!= null) {
            order.IsCardSelected= true;
            order.SelectedCard= card;
            setOrder(order);
        }
    }
    if ((order.Email== '' || order.Email== undefined || order.Email== null) && newCustomerInfo.customer.sEmail!=null && newCustomerInfo.customer.sEmail!= '') {
        order.Email= newCustomerInfo.customer.sEmail;
        setOrder(order);
    }
    // const customerData = useSelector((state) => state.customer.customersInfoByGuid);
    const userInfo = useSelector((state) => state.auth.userInfo);
    const [Loader, setLoader] = useState(false);
    const [selectedCardId, setSelectedCardId]= useState(order.IsCardSelected ? order.SelectedCard.id : -1);
    // eslint-disable-next-line no-unused-vars
    const skeletonLoader = () => {
        const content = [];
        for (let index = 0; index < 3; index++) {
            content.push(<SkeletonCClistOnCheckout key={index} />);
        }
        return content;
    };

    const applyPromotionCode = async () => {
        // setapply(true);
        order.IsPromocodeApplied = true;
        setOrder(order);
        const response = await getPromotionAvailable(userInfo.accessToken);
        if (response.status === 200) {
            if (response.data[0].discountAmount > 0) {
                order.DiscountAmount= response.data[0].discountAmount;
                order.GrandTotal= order.SubTotal - order.DiscountAmount;
                setOrder(order);
            } else {
                toast.warning('Discount not available on this code.', {autoClose: 3000});
            }
        } else {
            dispatch(setIsLoading(false));
            toast.error('Something went wrong.', {autoClose: 3000});
        }
    };

    const handleChangeCards = (e, card) => {
        order.IsCardSelected= e.target.checked;
        order.SelectedCard= card;
        // setselectCard(e.target.checked);
        // setOrder(order);
        // setOrder(order);
        setSelectedCardId(card.id);
    };

    const setNewAddedCard = (card) => {
        order.IsCardSelected= true;
        order.SelectedCard= card;
        // setOrder(order);
        setSelectedCardId(card.id);
    };

    const handleChangeTerm = (e) => {
        // setTermAndCondition(e.target.checked);
        order.IsCheckedTermAndCondition= e.target.checked;
        setOrder(order);
    };
    const setPromocode = (code) => {
        order.Promocode= code;
        setOrder(order);
    };

    const processCheckout = async () => {
        setLoading(false);
        dispatch(setwalletServices({services: []}));
        dispatch(setGiftCards({giftCards: []}));
        setLoader(true);
        if (order.IsUWCRequest) {
            // if(AdditionalPageData)
            const isLoading= await processUWCCheckout();
            setLoading(isLoading);
            setLoader(false);
        } else {
            const isLoading= await props.processCheckout(order);
            setLoading(isLoading);
            setLoader(false);
        }
    };

    const callbackOnPreviousPage = () => {
        dispatch(setFormOtherData({forOther: ''}));
        dispatch(setformOtherData({forOther: ''}));
        if (order.IsUWCRequest) {
            props.hideCheckoutPage(false);
        } else {
            props.hideCheckoutPage();
        }
    };

    const processUWCCheckout = async () => {
        if (order.IsCheckedTermAndCondition && order.IsCardSelected) {
            setLoading(true);
            const postJson= order.AdditionalPageData.postJson;
            postJson.Alias = order.SelectedCard.sPayerIdentifier;
            postJson.CreditCardNumber= order.SelectedCard.id;
            postJson.ExpiryDate= String(order.SelectedCard.sExpiredMonth) +'/'+ String(order.SelectedCard.sExpireYear);

            postJson.isSendMail= order.IsRecieveEmailReciept;
            postJson.isSendSMS= order.IsRecieveSMSReciept;
            postJson.sEmail = order.Email !== undefined ? order.Email : newCustomerInfo.customer.sEmail;

            let response;
            if (order.AdditionalPageData.IsMonthlyActive && order.AdditionalPageData.vipAccountTypeId > 0) {
                response = await upgradeUWCPlan(order.AdditionalPageData.accessToken, postJson);
            } else {
                response = await addNewUWCPlan(order.AdditionalPageData.accessToken, postJson);
            }
            setLoading(false);
            // const response = await addNewUWCPlan(order.AdditionalPageData.accessToken, postJson);
            if (response.status == 200) {
                setLoader(false);
                if (order.AdditionalPageData.IsMonthlyActive && order.AdditionalPageData.vipAccountTypeId > 0) {
                    toast.success(response.data);
                } else {
                    toast.success('Unlimited plan added sucessfully.');
                }
                props.hideCheckoutPage(true);
                // return navigate('/order-success');
            } else if (response.status === (401 | 404 | 470)) {
                toast.warning(`${response.data.title}.`, {autoClose: 3000});
                setLoader(false);
            } else {
                toast.error('Some error occurred.', {autoClose: 3000});
                setLoader(false);
            }
        } else if (!order.IsCheckedTermAndCondition) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please check term and condition.');
            }
            return false;
        } else if (!order.IsCardSelected) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please select credit card before checkout.');
            }
            return false;
        }
    };

    const expandPCIForm = (e) => {
        e.preventDefault();
        e.stopPropagation();
        setViewCardForm(true);
    };

    const PCIScript= getPCIDetailsLocal().PCIScript;
    // const cardUrl= 'https://js.globalpay.com/v1/globalpayments.js';

    useEffect(() => {
        const script = document.createElement('script');
        script.src = PCIScript;
        script.async = false;
        document.body.appendChild(script);

        const getCreditCardsList = async () => {
            if (creditCards.length === 0) {
                setLoading(true);
                const response = await creditCardsList(userInfo.accessToken, userInfo.customerIdentityID);
                if (response.status === 200) {
                    dispatch(setCreditCards({creditCards: response.data}));
                } else {
                    toast.error('Some error occurred when fetching card details.', {autoClose: 3000});
                    dispatch(setCreditCards({creditCards: []}));
                }
                setLoading(false);
            }
        };
        getCreditCardsList();

        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        return () => {
            document.body.removeChild(script);
        };
    }, []);

    const handleEmail = async () => {
        const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (userEmail.match(validRegex)) {
            const customerData = customerInfo;
            customerData.customer.sEmail = userEmail;
            localStorage.setItem('customerDetail', JSON.stringify(customerData));
            setCustomerInfo(customerData);
            const values = {
                firstName: newCustomerInfo.customer.sFirstName == null ? '' : newCustomerInfo.customer.sFirstName,
                lastName: newCustomerInfo.customer.sLastName == null ? '' : newCustomerInfo.customer.sLastName,
                phoneNumber: localStorage.getItem('phoneNumber'),
                email: userEmail
            };
            const button = document.getElementById('btnAlertYes');
            const btnSpan = document.getElementById('spanLoader');
            button.disabled = true;
            btnSpan.style.display = 'block';
            const response = await putCustomerInfoByGuid(userInfo.accessToken, values);
            if (response.status === 200) {
                console.log(response);
            }
            button.disabled = false;
            btnSpan.style.display = 'block';
            setShowEmailPopup(false);
            order.Email = userEmail;
            setOrder(order);
        } else {
            toast.error('Invalid email address.', {autoClose: 3000});
        }
    };

    return (
        <div>
            {modal ? <TermsModal id={order.termsId} closeModal={() => setModal(false)} /> : ''}
            {Loader === true ? <FullScreenLoader /> : ''}
            <div className='container'>
                <div className='row'>
                    <div className='col-12'>
                        <div className='d-flex mb-4 mt-3 justify-content-between align-items-center flex-wrap border-bottom'>
                            <div className='inner_title d-flex align-items-start'>
                                <div className='progress-circle over50 p100'>
                                    <span>2 of 2</span>
                                    <div className='left-half-clipper'>
                                        <div className='first50-bar'></div>
                                        <div className='value-bar'></div>
                                    </div>
                                </div>
                                <div className='main_title'>
                                    <h2>{order.TopHeaderText}</h2>
                                    <p>Order Summary</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <form> */}
                <div className='row payment_method_wrap'>
                    <div className='col-lg-8'>
                        <div className='pay_method_list'>
                            <h5>Select a payment method</h5>
                            <div className='table-responsive'>
                                <table className='table ccards_table table-borderless'>
                                    <thead>
                                        <tr>
                                            <th>Your credit card details</th>
                                            {/* <th>Name on Card</th> */}
                                            <th>Expires on</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {loading && skeletonLoader()}
                                        {creditCards.length > 0 && !loading ? creditCards.map((service, index) => {
                                            const ccDetail= getCCIcon(service);
                                            const isChecked= (order.IsCardSelected && (selectedCardId == service.id || order.SelectedCard.id== service.id));
                                            return (
                                                <tr key={index}>
                                                    <td>
                                                        <div className='card_info'>
                                                            <div className='form-check'>
                                                                <input
                                                                    className='form-check-input'
                                                                    type='radio'
                                                                    name='flexRadioDefault'
                                                                    key={index}
                                                                    // checked={isChecked}
                                                                    {...(isChecked ? {checked: 'checked'} : {})}
                                                                    onChange={(event) => handleChangeCards(event, service)}
                                                                    id={`card-${service.id}`}
                                                                />
                                                                <label className='form-check-label' htmlFor={`card-${service.id}`}>
                                                                    <b>{ccDetail.name}</b> <span>ending in {service.sSpan}</span>
                                                                    <div className='card_type'>
                                                                        <span className={`card_icon ${ccDetail.icon}`}></span>
                                                                    </div>
                                                                    {service.bIsRecurring!=null && service.bIsRecurring ?
                                                                        <div className='rc_card_badge ms-2'>Recurring Card</div> : '' }
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    {/* <td><div className='noc'>Glenn Martin</div></td> */}
                                                    <td><div className='exp_date'>{service.sExpiredMonth}/{service.sExpireYear}</div></td>
                                                </tr>
                                            );
                                        }) : ''}
                                    </tbody>
                                </table>
                                {creditCards.length === 0 ? <p className='notFound'>Credit Card not available.</p> : ''}
                            </div>

                            <div className='new_card_add'>
                                <button className='btn btn-outline-secondary w-100' type='button' data-bs-toggle='modal'
                                    data-bs-target='#add_new_card_pop' aria-expanded='false' aria-controls='add_new_card_from' onClick={(event)=> expandPCIForm(event)}>+ add new card</button>
                            </div>
                        </div>
                    </div>
                    <div className='modal fade cw_modal add_card_pop' id='add_new_card_pop' data-bs-backdrop='static' data-bs-keyboard='false' tabIndex='-1' aria-labelledby='add_new_card_popLabel' aria-hidden='true'>
                        <div className='modal-dialog modal-dialog-centered'>
                            <div className='modal-content'>
                                {/* <div className='modal-header'>
                                    <h5 className='modal-title' id='add_new_card_popLabel'>Add Credit Card</h5>
                                </div> */}
                                <button type="button" className="btn-close" id="btnCCPopCancel" data-bs-dismiss="modal" onClick={() => {
                                    setViewCardForm(false);
                                }}></button>
                                <div className='modal-body'>
                                    {(viewCardForm) ? <PaymentCard loader={(val) => setLoader(val)} setCard={(card) => setNewAddedCard(card)} hidePop={() => setViewCardForm(false)} /> : ''}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-lg-4'>
                        <div className='order_summ_wrap'>
                            <h5>Order Summary</h5>
                            { (order.IsUWCRequest && order.AdditionalPageData.IsMonthlyActive && order.AdditionalPageData.vipAccountTypeId > 0 ) ?
                                <div className="summary_card">
                                    <ul className="list_items os_user_plan">
                                        <li>
                                            <div className="current_plan">
                                                <small>Current Plan</small>
                                                <div className="cp_name">{order.ULPlanData.CurrentPlan}</div>
                                            </div>
                                        </li>
                                        <li>
                                            <span>Current Recurring Payment:</span> <b>${parseFloat(order.ULPlanData.CurrentRecurringPayment).toFixed(2)}</b>
                                        </li>
                                    </ul>
                                    <ul className="list_items order_details dash_bdr_top mt-4 pt-3">
                                        <li>
                                            <div className="current_plan">
                                                <small>New Selected Plan</small>
                                                <div className="cp_name">{order.ULPlanData.NewSelectedPlan}</div>
                                            </div>
                                        </li>
                                        <li>
                                            <span>Old Recurring Payment:</span> <span>${parseFloat(order.ULPlanData.CurrentRecurringPayment).toFixed(2)}</span>
                                        </li>
                                        <li>
                                            <span>New Recurring Payment:</span> <span>${parseFloat(order.ULPlanData.NewRecuringPayment).toFixed(2)}</span>
                                        </li>
                                        <li>
                                            <b>Amount to be Paid:</b> <b>${parseFloat(order.AdditionalPageData.postJson.Price).toFixed(2)}</b>
                                        </li>
                                    </ul>
                                </div> :
                                <div className='summary_card'>
                                    <ul className='list_items os_user_plan'>
                                        <li>
                                            <span>Customer</span> <b>{`${userInfo.firstname} ${userInfo.lastname}`}</b>
                                        </li>
                                        <li>
                                            <span>{order.ListItemText}:</span> <b>{order.ListItemDescription}</b>
                                        </li>
                                    </ul>
                                    {order.IsShowPromocodeDiv ?
                                        <div className='promo_form'>
                                            <div className='form-group d-flex mt-3 mb-3'>
                                                <input type='text' className='form-control promo_code me-2 form-control-lg  id="myInput' placeholder='Enter Promotion Code' value={order.Promocode} onChange={(event) => setPromocode(event.target.value)} />
                                                <button type='button' className='main_btn' onClick={() => applyPromotionCode()}>Apply</button>
                                            </div>
                                            {order.Promocode === '' && order.IsPromocodeApplied ? <p className='text-danger'>Please Enter Promotion Code</p>:''}
                                        </div> : ''}

                                    <ul className='list_items order_details'>
                                        <li>
                                            <span>Subtotal:</span> <span>${parseFloat(order.SubTotal).toFixed(2)}</span>
                                        </li>
                                        <li>
                                            <span>Discount:</span> <span>${parseFloat(order.DiscountAmount).toFixed(2)}</span>
                                        </li>
                                        <li>
                                            <span>Tax:</span> <span>${parseFloat(order.TaxAmount).toFixed(2)}</span>
                                        </li>
                                        <li>
                                            <b>Total:</b> <b>${order.GrandTotal}</b>
                                        </li>
                                    </ul>
                                </div>}
                            <div className='condi_checks pt-4'>
                                <div className='form-check'>
                                    <input className='form-check-input' type='checkbox' id='rEmail' {...(order.IsRecieveEmailReciept ? {checked: true} : '' )} onChange={(event) => {
                                        order.IsRecieveEmailReciept=event.target.checked;
                                        setOrder(order);
                                        if (newCustomerInfo !== null && Object.keys(newCustomerInfo).length > 0) {
                                            if (newCustomerInfo.customer.sEmail === null || newCustomerInfo.customer.sEmail === '') {
                                                setShowEmailPopup(true);
                                            } else {
                                                setShowEmailPopup(false);
                                            }
                                        }
                                    }} />
                                    <label className='form-check-label' htmlFor='rEmail'>
                                            Receive email receipt
                                    </label>
                                </div>
                                <div className='form-check'>
                                    <input className='form-check-input' type='checkbox' id='rSMS' onChange={(event) => {
                                        order.IsRecieveSMSReciept=event.target.checked; setOrder(order);
                                    }} />
                                    <label className='form-check-label' htmlFor='rSMS'>
                                            Receive SMS receipt
                                    </label>
                                </div>
                                <div className='form-check'>
                                    <input className='form-check-input' type='checkbox' id='termcondi' {...(order.IsCheckedTermAndCondition ? 'checked' : '')} onChange={(event) => handleChangeTerm(event)}/>
                                    <label className='form-check-label' htmlFor='termcondi'>
                                            I Agree to the <Link to='#' className='termandcondition_link' onClick={() => setModal(true)}> Terms and Conditions* </Link>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-12'>
                        <div className='d-flex justify-content-end mt-3 pt-3 border-top'>
                            <div className='btn_grps d-flex flex-wrap'>
                                <button className='back_btn icon_btn me-2' onClick={() => callbackOnPreviousPage()}><span className='icon arrow_left'></span> Back</button>
                                {/* <Link className='main_btn icon_btn' to="#" onClick={() => checkout(value)}>Checkout <span className='icon arrow_right_white'></span></Link>*/}
                                <button className='main_btn icon_btn' {...(loading ? {disabled: 'disabled'} : {})} onClick={() => {
                                    setLoading(true);
                                    processCheckout();
                                }} >{loading && <span className="spinner-border text-light btnLoader"></span>} Checkout <span className='icon arrow_right_white'></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* email popup */}
            <div className={`modal fade cw_modal add_card_pop ${showEmailPopup ? 'show d-block' : ''}`} id='addCardpop' data-bs-backdrop='static' data-bs-keyboard='false' tabIndex='-1' aria-labelledby='add_new_card_popLabel' aria-hidden='true'>
                <div className='modal-dialog modal-dialog-centered'>
                    <div className='modal-content'>
                        <form>
                            <div className='modal-body pt-4 pb-4'>
                                <label className='mb-2'>Enter email for receving receipt.</label>
                                <input
                                    type='text'
                                    name='emailReceipt'
                                    id='emailReceipt'
                                    className='form-control form-control-lg'
                                    placeholder='Email'
                                    onChange={(e) => setUserEmail(e.target.value)}
                                />
                            </div>
                            <div className='modal-footer justify-content-center'>
                                <button type='submit' id="btnAlertYes" onClick={(e)=> {
                                    e.preventDefault();
                                    // setShowEmailPopup(false);
                                    // if (customerInfo !== null && Object.keys(customerInfo).length > 0) {
                                    //     if (customerInfo.customer.sEmail !== null && customerInfo.customer.sEmail !== '') {
                                    //         setShowEmailPopup(true);
                                    //     } else {
                                    //         setShowEmailPopup(false);
                                    //     }
                                    // }
                                    handleEmail();
                                }} className='main_btn' ><span id="spanLoader" className="spinner-border text-light btnLoader" style={{display: 'none'}} />Save</button>
                                <button type='button' className='sec_btn' onClick={() => {
                                    order.IsRecieveEmailReciept= false;
                                    setOrder(order);
                                    setShowEmailPopup(false);
                                }}>Cancel</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {/* email fade */}
            {showEmailPopup ?
                <div className="modal-backdrop fade show"></div>:
                ''
            }
        </div>
    );
};
// {...(!(order.IsCardSelected && order.IsCheckedTermAndCondition) ? {disabled: 'disabled'}: {} )}
export default OrderSummary;

