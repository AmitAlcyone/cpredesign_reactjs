import {saveCreditCard} from '../../../services/order-service';
import {addNewAddedCard} from '../../../store/reducers/manage-account-reducer';
import {getPCIDetailsLocal} from '../../../utils/common-repo';

function InitializePCI(userInfo, preferedHomeLocation, dispatch, props, toast, toastId) {
    const pciConfig = getPCIDetailsLocal();
    GlobalPayments.configure({
        // publicApiKey: 'pkapi_cert_dNpEYIISXCGDDyKJiV'
        'X-GP-Api-Key': pciConfig.XGPApiKey,
        'X-GP-Environment': pciConfig.Enviornment
    });
    // Create Form
    const cardForm = GlobalPayments.ui.form({
        fields: {
            // 'card-holder-name': {
            //     placeholder: 'Card holder name.',
            //     target: '#credit-card-card-holder'
            // },
            'card-number': {
                placeholder: '•••• •••• •••• ••••',
                target: '#credit-card-card-number'
            },
            'card-expiration': {
                placeholder: 'MM / YYYY',
                target: '#credit-card-card-expiration'
            },
            'card-cvv': {
                placeholder: '•••',
                target: '#credit-card-card-cvv'
            },
            'submit': {
                value: props.noLogged == undefined ? 'Submit' : 'Checkout',
                target: '#credit-card-submit'
            }
        },
        styles: {
            'button': {
                'background': '#00b4e6 !important',
                'width': '100% !important',
                'font-size': '17px',
                'font-weight': '500',
                'border': '1px solid #00b4e6',
                'border-radius': '10px',
                'cursor': 'pointer',
                'outline': 'none',
                'color': '#fff',
                'height': '57px',
                'line-height': '1.5',
                'max-width': 'inherit',
                'position': 'absolute',
                'right': '0'
            },
            'input': {
                'display': 'block',
                'background': 'none',
                'border': 'none',
                'border-bottom': '1px solid #fefefe',
                'font-size': '1.275rem',
                'color': 'black',
                'width': '100%',
                'text-align': 'center'
            },
            'input.card-number': {
                'letter-spacing': '0.15rem',
                'border': '1px solid #e5e5e5',
                'border-radius': '10px',
                'margin-bottom': '10px',
                'width': '100%',
                'height': '57px',
                'box-sizing': 'border-box',
                'font-size': '15px',
                'outline': 'none',
                'color': '#363636',
                'background-color': '#ffffff!important',
                'padding': '0.375rem 0.75rem;'
            },
            'input.card-expiration': {
                'border': '1px solid #e5e5e5',
                'border-radius': '10px',
                'margin-bottom': '10px',
                'width': '100%',
                'height': '57px',
                'box-sizing': 'border-box',
                'font-size': '15px',
                'outline': 'none',
                'color': '#363636',
                'background-color': '#ffffff!important',
                'padding': '0.375rem 0.75rem;'
            },
            'input.card-cvv': {
                'letter-spacing': '0.35rem',
                'border': '1px solid #e5e5e5',
                'border-radius': '10px',
                'margin-bottom': '10px',
                'width': '100%',
                'height': '57px',
                'box-sizing': 'border-box',
                'font-size': '15px',
                'outline': 'none',
                'color': '#363636',
                'background-color': '#ffffff!important',
                'padding': '0.375rem 0.75rem;'
            },
            'input.invalid': {
                'border-color': '#dc3545 !important;'
            },
            'input:focus': {
                '-webkit-box-shadow': 'inset 0px 0px 3px 0px #b0dcff !important', 'box-shadow': 'inset 0px 0px 3px 0px #b0dcff !important', 'border-color': '#b0dcff !important'
            },
            'input': {
                'border-width': '1px'
            }
        }
    });

    cardForm.ready(() => {
        console.log('Registration of all credit card fields occurred');
    });

    cardForm.on('token-success', async (resp) => {
        if (props.noLogged == undefined) { // for logged in customer
            const postModel = {
            // eslint-disable-next-line camelcase
                temporary_token: resp.temporary_token,
                unixTimeStamp: Date.now(),
                CustomerIdentityId: userInfo.customerIdentityID,
                CustomerGuid: userInfo.customerGuid,
                PreferHomeLocationId: preferedHomeLocation
            };
            props.loader(true);
            const response = await saveCreditCard(userInfo.accessToken, postModel);
            props.loader(false);
            if (response.status === 200) {
                if (props.setCard!= undefined && props.setCard.length !=undefined) {
                    props.setCard(response.data.item1);
                }
                dispatch(addNewAddedCard({cC: response.data.item1}));
                toast.success(response.data.item2, {autoClose: 3000});
                // props.hidePop();
                document.getElementById('btnCCPopCancel').click();
            } else {
                toast.error('Something went wrong.', {autoClose: 3000});
            }
        } else {
            const txtMobileNo= document.getElementById('txtMobileNo');
            const regex = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;
            if (!regex.test(txtMobileNo.value)) {
                txtMobileNo.style.border = '0.5px solid #FF0000';
                toast.error('Invalid mobile no.');
                return false;
            }
            const postModel = {
                // eslint-disable-next-line camelcase
                temporary_token: resp.temporary_token,
                unixTimeStamp: Date.now(),
                phoneNumber: String(txtMobileNo.value).replace('(', '').replace(')', '').replace(' ', '').replace(' ', '')
            };

            props.checkout(postModel);
        }
    });

    cardForm.on('token-error', (resp) => {
        const error = resp.error;
        const validationDataset = [
            {
                isValid: (error.code === 'invalid_card') ? false : true,
                searchTerm: 'card_number', elementID: 'card-number',
                message: 'Invalid card number'
            },
            {
                isValid: true, searchTerm: 'expiry',
                elementID: 'card-expiration',
                message: 'Invalid expiration date'
            },
            {
                isValid: true, searchTerm: 'card_security_code',
                elementID: 'card-cvv', message: 'Invalid CVV'
            }];


        for (const validationData of validationDataset) {
            if (error.detail) {
                for (const detail of error.detail) {
                    const dataPath = detail.data_path || '';
                    const description = detail.description || '';
                    if (dataPath.search(validationData.searchTerm) > -1 || description.search(validationData.searchTerm) > -1) {
                        validationData.isValid = false;
                    }
                }
            }
            if (!validationData.isValid) {
                if (!toast.isActive(toastId.current)) {
                    toastId.current = toast.error(validationData.elementID + ' : ' + validationData.message);
                }
            }
        }
    });

    // field-level event handlers. example:
    cardForm.on('card-number', 'register', () => {
        // console.log('Registration of Card Number occurred');
    });
}

export {InitializePCI};
