import React, {useEffect} from 'react';
import {useSelector} from 'react-redux';
import {useDispatch} from 'react-redux';
import InputMask from 'react-input-mask';
import {InitializePCI as intializePCIGatway} from './global-payment';
import {toast} from 'react-toastify';

const PaymentCard = (props) => { // for logged in users
    toast.configure();
    const toastId = React.useRef(null);
    const handleChangeMobileNo = (e) => {
        const regex = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;
        const txtMobileNo= document.getElementById('txtMobileNo');
        if (!regex.test(e.target.value)) {
            txtMobileNo.style.border = '0.5px solid #FF0000';
        } else {
            txtMobileNo.removeAttribute('style');
        }
    };

    if (props.noLogged == undefined) {
        const userInfo = useSelector((state) => state.auth.userInfo);
        const preferedHomeLocation = useSelector((state) => state.auth.userInfo.preferedHomeLocation);
        const dispatch = useDispatch();
        useEffect(() => {
            intializePCIGatway(userInfo, preferedHomeLocation, dispatch, props, toast, toastId);
        }, []);
    } else { // for annomous
        useEffect(() => {
            intializePCIGatway(null, null, null, props, toast, toastId);
        }, []);
    }

    return (
        <div>
            <form id="payment-form" >
                <h5 className='mb-3'>{(props.noLogged == undefined) ? 'Add Credit Card' : 'Enter card details'}</h5>
                <div className="row">
                    { (props.noLogged != undefined) ? <div className="col-sm-12">
                        <div className='mb-2'>
                            <label htmlFor="phone"><small>Phone<sup className="text-danger">*</sup></small></label>
                            <InputMask
                                id="txtMobileNo"
                                className="form-control form-control-lg"
                                onChange={(e) => handleChangeMobileNo(e)}
                                // onBlur={handleBlur}
                                mask="(999) 999 9999"
                                maskChar=" "
                                // value={mobileNo}
                                onKeyPress={(e) => handleChangeMobileNo(e)}
                                name="phone"
                                placeholder="(000) 000 0000"
                            />
                        </div>
                    </div> : '' }
                    <div className="col-sm-12">
                        <div className='mb-2'>
                            <label htmlFor="ccNumber"><small>Credit Card Number</small></label>
                            <div id="credit-card-card-number"></div>
                        </div>
                    </div>
                    <div className="col-sm-12 col-md-6">
                        <div className='mb-2'>
                            <label htmlFor="ccNumber"><small>Expiration Date</small></label>
                            <div id="credit-card-card-expiration"></div>
                        </div>
                    </div>
                    <div className="col-sm-12 col-md-6">
                        <div className='mb-2'>
                            <label htmlFor="ccNumber"><small>CVV</small></label>
                            <div id="credit-card-card-cvv"></div>
                        </div>
                    </div>
                    { (props.noLogged == undefined) ? <div className="col-sm-12">
                        <div id="credit-card-submit"></div>
                    </div> : '' }
                </div>
            </form>
        </div>
    );
};

export default PaymentCard;
