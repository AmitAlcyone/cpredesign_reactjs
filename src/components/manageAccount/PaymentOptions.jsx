import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import Layout from './../../components/Layout';
import SkeletonOrderSummaryCC from './../../utils/skeletons/SkeletonOrderSummaryCC';
import {useSelector} from 'react-redux';
import {creditCardsList, deleteCreditCard} from './../../services/manage-account-service';
import {GetCreditCardIcon as getCCIcon, getPCIDetailsLocal} from '../../utils/common-repo';
import {useDispatch} from 'react-redux';
import {setCreditCards} from '../../store/reducers/manage-account-reducer';
import {toast} from 'react-toastify';
import AlertModalModule from '../modals/AlertModal';
import FullScreenLoader from '../../utils/loaders/FullScreenLoader';
import PaymentCard from '../order/PCI/payment-card';

const PaymentOptions = (props) => {
    const dispatch = useDispatch();
    const [modal, setModal] = useState(false);
    const creditCards = useSelector((state) => state.manageAccount.creditCards);
    const [isLoading, setIsLoading] = useState(creditCards.length ? false : true);
    const [CardId, setCardId] = useState('');
    const [viewCardForm, setViewCardForm] = useState(false);
    const userInfo = useSelector((state) => state.auth.userInfo);
    const [Loader, setLoader] = useState(false);

    const contentLoader = () => {
        const content = [];
        for (let index = 0; index < 5; index++) {
            content.push(<SkeletonOrderSummaryCC key={index} />);
        }
        return content;
    };

    const expandPCIForm = (e) => {
        e.preventDefault();
        e.stopPropagation();
        setViewCardForm(true);
    };

    const deletePaymentCard = async () => {
        setIsLoading(true);
        const response = await deleteCreditCard(userInfo.accessToken, CardId);
        if (response.status === 200) {
            setIsLoading(false);
            getCreditCardsList(true);
            toast.success('Credit Card Deleted Successfully.', {autoClose: 3000});
        } else {
            setIsLoading(false);
            toast.error('Something went wrong.', {autoClose: 3000});
        }
    };
    const [selectedPaymentOption, setSelectedPaymentOption] = useState('');
    const [alertModalData, setAlertModalData] = useState({});

    const setPaymentOption = () => {
        setModal(false);
    };
    const handleShow = (paymentOption, id) => {
        setCardId(id);
        setAlertModalData({
            modalClass: 'warning_pop',
            title: 'Confirmation',
            message: 'Do you want to Delete '+''+' the Current data ' + selectedPaymentOption + '?',
            btnSuccessText: 'Yes',
            btnCancelText: 'No'
        });
        setModal(true);
        setSelectedPaymentOption(paymentOption);
    };

    const getCreditCardsList = async (isForceLoad) => {
        if (creditCards.length === 0 || isForceLoad) {
            setIsLoading(true);
            const response = await creditCardsList(userInfo.accessToken, userInfo.customerIdentityID);
            if (response.status === 200) {
                setTimeout(() => {
                    setIsLoading(false);
                    dispatch(setCreditCards({creditCards: response.data}));
                }, 1000);
            } else {
                setIsLoading(false);
            }
        }
    };

    const PCIScript= getPCIDetailsLocal().PCIScript;

    useEffect(() => {
        const script = document.createElement('script');
        script.src = PCIScript;
        script.async = false;
        document.body.appendChild(script);

        getCreditCardsList(false);

        return () => {
            document.body.removeChild(script);
        };
    }, [PCIScript]);

    return (
        <Layout>
            {Loader === true ? <FullScreenLoader /> : ''}
            <div className='container'>
                <div className='row'>
                    <div className='col-12'>
                        <Link to='/manage_account' className='back_link icon_btn'><span className='icon arrow_left'></span> Back</Link>
                    </div>
                    <div className='col-12'>
                        <div className='d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap'>
                            <div className='main_title'>
                                <h2>Manage Payment Options</h2>
                                <p>Your credit cards</p>
                            </div>
                            <button type='button' className='main_btn' data-bs-toggle='modal'
                                data-bs-target='#addCardpop' disabled={isLoading} onClick={(event) => expandPCIForm(event)}>
                                {isLoading && <span className="spinner-border text-light btnLoader"></span>}<span>+ Add a New Card</span></button>
                        </div>
                    </div>
                </div>
                {modal ? <AlertModalModule data={alertModalData} targetEvent={() => {
                    setPaymentOption();
                    deletePaymentCard();
                }} closeModal={() => setModal(false)} /> : ''}
                <div className='row'>
                    <div className='col-lg-12'>
                        <div className='pay_method_list'>
                            <div className='table-responsive border-bottom'>
                                <table className='table ccards_table table-borderless'>
                                    <thead>
                                        <tr>
                                            <th>Credit card details</th>
                                            <th>Expires on</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {isLoading && contentLoader()}
                                        {(creditCards.length > 0 && !isLoading) ? creditCards.map((cc, index) => {
                                            const ccDetail= getCCIcon(cc);
                                            return (
                                                <tr key={index}>
                                                    <td>
                                                        <div className='card_info'>
                                                            <label className='form-check-label' htmlFor='cardOne'>
                                                                <b>{ccDetail.name}</b> <span>ending in {cc.sSpan}</span>
                                                                <div className='card_type'>
                                                                    <span className={`card_icon ${ccDetail.icon}`}></span>
                                                                </div>
                                                                {cc.bIsRecurring!=null && cc.bIsRecurring ?
                                                                    <div className='rc_card_badge ms-2'>Recurring Card</div> : '' }
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td><div className='exp_date'>{cc.sExpiredMonth}/{cc.sExpireYear}</div></td>
                                                    <td><div className='remove_btn' onClick={() => handleShow(cc.sSpan, cc.id)}><span className='icon trash_icon' title="Delete"></span></div>&nbsp; &nbsp;
                                                    </td>
                                                </tr>
                                            );
                                        }) : ''}
                                    </tbody>
                                </table>
                                {creditCards.length === 0 && isLoading === false && <p className='notFound'>Credit cards not available.</p>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='modal fade cw_modal add_card_pop' id='addCardpop' data-bs-backdrop='static' data-bs-keyboard='false' tabIndex='-1' aria-labelledby='add_new_card_popLabel' aria-hidden='true'>
                <div className='modal-dialog modal-dialog-centered'>
                    <div className='modal-content'>
                        {/* <div className='modal-header'>
                                    <h5 className='modal-title' id='add_new_card_popLabel'>Add Credit Card</h5>
                                </div> */}
                        <button type="button" className="btn-close" id="btnCCPopCancel" data-bs-dismiss="modal" onClick={() => {
                            setViewCardForm(false);
                        }}></button>
                        <div className='modal-body'>
                            {(viewCardForm) ? <PaymentCard loader={(val) => setLoader(val)} hidePop={() => setViewCardForm(false)} /> : ''}
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default PaymentOptions;
