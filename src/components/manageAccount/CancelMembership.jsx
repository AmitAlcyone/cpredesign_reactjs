import React, {useEffect, useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import Layout from '../../components/Layout';
import {setServices} from './../../store/reducers/manage-account-reducer';
import {useDispatch, useSelector} from 'react-redux';
import {cancelMembershipList, cancelMemberShip} from './../../services/cancel-membership-service';
import AlertModalModule from '../modals/AlertModal';
import {toast} from 'react-toastify';
import FullScreenLoader from '../../utils/loaders/FullScreenLoader';

const CancelMembership = (props) => {
    toast.configure();
    const navigate = useNavigate();
    const userInfo = useSelector((state) => state.auth.userInfo);
    const services = useSelector((state) => state.manageAccount.services);
    const dispatch = useDispatch();
    const [cancelId, setCancelId] = useState(null);
    const onChangeCancelReason = (cancelId) => {
        setCancelId(cancelId);
    };

    const [modal, setModal] = useState(false);
    const [alertModalData, setAlertModalData]= useState({});
    const [Loader, setLoader] = useState(false);

    const showConfirmationCancelMemberShip = () => {
        setAlertModalData({
            modalClass: 'warning_pop',
            title: 'Cancel membership!',
            message: 'Are you sure want to cancel membership ?',
            btnSuccessText: 'Yes',
            btnCancelText: 'No',
            btnPrimary: false
        });
        setModal(true);
    };

    const processCancelMembership = async () => {
        setLoader(true);
        const postData = {};
        postData.CustomerGuid = userInfo.customerGuid;
        postData.CustomerIdentityId = userInfo.customerIdentityID;
        postData.CancelId= cancelId;
        postData.Barcode= '';
        const response = await cancelMemberShip(userInfo.accessToken, postData);
        if (response.status === 200) {
            setLoader(false);
            toast.success('Membership cancel successfully.');
            setModal(false);
            return navigate('/my-account');
        } else {
            setLoader(false);
            toast.error('Some error occurred.', {autoClose: 3000});
        }
    };

    useEffect(() => {
        const getServies = async () => {
            if (services.length === 0) {
                setLoader(true);
                const response = await cancelMembershipList(userInfo.accessToken);
                if (response.status === 200) {
                    dispatch(setServices({services: response.data}));
                }
                setLoader(false);
            }
        };
        getServies();
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }, []);

    return (
        <>
            {Loader === true ? <FullScreenLoader /> : ''}
            <Layout>
                {modal ? <AlertModalModule data={alertModalData} targetEvent={() => processCancelMembership()} closeModal={() => setModal(false)} /> : '' }
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <div className='main_title mb-4 mt-3'>
                                <h2>Select reason for cancelling membership</h2>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-12'>
                            <form action='' className='cancel_reason_list border-top pt-3 '>
                                {services.map((service, index) => {
                                    return (
                                        <div className='form-check' key={index} onChange={() => onChangeCancelReason(service.lCustomerCancelReasonId)}>
                                            <><input className='form-check-input' type='radio' name='flexRadioDefault' key={service.lCustomerCancelReasonId} /><label className='form-check-label' htmlFor='1'>
                                                {service.sDescription}
                                            </label></>

                                        </div>
                                    );
                                })}
                            </form>
                            {!services.length && Loader === false ? <p className='notFound'>Services not available.</p> : ''}
                            <div className='d-flex justify-content-end mt-3 pt-3 border-top'>
                                <div className='btn_grps d-flex flex-wrap'>
                                    <Link className='back_btn icon_btn me-2' to='/my-account'><span className='icon arrow_left'></span> Back</Link>
                                    { cancelId > 0 ? (
                                        <button className='main_btn icon_btn' onClick={() => showConfirmationCancelMemberShip()}>Continue <span className='icon arrow_right_white'></span></button>
                                    ) : (
                                        <Link className='main_btn icon_btn' to='' style={{cursor: 'not-allowed'}} >Continue <span className='icon arrow_right_white'></span></Link>
                                    )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        </>
    );
};

export default CancelMembership;
