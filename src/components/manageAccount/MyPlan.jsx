import React from 'react';
import Layout from '../../Layout';
import Serfive from '../../assets/images/icons/unlimited_plans.svg';
import BarcodeImg from '../../assets/images/bar_code_img.jpg';
import {Link} from 'react-router-dom';

const MyPlan = (props) => {
    return (
        <Layout>
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="main_title mb-4 mt-3">
                            <h2>My Unlimited Wash Program</h2>
                            <p>Your unlimited code: $AVN5Y1OUXMC</p>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="border-top pt-4">
                            <div className="my_ultd_plan">
                                <div className="mup_head d-flex align-items-center">
                                    <div className="ser_img me-3"><img src={Serfive} className="img-fluid" alt=""/></div>
                                    <h3>Extreme 15 Days Unlimited</h3>
                                </div>
                                <div className="mup_body">
                                    <ul className="mup_info">
                                        <li>Start Date: <span>12/08/2021</span></li>
                                        <li>Expiration Date: <span>12/17/2021</span></li>
                                        <li>Last Payment on: <span>12/17/2021</span></li>
                                        <li>Payment method: <span>(Visa) Ending in XXXX</span></li>
                                        <li className="mup_foot"><a data-bs-toggle="collapse" href="#viewBarcode" role="button" aria-expanded="false" aria-controls="viewBarcode">View Barcode</a></li>
                                    </ul>
                                    <div className="view_barcode collapse" id="viewBarcode">
                                        <div className="pt-4 border-top">
                                            <div className="barcode_img mx-auto text-center"><img src={BarcodeImg} className="img-fluid" alt=""/></div>
                                            <div className="barcode_number text-center p-4">
                                                <small>Your Redeem Code:</small>
                                                <h3 className="mt-2">1YWR6HVURW</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="btn_grp mt-4">
                                <Link to="/add_new_ultd_plan" className="main_btn me-2">Change Membership</Link>
                                <Link to="/cancel_membership" className="sec_btn">Cancel Membership</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default MyPlan;
