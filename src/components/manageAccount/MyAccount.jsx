import React, {useEffect, useState} from 'react';
import Layout from '../Layout';
import UserIcon from '../../assets/images/user.png';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {getCustomerInfoByGuid} from '../../services/user-services';
// import {setCustomerInfo} from '../../store/reducers/user-info-reducer';
import customerSchema from '../../validation-schemas/updateCustomerInfoSchema';
import {useFormik} from 'formik';
import InputMask from 'react-input-mask';
import {putCustomerInfoByGuid} from '../../services/user-services';
import UnlimitedPlanListModule from '../unlimitedPlans/UnlimitedPlans';
import {toast} from 'react-toastify';
import OrderCheckout from '../order/order-checkout';
// import {updateUserInfoOnUpdateUser} from '../../store/reducers/auth-reducer';
import ServiceCardLoader from '../../utils/loaders/serviceCardLoader';
import {GetCreditCardIcon as getCCIcon} from '../../utils/common-repo';
import {setCreditCards} from '../../store/reducers/manage-account-reducer';
import {creditCardsList, updateCreditCardInfo} from '../../services/manage-account-service';
import AlertModalModule from '../modals/AlertModal';
import {isStringNullOrEmpty} from '../../utils/common-repo';
import FullScreenLoader from '../../utils/loaders/FullScreenLoader';


const MyAccount = (props) => {
    toast.configure();
    const [loading, setLoading] = useState(false);
    const [isCCLoad, setIsCCLoad] = useState(false);
    const [showLoader, setShowLoader] = useState(false);
    const [Loader, setLoader] = useState(false);
    const dispatch = useDispatch();
    const userInfo = useSelector((state) => state.auth.userInfo);
    const previousPage = useSelector((state) => state.auth.previousPage);
    const customerInfo = JSON.parse(localStorage.getItem('customerDetail'));
    let firstName = '';
    let lastName = '';
    if (customerInfo !== null) {
        firstName = customerInfo.customer.sFirstName;
        lastName = customerInfo.customer.sLastName;
    }
    if (isStringNullOrEmpty(firstName) && !isStringNullOrEmpty(userInfo.firstname)) {
        firstName = userInfo.firstname;
    }
    if (isStringNullOrEmpty(lastName) && isStringNullOrEmpty(userInfo.lastname)) {
        lastName = userInfo.lastName;
    }
    // const accessToken = userInfo.accessToken;
    // const phoneNumber = useSelector((state) => state.auth.phoneNumber);
    const [isUWCCheckoutShow, setIsUWCCheckoutShow] = useState(false);
    const [orderJson, setOrderJson] = useState({});
    const creditCards = useSelector((state) => state.manageAccount.creditCards);
    const [preferredCard, setPreferredCard]=useState('');
    const preferredCardDetails= creditCards.filter((creditCards) => creditCards.sPayerIdentifier == preferredCard);
    const [customerData, setCustomerData]= useState([]);
    const [isShowULPlans, setIsShowULPlans]= useState(false);
    const [modal, setModal] = useState(false);
    const [alertModalData, setAlertModalData]= useState({});
    const [newPreferredCard, setNewPreferredCard]= useState({});
    const [vipAccountData, setVipAccountData]= useState({});


    const userFormik = useFormik({
        initialValues: {
            firstName: firstName,
            lastName: lastName,
            phoneNumber: localStorage.getItem('phoneNumber'),
            email: customerInfo.customer.sEmail == null ? '' : customerInfo.customer.sEmail
        },
        enableReinitialize: true,
        validationSchema: customerSchema,
        onSubmit: async (values, {resetForm}) => {
            setLoader(true);
            setLoading(true);
            values.phoneNumber = (values.phoneNumber).replace(/([() ])+/g, '');
            values.customerGuid = userInfo.customerGuid;
            // values.preferredWorkLocation = '';
            const response = await putCustomerInfoByGuid(userInfo.accessToken, values);
            if (response.status === 200) {
                setLoader(false);
                setLoading(false);
                toast.success('Success', {autoClose: 3000});
                getCustomerInfo();
            } else {
                toast.error('Some error occurred.', {autoClose: 3000});
                setLoading(false);
            }
        }
    });

    const showUWCCheckoutView = (order) => {
        setOrderJson(order);
        setIsShowULPlans(false);
        setIsUWCCheckoutShow(true);
    };

    const getCreditCardsList = async () => {
        if (creditCards.length === 0) {
            setIsCCLoad(true);
            const response = await creditCardsList(userInfo.accessToken, userInfo.customerIdentityID);
            setIsCCLoad(false);
            if (response.status === 200) {
                // setTimeout(() => {
                dispatch(setCreditCards({creditCards: response.data}));
                // }, 1000);
            } else {
                dispatch(setCreditCards({creditCards: []}));
            }
        }
    };

    const getCustomerInfo = async () => {
        setCustomerData([]);
        // props.setParentLoader(true);
        setLoader(true);
        setShowLoader(true);
        const response = await getCustomerInfoByGuid(userInfo.accessToken, userInfo.customerGuid);
        // props.setParentLoader(false);
        if (response.status == 200) {
            localStorage.setItem('customerDetail', JSON.stringify(response.data));
            setCustomerData(response.data);
            setVipAccountData(response.data.vipAccountType);
            setPreferredCard(response.data.customerDetail.sPayerIdentifier);
        } else if (response.status === (401 | 404 | 470)) {
            toast.warning(`${response.data.title}.`, {autoClose: 3000});
        } else {
            toast.error('Some error occurred.', {autoClose: 3000});
        }
        setLoader(false);
        setShowLoader(false);
    };

    useEffect(() => {
        getCreditCardsList();
        getCustomerInfo();
    }, []);

    const showChangePreferredCardConfirmationPopup = (card) => {
        setAlertModalData({
            modalClass: 'warning_pop',
            title: 'Change preferred card',
            message: 'Do you want to change preferred card?',
            btnSuccessText: 'Yes',
            btnCancelText: 'No',
            btnPrimary: true
        });
        setModal(true);
        setNewPreferredCard(card);
    };

    const submitChangePreferredCard = async () => {
        const expiryDate= `${String(newPreferredCard.sExpiredMonth)}${String(newPreferredCard.sExpireYear.length > 2 ? newPreferredCard.sExpireYear.substr(2, 2) : newPreferredCard.sExpireYear)}`;
        const postJson= {
            'customerGUID': userInfo.customerGuid,
            'sMaskedCCNumber': '',
            'sCCExpDate': expiryDate,
            'token': newPreferredCard.sPayerIdentifier,
            'sCCType': '',
            'sCCHolderName': '',
            'customerIdentityID': userInfo.customerIdentityID,
            'sBin': newPreferredCard.sBin,
            'sSpan': newPreferredCard.sSpan
        };
        const button = document.getElementById('btnAlertYes');
        const btnSpan= document.getElementById('spanLoader');
        button.disabled=true;
        btnSpan.style.display='block';
        const response = await updateCreditCardInfo(userInfo.accessToken, postJson);
        button.disabled=false;
        btnSpan.style.display='block';
        if (response.status == 200) {
            getCustomerInfo();
            setModal(false);
        } else if (response.status === (401 | 404 | 470)) {
            toast.warning(`${response.data.title}.`, {autoClose: 3000});
        } else {
            toast.error('Some error occurred.', {autoClose: 3000});
        }
    };
    return (
        <Layout>
            {Loader === true ? <FullScreenLoader /> : ''}
            {modal ? <AlertModalModule data={alertModalData} showLoad={showLoader} targetEvent={() => submitChangePreferredCard()} closeModal={() => setModal(false)} /> : '' }
            {isUWCCheckoutShow ? <OrderCheckout hideCheckoutPage={(isCheckout) => {
                if (isCheckout) {
                    getCustomerInfo();
                }
                setIsUWCCheckoutShow(false);
            }} order={orderJson} /> :
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <Link to={previousPage} className='back_link icon_btn'><span className='icon arrow_left'></span> Back</Link>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-md-12 col-sm-6 col-12'>
                            <div className='profile_card d-flex pt-3 pb-3 align-items-center mb-4'>
                                <div className='mac_img'><img src={UserIcon} className='img-fluid' alt='' /></div>
                                <div>
                                    <h4 style={{textTransform: 'capitalize'}}>{firstName} {lastName}</h4>
                                    <Link to='' data-bs-toggle='modal' data-bs-target='#updatePersonalInfo'>Update personal info</Link>
                                </div>
                            </div>
                        </div>
                        <ServiceCardLoader data={{height: 180, class: 'col-md-4 col-sm-6 col-12', isLoad: showLoader}}></ServiceCardLoader>
                        <div className='col-md-4 col-sm-6 col-12' style={{display: showLoader ? 'none' : 'block'}}>
                            <div className='ma_card'>
                                <div className='mac_info'>
                                    <h4>My Unlimited Plan</h4>
                                    {customerInfo.customerDetail.bMonthlyActive?
                                        <div className="mp_info"><div className="mp_name">{vipAccountData.sDescription ?? ''}<span className='best_value ms-2'>Current Membership</span></div><div className="mp_price">${vipAccountData.dblAmount ?? ''}</div></div> :
                                        'Currently you have no unlimited plans'
                                    }
                                    <a className='d-inline-block mt-3 collapsed' data-bs-toggle='collapse' href='#planExpand' role='button' aria-expanded='false' onClick={() => {
                                        setIsShowULPlans(isShowULPlans ? false : true);
                                    }} aria-controls='planExpand'>See more plans</a>
                                </div>
                                <div className='mac_more_info collapse pt-3' id='planExpand'>
                                    {(customerData !=null && customerData.customerDetail !=undefined && isShowULPlans) ?
                                        <UnlimitedPlanListModule goToUWCCheckout={(order) => showUWCCheckoutView(order)} custData={customerData} setPreferredCard={(sPayerIdentifier) => setPreferredCard(sPayerIdentifier)}/> : '' }
                                </div>
                            </div>
                        </div>
                        <ServiceCardLoader data={{height: 180, class: 'col-md-4 col-sm-6 col-12', isLoad: (isCCLoad || showLoader)}}></ServiceCardLoader>
                        <div className='col-md-4 col-sm-6 col-12' style={{display: (isCCLoad || showLoader) ? 'none' : 'block'}}>
                            <div className='ma_card'>
                                <div className='mac_info'>
                                    <div>
                                        {/* <h4>Payment Method</h4> */}
                                        <h4>Preferred Payment method</h4>
                                        <ul>
                                            { preferredCardDetails!=null && preferredCardDetails.length > 0 ?
                                                <><li>{getCCIcon(preferredCardDetails[0]).name} credit card ending with {preferredCardDetails[0].sSpan}</li><li>Expires on: {preferredCardDetails[0].sExpiredMonth}/{preferredCardDetails[0].sExpireYear}</li></> :
                                                <li>No preferred card found.</li>
                                            }
                                        </ul>
                                    </div>
                                    <a className='d-inline-block mt-3 collapsed' data-bs-toggle='collapse' href='#payMethodExpand' role='button' aria-expanded='false' aria-controls='payMethodExpand'>Update your payment method</a>
                                </div>
                                <div className='mac_more_info collapse pt-3' id='payMethodExpand'>
                                    <div className='more_mm'>
                                        <div className='mm_info'>
                                            {/* <p>Last Payment</p>
                                            <div className='mm_name mt-2 mb-3'>March 16, 2022</div> */}
                                            <Link to="/order-history">View all payment receipts</Link>
                                        </div>
                                        {/* <div>
                                            {preferredCardDetails.map((card, index) => {
                                                const ccDetail= getCCIcon(card);
                                                return (
                                                    <div className='mm_info' key={index}>
                                                        <p>Preferred Payment method</p>
                                                        <div className='mm_name mt-2 mb-3'>{ccDetail.name} credit card ending with {card.sSpan}</div>
                                                        <Link to="/payment-options">Edit payment method</Link>
                                                    </div>
                                                );
                                            })}
                                        </div> */}

                                        <div className='mm_info pb-0'>
                                            <a className='d-inline-block mt-3 collapsed' data-bs-toggle='collapse' href='#paymentMethods' role='button' aria-expanded='false' aria-controls='payMethodExpand' style={{paddingBottom: '10px'}}>Change your payment method</a>
                                            <div className='mac_more_info collapse pt-3' id='paymentMethods' style={{overflowY: 'scroll', overflowX: 'none', maxHeight: '320px'}}>
                                                <ServiceCardLoader data={{height: 120, isLoad: creditCards.length==0, wcBox: false, class: 'service_box', width: 250}}></ServiceCardLoader>
                                                {creditCards.length > 0 && creditCards.map((card, index) => {
                                                    const ccDetail= getCCIcon(card);

                                                    if (card.sPayerIdentifier == preferredCard) {
                                                        return ( <div className={`service_box cpm_card active preferred`} style={{cursor: 'not-allowed', width: '95%'}} key={index} >
                                                            <div className='ser_img'>
                                                                <div className='card_type'>
                                                                    <span className={`card_icon ${ccDetail.icon}`}></span>
                                                                </div>
                                                            </div>
                                                            <div className='ser_info'>
                                                                <h3><b>{ccDetail.name}</b> <span>ending in {card.sSpan}</span></h3>
                                                                <span>{card.sExpiredMonth}/{card.sExpireYear}</span>
                                                            </div>
                                                        </div> );
                                                    } else {
                                                        return ( <div className="service_box cpm_card" onClick={() => showChangePreferredCardConfirmationPopup(card)} key={index} style={{width: '95%'}}>
                                                            <div className='ser_img'>
                                                                <div className='card_type'>
                                                                    <span className={`card_icon ${ccDetail.icon}`}></span>
                                                                </div>
                                                            </div>
                                                            <div className='ser_info'>
                                                                <h3><b>{ccDetail.name}</b> <span>ending in {card.sSpan}</span></h3>
                                                                <span>{card.sExpiredMonth}/{card.sExpireYear}</span>
                                                            </div>
                                                        </div>);
                                                    }
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!customerInfo.customerDetail.bMonthlyActive ? '' :
                            <div className='col-md-4 col-sm-6 col-12' style={{display: showLoader ? 'none' : 'block'}}>
                                <div className='ma_card'>
                                    <div className='mac_info'>
                                        <div>
                                            <h4>Manage Membership</h4>
                                            <ul>
                                                <li>Unlimited Wash Plan</li>
                                            </ul>
                                        </div>
                                        <a className='d-inline-block mt-3 collapsed' data-bs-toggle='collapse' href='#manageExpand' role='button' aria-expanded='false' aria-controls='manageExpand'>Cancel plan</a>
                                    </div>
                                    <div className='mac_more_info collapse pt-3' id='manageExpand'>
                                        <div className='more_mm'>
                                            <div className='mm_info'>
                                                <div className='mm_name'>End Membership</div>
                                                <p>By ending your membership you will lose access to your unlimited plan benefits</p>
                                                <Link to="/cancel-membership" className='mt-3 d-inline-block'>Cancel Membership</Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }

                        <div className='modal fade cw_modal add_info_pop' id='updatePersonalInfo' data-bs-backdrop='static' data-bs-keyboard='false' tabIndex='-1' aria-labelledby='updatePersonalInfoLabel' aria-hidden='true'>
                            <div className='modal-dialog modal-dialog-centered '>
                                <div className='modal-content personalinfo'>
                                    <form onSubmit={userFormik.handleSubmit}>
                                        <div className='modal-header'>
                                            <h5 className='modal-title' id='updatePersonalInfoLabel'>Personal Info</h5>
                                        </div>
                                        <div className='modal-body'>
                                            <div className='row'>
                                                <div className='col-6'>
                                                    <div className='input_wrap mb-4'>
                                                        <label htmlFor='firstName' className='form-label'>First Name <sup className='text-danger'></sup></ label>
                                                        <input type='text' name='firstName' id='firstName' className='form-control form-control-lg' placeholder=''
                                                            onChange={userFormik.handleChange}
                                                            onBlur={userFormik.handleBlur}
                                                            value={userFormik.values.firstName}
                                                        />
                                                        {userFormik.touched.firstName && userFormik.errors.firstName ? (
                                                            <small className="text-danger validationError">{userFormik.errors.firstName}</small>
                                                        ) : null}

                                                    </div>
                                                </div>
                                                <div className='col-6'>
                                                    <div className='input_wrap mb-4'>
                                                        <label htmlFor='lastName' className='form-label'>Last Name <sup className='text-danger'></sup></ label>
                                                        <input type='text' id='lastName' name='lastName' className='form-control form-control-lg' placeholder=''
                                                            onChange={userFormik.handleChange}
                                                            onBlur={userFormik.handleBlur}
                                                            value={userFormik.values.lastName}
                                                        />
                                                        {userFormik.touched.lastName && userFormik.errors.lastName ? (
                                                            <small className="text-danger validationError">{userFormik.errors.lastName}</small>
                                                        ) : null}
                                                    </div>
                                                </div>
                                                <div className='col-6'>
                                                    <div className='input_wrap mb-4'>
                                                        <label htmlFor='phoneNumber' className='form-label'>Mobile Number <sup className='text-danger'></sup></ label>
                                                        {/* <input type='text' name='phoneNumber' id='phoneNumber' className='form-control form-control-lg' placeholder='' value={formik.values.phoneNumber} /> */}
                                                        <InputMask
                                                            className="form-control form-control-lg"
                                                            readOnly={true}
                                                            mask="(999) 999 9999"
                                                            maskChar=" "
                                                            value={userFormik.values.phoneNumber}
                                                            name="phoneNumber"
                                                            placeholder="(000) 000 0000"
                                                        />
                                                        {userFormik.touched.phoneNumber && userFormik.errors.phoneNumber ? (
                                                            <small className="text-danger validationError">{userFormik.errors.phoneNumber}</small>
                                                        ) : null}
                                                    </div>
                                                </div>
                                                <div className='col-6'>
                                                    <div className='input_wrap mb-4'>
                                                        <label htmlFor='email' className='form-label'>Email Address <sup className='text-danger'></ sup></label>
                                                        {/* <input type='email' id='repEmail' className='form-control form-control-lg' placeholder='' value={userInfo ? `${phoneNumber}` : ''} /> */}
                                                        <input type='email' name='email' id='email' className='form-control form-control-lg' value={userFormik.values.email} onChange={userFormik.handleChange} onBlur={userFormik.handleBlur} placeholder=''/>
                                                        {userFormik.touched.email && userFormik.errors.email ? (
                                                            <small className="text-danger">{userFormik.errors.email}</small>
                                                        ) : null}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='modal-footer justify-content-start'>
                                            <button type='submit' className='main_btn' disabled={loading || !userFormik.isValid} data-bs-dismiss='modal'>Save</button>
                                            <button type='button' className='sec_btn' data-bs-dismiss='modal' onClick={() => userFormik.resetForm()}>Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </Layout>
    );
};

export default MyAccount;
