import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import Layout from '../Layout';
import Serone from '../../assets/images/icons/wash.svg';
import {setwalletServices} from '../../store/reducers/wallet-reducer';
import ServiceCardLoader from './../../utils/loaders/serviceCardLoader';

import {wallet} from './../../services/wallet-service';
// import {setwalletServices, setSelectedWallet} from '../store/reducers/wallet-reducer';
import {setSelectedWallet} from '../../store/reducers/wallet-reducer';
import {toast} from 'react-toastify';
import {useSelector} from 'react-redux';
import {useDispatch} from 'react-redux';
import dateFormatter from '../../utils/dateFormatter';

const OrderHistory = (props) => {
    const userInfo = useSelector((state) => state.auth.userInfo);
    const services = useSelector((state) => state.wallet.services);
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(services.length ? false : true);
    const [pageServices, setPageServices] = useState([]);

    const handleSelectedWallet = (wallet) => {
        const upDatedWallet={...wallet, pageBackUrl: '/order-history'};
        dispatch(setSelectedWallet({selectedWallet: upDatedWallet}));
    };

    useEffect(() => {
        const getServices = async () => {
            setPageServices(services);
            if (services.length === 0) {
                const response = await wallet(userInfo.accessToken, userInfo.customerIdentityID);
                if (response.status === 200) {
                    setTimeout(() => {
                        setIsLoading(false);
                        dispatch(setwalletServices({services: response.data}));
                        setPageServices(response.data);
                    }, 1000);
                } else {
                    setIsLoading(false);
                    toast.error('Something went wrong.', {autoClose: 3000});
                }
            }
        };
        getServices();
    }, []);
    return (
        <Layout>
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <Link to="/manage_account" className="back_link icon_btn"><span className="icon arrow_left"></span> Back</Link>
                    </div>
                    <div className="col-12">
                        <div className="main_title mb-4 mt-3">
                            <h2>Order History</h2>
                            <p>Manage your account, orders and much more...</p>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className='tab-content' id='pills-tabContent' style={{minHeight: 'auto'}}>
                        {services.length === 0 && isLoading === false ? <p className='notFound'>Services not available.</p> : ''}
                        <div className='tab-pane fade show active' id='pills-all' role='tabpanel' aria-labelledby='pills-all-tab'></div>
                        <div className='tab-pane fade' id='pills-washcard' role='tabpanel' aria-labelledby='pills-washcard-tab'></div>
                        <div className='tab-pane fade' id='pills-giftcard' role='tabpanel' aria-labelledby='pills-giftcard-tab'></div>
                    </div>
                    {isLoading &&
                        <div className='row wash_list'>
                            <ServiceCardLoader></ServiceCardLoader>
                            <ServiceCardLoader></ServiceCardLoader>
                            <ServiceCardLoader></ServiceCardLoader>
                        </div>
                    }
                    <div className="col-12">
                        <div className="order_list">
                            <div className="row">
                                {
                                    pageServices.length >0 && pageServices.map((service, index) => {
                                        // const [date, time] = (service.walletDate !== null ? service.walletDate.split('T') : '');
                                        return (
                                            <div className="col-md-4 col-sm-6 col-12" key={index}>
                                                <div className="order_box">
                                                    <div className="od_head">
                                                        <b className="od_date">{dateFormatter(service.walletDate)}</b>
                                                    </div>
                                                    <div className="od_item_info d-flex align-items-center">
                                                        <div className="ser_img"><img src={Serone} className="img-fluid" alt="" /></div>
                                                        <div className="ser_info">
                                                            <h3>{service.description}</h3>
                                                        </div>
                                                    </div>
                                                    <div className="od_trans_info">
                                                        <div>
                                                            <small>Total</small>
                                                            <span>${service.amount}</span>
                                                        </div>
                                                        <div>
                                                            <small>Order Number</small>
                                                            <span>{service.barcode}</span>
                                                        </div>
                                                    </div>
                                                    <div className="od_foot">
                                                        <Link to='/wallet-barcode' className="main_btn"
                                                            onClick={() => handleSelectedWallet(service)}
                                                        > View Receipt</Link>
                                                    </div>
                                                </div>
                                            </div>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default OrderHistory;
