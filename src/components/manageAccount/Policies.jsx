import React from 'react';
import {Link} from 'react-router-dom';
import Layout from '../Layout';
import {toast} from 'react-toastify';
import parse from 'html-react-parser';
import {useSelector} from 'react-redux';

const Policies = (props) => {
    toast.configure();
    // const Policy = useSelector((state) => state.auth.isPolicy);
    const policyDetail = JSON.parse(localStorage.getItem('policyData'));
    const previousPage = useSelector((state) => state.auth.previousPage);
    return (
        <Layout>
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <Link to={previousPage} className="back_link icon_btn"><span className="icon arrow_left"></span> Back</Link>
                    </div>
                    {/* <div className="col-12">
                        <div className="d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap">
                            <div className="main_title">
                                <h2>{Policy ? 'Privacy Notice' : 'Terms and condition'}</h2>
                            </div>
                        </div>
                    </div> */}
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="policy_wrap mb-5">
                            {
                                policyDetail.length >0 && policyDetail.filter((value)=>value.contentId===props.id || value.contentId===props.id1).map((detail, index) => {
                                    return (
                                        <>
                                            <h5>{parse(detail.title ?? '')}</h5><div>{parse(detail.contentDescription ?? '')}</div>
                                        </>
                                    );
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};
export default Policies;
