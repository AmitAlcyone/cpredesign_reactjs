import Header from './layout/Header';
import Menu from './layout/Menu';
import Footer from './layout/Footer';
import React from 'react';
import {useSelector} from 'react-redux';
import FullScreenLoader from './../utils/loaders/FullScreenLoader';

const Layout = (props) => {
    const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
    const isLoading = useSelector((state) => state.global.isLoading);

    return (
        <React.Fragment>
            {isLoading ? <FullScreenLoader /> : ''}
            {isLoggedIn ? <Header /> : ''}
            {isLoggedIn ? <Menu /> : ''}
            <div className='main_wrap'>{props.children}</div>
            {isLoggedIn ? <Footer /> : ''}
        </React.Fragment>
    );
};

export default Layout;
