import React from 'react';
import {useDispatch} from 'react-redux';
import {setformOtherData} from './../../store/reducers/gift-card-reducer';

const OtherForm = (props) => {
    const dispatch = useDispatch();
    const otherFormJson= {
        FirstName: props.data.forOther.FirstName,
        LastName: props.data.forOther.LastName,
        RecipientEmail: props.data.forOther.RecipientEmail,
        Message: props.data.forOther.Message
    };

    return (
        <div className='for_other_form p-3 mb-3'>
            <form action='#'>
                <div className='row'>
                    <div className='col-12 col-sm-6 col-md-2'>
                        <div className='input_wrap'>
                            <label htmlFor='fName' className='form-label'>First Name <sup className='text-danger'>*</sup></label>
                            <input type='text' name='FirstName' value={otherFormJson.FirstName} onChange={(event) => {
                                otherFormJson.FirstName= event.target.value;
                                dispatch(setformOtherData({forOther: otherFormJson}));
                            }} className='form-control form-control-lg' placeholder='' />
                        </div>
                    </div>
                    <div className='col-12 col-sm-6 col-md-2'>
                        <div className='input_wrap'>
                            <label htmlFor='lName' className='form-label'>Last Name <sup className='text-danger'>*</sup></label>
                            <input type='text' name='LastName' value={otherFormJson.LastName} onChange={(event) => {
                                otherFormJson.LastName= event.target.value;
                                dispatch(setformOtherData({forOther: otherFormJson}));
                            }} className='form-control form-control-lg' placeholder=''/>
                        </div>
                    </div>
                    <div className='col-12 col-sm-6 col-md-4'>
                        <div className='input_wrap'>
                            <label htmlFor='repEmail' className='form-label'>Recipient Email <sup className='text-danger'>*</sup></label>
                            <input type='email' name='RecipientEmail' value={otherFormJson.RecipientEmail} onChange={(event) => {
                                otherFormJson.RecipientEmail= event.target.value;
                                dispatch(setformOtherData({forOther: otherFormJson}));
                            }} className='form-control form-control-lg' placeholder='' />
                        </div>
                    </div>
                    <div className='col-12 col-sm-6 col-md-4'>
                        <div className='input_wrap'>
                            <label htmlFor='message' className='form-label'>Your Message</label>
                            <input type='email' name='Message' value={otherFormJson.Nessage} onChange={(event) => {
                                otherFormJson.Message= event.target.value;
                                dispatch(setformOtherData({forOther: otherFormJson}));
                            }} className='form-control form-control-lg' placeholder='' />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default OtherForm;
