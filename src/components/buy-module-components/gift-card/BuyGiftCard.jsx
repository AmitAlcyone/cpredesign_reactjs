import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import LayoutBuy from '../../LayoutBuy';
import GCLogo from '../../../assets/images/logo_white.png';
import GiftCard from '../../giftCard/GiftCard';
// import OtherForm from '../../forms/OtherForm';
import {useSelector} from 'react-redux';
import {buyGiftCard} from '../../../services/buy-module-services/gift-card-service';
import {toast} from 'react-toastify';
import {useDispatch} from 'react-redux';
import {setGiftCardAmounts, setIsForMySelf} from '../../../store/reducers/gift-card-reducer';
import Checkout from '../checkout/checkout';
import renderSkeletonLoader from '../../../utils/skeletons/common-skeleton';
// import {checkoutJson} from '../../../services/auth-service';
import {setBarcodeString} from '../../../store/reducers/global-reducer';
import {giftCardList} from '../../../services/buy-module-services/gift-card-service';
import {getPCIDetailsLocal, getNoLoginCheckoutJSON, quickBuyGiftCardModel} from '../../../utils/common-repo';
import CheckoutSuccess from '../checkout/checkout-success';

const BuyGiftCard = (props) => {
    toast.configure();
    const toastId = React.useRef(null);
    const [cardAmount, setCardAmount] = useState('');
    const [selectedGCAmount, setSelectedGCAmount] = useState({});
    // const navigate = useNavigate();
    const dispatch = useDispatch();
    const giftCardAmounts = useSelector((state) => state.giftCard.giftCardAmounts);
    const orderJson= useState(getNoLoginCheckoutJSON());
    const [isCheckoutShow, setIsCheckoutShow] = useState(false);
    const [isGiftCardAmountLoading, setISGiftCardAmountLoading] = useState(false);
    const [isProcessCheckout, setIsProcessCheckout] = useState(false);
    const customerCode = localStorage.getItem('customerCode');
    const [viewCheckoutSuccess, setViewCheckoutSuccess] = useState(false);
    const buylocationId = localStorage.getItem('buyLocationId');

    const [defaultGCInputData, setDefaultGCInputData]= useState({});

    const onSelectGCAmount = async (value) => {
        setCardAmount('');
        const jsonData= quickBuyGiftCardModel(true, value.taxIncluded, value.price, value.productTypeId, value.description, value.taxRate);
        setSelectedGCAmount(jsonData);
    };

    const onCardAmountInputFocus = (e, defaultInputData) => {
        if (selectedGCAmount.IsSelected !=undefined && selectedGCAmount.IsSelected) {
            setCardAmount('');
            const jsonData= quickBuyGiftCardModel(false, defaultInputData.taxIncluded, '', defaultInputData.productTypeId, 'Ohter', defaultGCInputData.taxRate);
            setSelectedGCAmount(jsonData);
        };
    };

    const onCardAmountInputChange = (e, defaultInputData) => {
        if (e.target.value.length >= 0 && e.target.value.length < 6) {
            const jsonData = quickBuyGiftCardModel(false, defaultInputData.taxIncluded, e.target.value, defaultInputData.productTypeId, 'Other', defaultGCInputData.taxRate);
            setSelectedGCAmount(jsonData);
            setCardAmount(e.target.value);
        }
    };

    const continueToAddGCCheckout = async () => {
        debugger;
        orderJson[0].TermsId = 4;
        if (selectedGCAmount.ProductTypeId == undefined) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please select gift card first.');
            }
            return false;
        }
        if (selectedGCAmount.Description == 'Other' && selectedGCAmount.Amount < 1) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please enter gift card amount. Amount should be greater than 0.');
            }
            return false;
        }
        orderJson[0].TopHeaderText= 'Buy a gift card';
        orderJson[0].ListItemText= 'Gift Card';
        orderJson[0].ListItemDescription= selectedGCAmount.Description;
        orderJson[0].TaxAmount= 0;
        if (selectedGCAmount.IsTaxable && selectedGCAmount.TaxAmount!=0) {
            orderJson[0].TaxAmount= Math.round((double)(selectedGCAmount.Amount / (1 + (selectedGCAmount.TaxAmount / 100))));
        }
        orderJson[0].SubTotal= selectedGCAmount.Amount;
        orderJson[0].GrandTotal= (parseFloat(orderJson[0].SubTotal) + parseFloat(orderJson[0].TaxAmount)).toFixed(2);

        orderJson[0].IsShowRecieveEmailReciept= false;
        orderJson[0].IsShowRecieveSMSReciept= false;
        setIsCheckoutShow(true);
    };

    const giftCardCheckout = async (order, ccDetails) => {
        if (order.IsCheckedTermAndCondition) {
            const postModel= {};
            postModel.productTypeId= selectedGCAmount.ProductTypeId;
            postModel.phoneNumber= ccDetails.phoneNumber;
            // eslint-disable-next-line camelcase
            postModel.temporary_token= ccDetails.temporary_token;
            postModel.unixTime= ccDetails.unixTimeStamp;
            postModel.amount= selectedGCAmount.Description == 'Other' ? selectedGCAmount.Amount : 0;
            postModel.CreditCardNumber= '';
            postModel.CardHolderName= '';
            postModel.ExpiryDate= '';
            postModel.Alias= '';
            postModel.LocationId = buylocationId;

            setIsProcessCheckout(true);
            const response = await buyGiftCard(customerCode, '', postModel);
            setIsProcessCheckout(false);
            if (response.status === 200) {
                dispatch(setBarcodeString({barcodeString: response.data[0].sGiftCard_Number}));
                setViewCheckoutSuccess(true);
                setIsCheckoutShow(false);
                // return navigate('/order-success');
            } else {
                toast.error('Something went wrong.', {autoClose: 3000});
                return false;
            }
        } else if (!order.IsCheckedTermAndCondition) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please check term and condition.');
            }
            return false;
        } else if (!order.IsCardSelected) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please select credit card before checkout.');
            }
            return false;
        }
    };

    const PCIScript= getPCIDetailsLocal().PCIScript;

    useEffect(() => {
        const getGiftCardAmountList = async () => {
            setISGiftCardAmountLoading(true);
            const response = await giftCardList(customerCode);
            setISGiftCardAmountLoading(false);
            if (response.status === 200) {
                dispatch(setGiftCardAmounts({giftCardAmounts: response.data.products}));
                setDefaultGCInputData(response.data.products.find((d) => d.price == 9999));
            } else {
                dispatch(setGiftCardAmounts({giftCardAmounts: []}));
                toast.error('Something went wrong.', {autoClose: 3000});
            }
        };

        if (giftCardAmounts == null || giftCardAmounts.length == 0) {
            getGiftCardAmountList();
        }

        const script = document.createElement('script');
        script.src = PCIScript;
        script.async = false;
        document.body.appendChild(script);
        return () => {
            document.body.removeChild(script);
        };
    }, []);

    return (
        <LayoutBuy>
            {viewCheckoutSuccess ? <CheckoutSuccess Annomous={true} /> : isCheckoutShow ? <Checkout processCheckout={(o, cc) => giftCardCheckout(o, cc)} hideCheckoutPage={() => {
                document.body.scrollTop = 0; // For Safari
                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                setIsCheckoutShow(false);
            }} order={orderJson} /> :
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <div className='d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap'>
                                <div className='inner_title d-flex align-items-start'>
                                    <div className='progress-circle over50 p50'>
                                        <span>1 of 2</span>
                                        <div className='left-half-clipper'>
                                            <div className='first50-bar'></div>
                                            <div className='value-bar'></div>
                                        </div>
                                    </div>
                                    <div className='main_title'>
                                        <h2>Add Gift Card</h2>
                                        <p>Select an amount</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-12'>
                            <ul className='nav custom_tabs mb-3' id='pills-tab' role='tablist'>
                                <li className='nav-item' role='presentation'>
                                    <button className={`nav-link active`} id='pills-myself-tab' data-bs-toggle='pill' data-bs-target='#pills-myself' type='button' role='tab' aria-controls='pills-myself' onClick={() => {
                                        dispatch(setIsForMySelf({isForMySelf: true}));
                                    }} aria-selected='true'>For Myself</button>
                                </li>
                            </ul>
                            <div className='tab-content' id='pills-tabContent'>
                                <div className={`tab-pane fade show active`} id='pills-myself' role='tabpanel' aria-labelledby='pills-myself-tab'></div>
                                <div className='row'>
                                    <div className='col-lg-8'>
                                        <div className='add_amount_wrap'>
                                            <h5>Select an Amount</h5>
                                            <div className='add_amount'>
                                                {isGiftCardAmountLoading ?
                                                    <><div className='amount_btn_grp'>
                                                        {renderSkeletonLoader(5, 'skeletonAddGiftCardAmounts')}
                                                        {/* {giftCardAmounts.map((value, i) => {
                                                            return <button className='' type='button' key={i} onClick={() => onSelectGCAmount(value)}>+{value.price}</button>;
                                                        })} */}
                                                    </div></> :
                                                    <>
                                                        {
                                                            giftCardAmounts == null && giftCardAmounts.length == 0 ? <h3>No gift card found.</h3> :
                                                                defaultGCInputData != null ? <div className='other_amount_input input-group mb-3'>
                                                                    <span className='input-group-text'>$</span>
                                                                    <input pattern="[0-9]*" value={cardAmount} onClick={(event) => onCardAmountInputFocus(event, defaultGCInputData)} onChange={(event) => onCardAmountInputChange(event, defaultGCInputData)} maxLength='5' min="0" max="10000" type='number' className='form-control form-control-lg' aria-label='Amount (to the nearest dollar)' placeholder='Type amount here' />
                                                                </div> : ''
                                                        }
                                                        <div className='amount_btn_grp'>
                                                            {giftCardAmounts.map((value, i) => {
                                                                return defaultGCInputData != null && defaultGCInputData.productTypeId == value.productTypeId ? '' : <button className={`${selectedGCAmount.ProductTypeId == value.productTypeId ? 'active' : ''}`} type='button' key={i} onClick={() => onSelectGCAmount(value)}>+{value.price}</button>;
                                                            })}
                                                        </div>
                                                    </> }
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-lg-4'>
                                        <div className='order_summ_wrap'>
                                            <h5>Gift Card Preview</h5>
                                            <GiftCard amount={selectedGCAmount.Amount} brandLogo={GCLogo}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-12'>
                            <div className='d-flex justify-content-end mt-3 pt-3 border-top'>
                                <div className='btn_grps d-flex flex-wrap'>
                                    <Link to='/buy' className='back_btn icon_btn me-2'><span className='icon arrow_left'></span> Back</Link>
                                    <span onClick={() => continueToAddGCCheckout()} className='main_btn icon_btn' {...(isProcessCheckout ? {disabled: 'disabled'} : {})} >{isProcessCheckout && <span className="spinner-border text-light btnLoader"></span>}Continue <span className='icon arrow_right_white'></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </LayoutBuy>
    );
};

export default BuyGiftCard;
