import React, {useEffect} from 'react';
import {Link} from 'react-router-dom';
// import Layout from '../Layout';
import QRCode from 'react-qr-code';
import Barcode from 'react-barcode';
import {useSelector} from 'react-redux';

const CheckoutSuccess = (props) => {
    const barcodeString = useSelector((state) => state.global.barcodeString);
    const barcodeStr = barcodeString !== undefined ? barcodeString : 'DemoBarcode';
    useEffect(() => {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    });
    return (
        <div className='container'>
            <div className='row payment_method_wrap'>
                <div className='col-lg-7 mx-auto'>
                    <div className='barcode_img mx-auto text-center'>
                        <Barcode value={barcodeStr} width="2" height="60"/>
                    </div>
                    <div className='barcode_img mx-auto text-center mt-4'>
                        <QRCode value={barcodeStr} size="200" />
                    </div>
                    <div className='barcode_number text-center p-4'>
                        <small>Your Redeem Code:</small>
                        <h3 className='mt-2'>{barcodeStr}</h3>
                    </div>
                    <div className='video_promo d-flex justify-content-center mb-4'>
                        <iframe width='560' height='315' src='https://player.vimeo.com/video/435870912?title=0&portrait=0&byline=0&autoplay=1' title='YouTube video player' frameBorder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowFullScreen></iframe>
                    </div>
                    {props.Annomous ?
                        <div className='btn_grps d-flex flex-wrap justify-content-center'>
                            <a className='main_btn' onClick={() => window.location.reload()}>Back to Home</a>
                        </div> :
                        <div className='btn_grps d-flex flex-wrap justify-content-center'>
                            <Link className='main_btn' to='/'>Back to Home</Link>
                        </div> }
                </div>
            </div>
        </div>
    );
};

export default CheckoutSuccess;
