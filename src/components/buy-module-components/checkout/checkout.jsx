import React, {useState, useEffect} from 'react';
// import {useSelector} from 'react-redux';
import {Link} from 'react-router-dom';
import TermsModal from '../../modals/TermsModal';
import FullScreenLoader from '../../../utils/loaders/FullScreenLoader';
import PaymentCard from '../../order/PCI/payment-card';
import {toast} from 'react-toastify';

export default function Checkout(props) {
    const [modal, setModal] = useState(false);
    toast.configure();
    // const dispatch = useDispatch();
    // const [loading, setLoading] = useState(false);
    // const [viewCardForm, setViewCardForm] = useState(false);
    // eslint-disable-next-line no-unused-vars
    const [newAddedCard, setNewAddedCard] = useState(-1);
    const [order, setOrder]=useState(props.order[0]);
    const [Loader, setLoader] = useState(false);

    const applyPromotionCode = async () => {
        // setapply(true);
        order.IsPromocodeApplied = true;
        setOrder(order);
        const response = await getPromotionAvailable(userInfo.accessToken);
        if (response.status === 200) {
            if (response.data[0].discountAmount > 0) {
                order.DiscountAmount= response.data[0].discountAmount;
                order.GrandTotal= order.SubTotal - order.DiscountAmount;
                setOrder(order);
            } else {
                toast.warning('Discount not available on this code.', {autoClose: 3000});
            }
        } else {
            toast.error('Something went wrong.', {autoClose: 3000});
        }
    };

    const handleChangeTerm = (e) => {
        // setTermAndCondition(e.target.checked);
        order.IsCheckedTermAndCondition= e.target.checked;
        setOrder(order);
    };
    const setPromocode = (code) => {
        order.Promocode= code;
        setOrder(order);
    };

    const callbackOnPreviousPage = () => {
        props.hideCheckoutPage();
    };

    const processCheckout = async (ccDetails) => {
        setLoader(true);
        const isLoading= await props.processCheckout(order, ccDetails);
        setLoader(isLoading);
    };
    useEffect(() => {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }, []);
    return (
        <div>
            {modal ? <TermsModal id={order.termsId} closeModal={() => setModal(false)} /> : ''}
            {Loader === true ? <FullScreenLoader /> : ''}
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap border-bottom">
                            <div className="inner_title d-flex align-items-start">
                                <div className="progress-circle over50 p100">
                                    <span>2 of 2</span>
                                    <div className="left-half-clipper">
                                        <div className="first50-bar"></div>
                                        <div className="value-bar"></div>
                                    </div>
                                </div>
                                <div className="main_title">
                                    <h2>{order.TopHeaderText}</h2>
                                    <p>Order Summary</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="row payment_method_wrap">
                    <div className="col-lg-8">
                        <div className="pay_method_list">
                            <h5>Select a payment method</h5>
                            <div className="new_card_add">
                                {/* <button className="btn btn-outline-secondary w-100" type="button" data-bs-toggle="collapse" data-bs-target="#add_new_card_from" aria-expanded="false" aria-controls="add_new_card_from">+ add a new card</button> */}
                                <div className="add_cc_form" id="add_new_card_from">
                                    <PaymentCard checkout={(data) => processCheckout(data)} loader={(val) => setLoader(val)} noLogged={true} setCard={(id) => setNewAddedCard(id)} hidePop={() => setViewCardForm(false)} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="order_summ_wrap">
                            <h5>Order Summary</h5>
                            <div className="summary_card">
                                <ul className="list_items os_user_plan" style={{minHeight: 'auto'}}>
                                    {/* <li>
                                        <span>Customer</span> <b>Glenn</b>
                                    </li> */}
                                    <li>
                                        <span>{order.ListItemText}:</span> <b>{order.ListItemDescription}</b>
                                    </li>
                                </ul>

                                {order.IsShowPromocodeDiv ?
                                    <div className='promo_form'>
                                        <div className='form-group d-flex mt-3 mb-3'>
                                            <input type='text' className='form-control promo_code me-2 form-control-lg  id="myInput' placeholder='Enter Promotion Code' value={order.Promocode} onChange={(event) => setPromocode(event.target.value)} />
                                            <button type='button' className='main_btn' onClick={() => applyPromotionCode()}>Apply</button>
                                        </div>
                                        {order.Promocode === '' && order.IsPromocodeApplied ? <p className='text-danger'>Please Enter Promotion Code</p>:''}
                                    </div> : ''}

                                <ul className="list_items order_details">
                                    <li>
                                        <span>Subtotal:</span> <span>${parseFloat(order.SubTotal).toFixed(2)}</span>
                                    </li>
                                    <li>
                                        <span>Discount:</span> <span>${parseFloat(order.DiscountAmount).toFixed(2)}</span>
                                    </li>
                                    <li>
                                        <span>Tax:</span> <span>${parseFloat(order.TaxAmount).toFixed(2)}</span>
                                    </li>
                                    <li>
                                        <b>Total:</b> <b>${order.GrandTotal}</b>
                                    </li>
                                </ul>
                            </div>
                            <div className='condi_checks pt-4'>
                                {order.IsShowRecieveEmailReciept ?
                                    <div className='form-check'>
                                        <input className='form-check-input' type='checkbox' id='rEmail' onChange={(event) => {
                                            order.IsRecieveEmailReciept=event.target.checked; setOrder(order);
                                        }} />
                                        <label className='form-check-label' htmlFor='rEmail'>
                                            Receive email receipt
                                        </label>
                                    </div> : '' }
                                {order.IsShowRecieveSMSReciept ?
                                    <div className='form-check'>
                                        <input className='form-check-input' type='checkbox' id='rSMS' onChange={(event) => {
                                            order.IsRecieveSMSReciept=event.target.checked; setOrder(order);
                                        }} />
                                        <label className='form-check-label' htmlFor='rSMS'>
                                            Receive SMS receipt
                                        </label>
                                    </div> : '' }
                                <div className='form-check'>
                                    <input className='form-check-input' type='checkbox' id='termcondi' {...(order.IsCheckedTermAndCondition ? 'checked' : '')} onChange={(event) => handleChangeTerm(event)}/>
                                    <label className='form-check-label' htmlFor='termcondi'>
                                            I Agree to the <Link to='#' className='termandcondition_link' onClick={() => setModal(true)}> Terms and Conditions* </Link>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className='d-flex justify-content-end mt-3 pt-3 border-top'>
                            <div className='btn_grps d-flex flex-wrap align-items-center'>
                                <button className='back_btn icon_btn me-2' onClick={() => callbackOnPreviousPage()}><span className='icon arrow_left'></span> Back</button>
                                <div id="credit-card-submit"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
