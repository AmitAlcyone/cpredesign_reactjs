import React from 'react';

// Props properties
// modalClass: 'warning_pop',
// title: '',
// message: '',
// btnSuccessText: 'Yes',
// btnCancelText: 'No',
const AlertModalModule = (props) => {
    const closeModal = () =>{
        props.closeModal();
    };
    const targetedEvent = () =>{
        props.targetEvent();
    };
    return <>
        {/* //error_pop */}
        <div className={`modal fade custom_pop ${props.data.modalClass ? props.data.modalClass : '' } show`} id='modal_pop' data-bs-backdrop='static' data-bs-keyboard='false' tabIndex='-1' aria-labelledby='errorModalLabel' aria-hidden='true' style={{display: 'block'}}>
            <div className='modal-dialog  modal-dialog-centered'>
                <div className='modal-content'>
                    <div className='modal-body'>
                        <h2>{props.data.title}</h2>
                        <p>{props.data.message}</p>
                    </div>
                    <div className='modal-footer justify-content-center'>
                        <button className={props.data.btnPrimary ? 'main_btn_ulimited' : 'sec_btn'} onClick={() => targetedEvent()} id="btnAlertYes"> <span id="spanLoader" className="spinner-border text-light btnLoader" style={{display: 'none'}} /> {props.data.btnSuccessText}</button>
                        <button className={!props.data.btnPrimary ? 'main_btn_ulimited' : 'sec_btn'} onClick={() => closeModal()} data-bs-dismiss='modal'>{props.data.btnCancelText}</button>
                    </div>
                </div>
            </div>
        </div>
        <div className="modal-backdrop fade show"></div>
    </>;
};

export default AlertModalModule;
