import React from 'react';
import parse from 'html-react-parser';

export default function TermsModal(props) {
    const policyDetail = JSON.parse(localStorage.getItem('policyData'));
    const closeModal = () =>{
        props.closeModal();
    };

    return <>
        <div className="modal fade show terms_modal" tabIndex="-1" style={{display: 'block'}}>
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        {/* <h5 className="modal-title">Privacy Notice</h5> */}
                        <button type="button" className="btn-close" onClick={closeModal}></button>
                    </div>
                    <div className="modal-body">
                        <div className="row">
                            <div className="col-12">
                                <div className="policy_wrap mb-5">
                                    {
                                        policyDetail.length >0 && policyDetail.filter((value)=>value.contentId===props.id || value.contentId===props.id1).map((detail, index) => {
                                            return (
                                                <>
                                                    <h5>{parse(detail.title ?? '')}</h5><div>{parse(detail.contentDescription ?? '')}</div>
                                                </>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="modal-backdrop fade show"></div>
    </>;
}
