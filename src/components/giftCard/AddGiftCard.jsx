import React, {useState, useEffect} from 'react';
import {Link, useNavigate, useParams} from 'react-router-dom';
import Layout from './../../components/Layout';
import GCLogo from '../../assets/images/logo_white.png';
import GiftCard from './GiftCard';
import OtherForm from '../forms/OtherForm';
import {useSelector} from 'react-redux';
import {giftCardAmountList, getCalculatedGCTax, addValueOnExistingGC, buyGiftCardOnline} from '../../services/gift-card-service';
import {toast} from 'react-toastify';
import {useDispatch} from 'react-redux';
import {setGiftCardAmounts, setIsForMySelf} from './../../store/reducers/gift-card-reducer';
import OrderCheckout from '../order/order-checkout';
import renderSkeletonLoader from '../../utils/skeletons/common-skeleton';
import {checkoutJson} from '../../services/auth-service';
import {setBarcodeString} from './../../store/reducers/global-reducer';
import {giftCardModel, isNumeric} from '../../utils/common-repo';

const AddGiftCard = (props) => {
    toast.configure();
    const navigate = useNavigate();
    const toastId = React.useRef(null);
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const dispatch = useDispatch();
    const urlParam = useParams();
    const requestPageType= urlParam.type == undefined ? '' : urlParam.type;
    const giftCardGuid= urlParam.gcGuid == undefined ? '' : urlParam.gcGuid;
    // const [query, setQuery] = useState('');
    // eslint-disable-next-line no-unused-vars
    const accessToken = useSelector((state) => state.auth.userInfo.accessToken);
    const userInfo = useSelector((state) => state.auth.userInfo);
    const giftCardAmounts = useSelector((state) => state.giftCard.giftCardAmounts);
    const giftCardTabs = useSelector((state) => state.giftCard.giftCardTabs);
    const preferedHomeLocation = useSelector((state) => state.auth.locations[0].locationId);
    const customerInfo = JSON.parse(localStorage.getItem('customerDetail')).customer;
    const [cardAmount, setCardAmount] = useState('');
    const orderJson= useState(checkoutJson());
    const [isCheckoutShow, setIsCheckoutShow] = useState(false);
    // const [isLoading, setIsLoading] = useState(giftCards.length ? false : true);
    const [isGiftCardAmountLoading, setISGiftCardAmountLoading] = useState(false);
    const [defaultGCInputData, setDefaultGCInputData]= useState(giftCardAmounts !=null && giftCardAmounts.length > 0 ? giftCardAmounts.find((d) => d.dblAmount == 9999) : {});
    const [selectedGCAmount, setSelectedGCAmount] = useState({});
    const [isProcessCheckout, setIsProcessCheckout] = useState(false);


    const onSelectGCAmount = async (value) => {
        // setCardAmount('00.00');
        // const jsonData = {};
        // jsonData.IsSelected = true;
        // jsonData.Other = '00.00';
        // jsonData.IsTaxable= (value.bTaxable && value.lTaxID != null);
        // jsonData.Amount =parseFloat(value.dblAmount).toFixed(2);
        // jsonData.SurchargeId = value.surcharge_ID;
        // jsonData.CustomerGuid = userInfo.customerGuid;
        // jsonData.Description = value.sDescription;
        const jsonData= giftCardModel(true, (value.bTaxable && value.lTaxID != null), value.dblAmount, value.surcharge_ID, value.sDescription, '', userInfo.customerGuid);
        setSelectedGCAmount(jsonData);
    };

    const onCardAmountInputFocus = (e, defaultInputData) => {
        if (selectedGCAmount.IsSelected !=undefined && selectedGCAmount.IsSelected) {
            // const jsonData = {};
            setCardAmount('');
            // jsonData.Other = '00.00';
            // jsonData.IsTaxable= true;
            // jsonData.Amount = '00.00';
            // jsonData.IsSelected = false;
            // jsonData.SurchargeId = 1;
            // jsonData.CustomerGuid = userInfo.customerGuid;
            // jsonData.Description= '';

            const jsonData= giftCardModel(false, (defaultInputData.bTaxable && defaultInputData.lTaxID != null), '00.00', defaultInputData.surcharge_ID, 'Other', '00.00', userInfo.customerGuid);
            setSelectedGCAmount(jsonData);
        };
    };

    const onCardAmountInputChange = (e, defaultInputData) => {
        // const jsonData = {};
        // jsonData.IsSelected = false;
        // jsonData.IsTaxable= true;
        // jsonData.Other = parseFloat(e.target.value).toFixed(2);
        // jsonData.Amount = parseFloat(e.target.value).toFixed(2);
        // jsonData.SurchargeId = 1;
        // jsonData.CustomerGuid = userInfo.customerGuid;
        // jsonData.Description= 'Other';
        if (e.target.value.length >= 0 && e.target.value.length < 6) {
            const jsonData = giftCardModel(false, (defaultInputData.bTaxable && defaultInputData.lTaxID != null), e.target.value, defaultInputData.surcharge_ID, 'Other', e.target.value, userInfo.customerGuid);
            setSelectedGCAmount(jsonData);
            setCardAmount(e.target.value);
        }
    };

    const continueToAddGCCheckout = async () => {
        orderJson[0].termsId= 4;
        if (selectedGCAmount.SurchargeId == undefined) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please select gift card first.');
            }
            return false;
        }
        // if (!selectedGCAmount.IsSelected && parseFloat(selectedGCAmount.Other) < 1) {
        //    if (!toast.isActive(toastId.current)) {
        //        toastId.current = toast.error('Please enter gift card amount. Amount should be greater than 0.');
        //    }
        //    return false;
        // }
        if (selectedGCAmount.Amount < 1) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please enter gift card amount. Amount should be greater than 0.');
            }
            return false;
        }

        orderJson[0].TopHeaderText= 'Buy a gift card';
        orderJson[0].SubTotal= selectedGCAmount.Amount;
        orderJson[0].ListItemText= 'Gift Card';
        orderJson[0].ListItemDescription= selectedGCAmount.Description;
        if (!giftCardTabs.isForMySelf) {
            if (giftCardTabs.forOther.FirstName.length == 0) {
                if (!toast.isActive(toastId.current)) {
                    toastId.current = toast.error('First name is mandatory.');
                }
                return false;
            } else if (giftCardTabs.forOther.LastName.length == 0) {
                if (!toast.isActive(toastId.current)) {
                    toastId.current = toast.error('Last name is mandatory.');
                }
                return false;
            } else if (giftCardTabs.forOther.RecipientEmail.length == 0) {
                if (!toast.isActive(toastId.current)) {
                    toastId.current = toast.error('Recipient email is mandatory.');
                }
                return false;
            } else if (!emailRegex.test(giftCardTabs.forOther.RecipientEmail)) {
                if (!toast.isActive(toastId.current)) {
                    toastId.current = toast.error('Recipient email should be a valid email.');
                }
                return false;
            }
        }
        orderJson[0].Email= '';
        if (selectedGCAmount.IsTaxable) {
            // setIsLoading(true);
            const postData = {};
            postData.Other= isNumeric(selectedGCAmount.Other) ? parseFloat(selectedGCAmount.Other) : 0;
            postData.SurchargeId= selectedGCAmount.SurchargeId;
            postData.CustomerGuid= selectedGCAmount.CustomerGuid;
            setIsProcessCheckout(true);
            const response = await getCalculatedGCTax(accessToken, postData);
            setIsProcessCheckout(false);
            if (response.status === 200) {
                orderJson[0].TaxAmount= response.data;
                orderJson[0].GrandTotal= (parseFloat(orderJson[0].SubTotal) + parseFloat(response.data)).toFixed(2);
                setIsCheckoutShow(true);
            } else {
                // dispatch(setIsLoading(false));
                toast.error('Something went wrong.', {autoClose: 3000});
            }
        } else {
            orderJson[0].TaxAmount= 0;
            orderJson[0].GrandTotal= orderJson[0].SubTotal;
            setIsCheckoutShow(true);
        }
    };

    // const hideCheckoutPage = () =>{
    //     setIsCheckoutShow(false);
    // };

    const giftCardCheckout = async (order) => {
        if (order.IsCheckedTermAndCondition && order.IsCardSelected) {
            const postModel= {};
            if (requestPageType== 'add_value') {
                postModel.responseCodeID= '';
                postModel.authID= '';
                postModel.dblBalance= selectedGCAmount.IsSelected ? selectedGCAmount.Amount : selectedGCAmount.Other;
                postModel.giftCardGUID= giftCardGuid;
                postModel.customerGUID= userInfo.customerGuid;
                postModel.encryptednumber= '';
                postModel.paymenttype= '';
                postModel.Token= order.SelectedCard.sPayerIdentifier;
                postModel.creditCardNumber= '';
                postModel.cardHolderName= '';
                postModel.isExisting= true;
                postModel.expiryDate= '';
            } else {
                postModel.surchargeId = selectedGCAmount.SurchargeId;
                postModel.other = selectedGCAmount.IsSelected ? 0 : selectedGCAmount.Other;
                postModel.firstName = giftCardTabs.forOther.FirstName;
                postModel.email = order.Email !== undefined ? order.Email : customerInfo.customer.sEmail;
                postModel.recipientEmail = !giftCardTabs.isForMySelf ? giftCardTabs.forOther.RecipientEmail : '';
                postModel.message = giftCardTabs.forOther.Message;
                postModel.paymentCardId = order.SelectedCard.id;
                postModel.isSendEmail = order.IsRecieveEmailReciept;
                postModel.bSendasGift = !giftCardTabs.isForMySelf;
                postModel.formyself = giftCardTabs.isForMySelf;
                postModel.discountAmount = 0;
                postModel.token = order.SelectedCard.sPayerIdentifier;
                postModel.isSendSms = order.IsRecieveSMSReciept;
                postModel.discountTaxAmount = 0;
                postModel.discountId = 0;
                postModel.locationId = userInfo.preferedHomeLocation;
                postModel.customerIdentityId = userInfo.customerIdentityID;
                postModel.customerGuid = userInfo.customerGuid;
                // postModel.dblBalance= order.GrandTotal;
                // postModel.Token= order.SelectedCard.sPayerIdentifier;
                // // postModel.Token = 'gHIBrcksvL';
                // postModel.CreditCardNumber= order.SelectedCard.id;
                // postModel.CardHolderName= '';
                // postModel.ExpiryDate= String(order.SelectedCard.sExpiredMonth) + String(order.SelectedCard.sExpireYear);
                // postModel.CustomerGuid= userInfo.customerGuid;
                // postModel.GiftCardTypeId= null;
                // postModel.isExisting= true;
                // postModel.cardType= '';
                // postModel.sCompany= '';
                // postModel.isMobile= true;
                // postModel.sEmail= userInfo.customerEmail;
                // // postModel.approval_code= ;
                // postModel.bSendasGift= !giftCardTabs.isForMySelf;
                // postModel.sSendAsGiftCardEmail= giftCardTabs.forOther.RecipientEmail != null ? giftCardTabs.forOther.RecipientEmail : '';
                // postModel.Formyself= giftCardTabs.isForMySelf;
                // postModel.FirstName= giftCardTabs.forOther.FirstName;
                // postModel.LastName= giftCardTabs.forOther.LastName;
                // postModel.RecipientEmail= !giftCardTabs.isForMySelf ? giftCardTabs.forOther.RecipientEmail : '';
                // postModel.Message= giftCardTabs.forOther.Message;
                // postModel.CustomerIdentityId= userInfo.customerIdentityID;
                // postModel.IsSendMail= order.IsRecieveEmailReciept;
                // postModel.IsSendSMS= order.IsRecieveSMSReciept;
            }

            setISGiftCardAmountLoading(true);
            let response= null;
            if (requestPageType== 'add_value') {
                response= await addValueOnExistingGC(accessToken, preferedHomeLocation, postModel);
            } else {
                response= await buyGiftCardOnline(accessToken, postModel);
            }
            // const response = await buyGiftCardOnline(accessToken, postModel);
            setISGiftCardAmountLoading(false);
            if (response.status== 200) {
                if (requestPageType== 'add_value') {
                    return navigate('/gift_card');
                } else {
                    dispatch(setBarcodeString({barcodeString: response.data.sGiftCard_Number}));
                    return navigate('/order-success');
                }
            } else {
                toast.error('Something went wrong.', {autoClose: 3000});
                return false;
            }
        } else if (!order.IsCheckedTermAndCondition) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please check term and condition.');
            }
            return false;
        } else if (!order.IsCardSelected) {
            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.error('Please select credit card.');
            }
            return false;
        }
    };

    useEffect(() => {
        const getGiftCardAmountList = async () => {
            setISGiftCardAmountLoading(true);
            const response = await giftCardAmountList(accessToken);
            setISGiftCardAmountLoading(false);
            if (response.status === 200) {
                setDefaultGCInputData(response.data.find((d) => d.dblAmount == 9999));
                dispatch(setGiftCardAmounts({giftCardAmounts: response.data}));
            } else {
                dispatch(setGiftCardAmounts({giftCardAmounts: []}));
                toast.error('Something went wrong.', {autoClose: 3000});
            }
        };
        if (giftCardAmounts==null || giftCardAmounts.length == 0) {
            getGiftCardAmountList();
        }
    }, []);

    return (
        <Layout>
            {isCheckoutShow ? <OrderCheckout processCheckout={(data) => giftCardCheckout(data)} hideCheckoutPage={() => {
                document.body.scrollTop = 0; // For Safari
                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                setIsCheckoutShow(false);
            }} order={orderJson} /> :
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <div className='d-flex mb-4 mt-3 justify-content-between align-items-center flex-wrap'>
                                <div className='inner_title d-flex align-items-start'>
                                    <div className='progress-circle over50 p50'>
                                        <span>1 of 2</span>
                                        <div className='left-half-clipper'>
                                            <div className='first50-bar'></div>
                                            <div className='value-bar'></div>
                                        </div>
                                    </div>
                                    <div className='main_title'>
                                        <h2>Add Gift Card</h2>
                                        <p>Select an amount</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-12'>
                            <ul className='nav custom_tabs mb-3' id='pills-tab' role='tablist'>
                                <li className='nav-item' role='presentation'>
                                    <button className={`nav-link ${giftCardTabs.isForMySelf ? 'active' : {}}`} id='pills-myself-tab' data-bs-toggle='pill' data-bs-target='#pills-myself' type='button' role='tab' aria-controls='pills-myself' onClick={() => {
                                        dispatch(setIsForMySelf({isForMySelf: true}));
                                    }} aria-selected='true'>For Myself</button>
                                </li>
                                <li className='nav-item' role='presentation'>
                                    <button className={`nav-link ${giftCardTabs.isForMySelf ? {} : 'active'}`} id='pills-others-tab' data-bs-toggle='pill' data-bs-target='#pills-others' type='button' role='tab' aria-controls='pills-others' onClick={() => {
                                        dispatch(setIsForMySelf({isForMySelf: false}));
                                    }} aria-selected='false'>For Others</button>
                                </li>
                            </ul>
                            <div className='tab-content' id='pills-tabContent'>
                                <div className={`tab-pane fade ${giftCardTabs.isForMySelf ? 'show active' : {}}`} id='pills-myself' role='tabpanel' aria-labelledby='pills-myself-tab'>
                                    <div className='row'>
                                        <div className='col-lg-8'>
                                            <div className='add_amount_wrap'>
                                                <h5>Select an Amount</h5>
                                                <div className='add_amount'>
                                                    {isGiftCardAmountLoading ?
                                                        <div className='amount_btn_grp'>
                                                            { renderSkeletonLoader(5, 'skeletonAddGiftCardAmounts') }
                                                        </div> :
                                                        giftCardAmounts == null && giftCardAmounts.length == 0 ? <h3>No gift card found.</h3> :
                                                            defaultGCInputData != null ? <div className='other_amount_input input-group mb-3'>
                                                                <span className='input-group-text'>$</span>
                                                                <input pattern="[0-9]*" value={cardAmount} onClick={(event) => onCardAmountInputFocus(event, defaultGCInputData)} onChange={(event) => onCardAmountInputChange(event, defaultGCInputData)} maxLength='5' min="0" max="10000" type='number' className='form-control form-control-lg' aria-label='Amount (to the nearest dollar)' placeholder='Type amount here' />
                                                            </div> : '' }
                                                    <div className='amount_btn_grp'>
                                                        {giftCardAmounts.map((value, i) => {
                                                            return defaultGCInputData != null && defaultGCInputData.surcharge_ID == value.surcharge_ID ? '' : <button className={`${selectedGCAmount.SurchargeId == value.surcharge_ID ? 'active' : ''}`} type='button' key={i} onClick={() => onSelectGCAmount(value)}>+{value.dblAmount}</button>;
                                                        })}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='col-lg-4'>
                                            <div className='order_summ_wrap'>
                                                <h5>Gift Card Preview</h5>
                                                <GiftCard amount={selectedGCAmount.Amount} brandLogo={GCLogo}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={`tab-pane fade ${giftCardTabs.isForMySelf ? {} : 'show active'}`} id='pills-others' role='tabpanel' aria-labelledby='pills-others-tab'>
                                    <OtherForm data={giftCardTabs} />
                                    <div className='row'>
                                        <div className='col-lg-8'>
                                            <div className='add_amount_wrap'>
                                                <h5>Select an Amount</h5>
                                                <div className='add_amount'>
                                                    {isGiftCardAmountLoading ?
                                                        <div className='amount_btn_grp'>
                                                            {renderSkeletonLoader(5, 'skeletonAddGiftCardAmounts')}
                                                        </div> :
                                                        giftCardAmounts == null && giftCardAmounts.length == 0 ? <h3>No gift card found.</h3> :
                                                            defaultGCInputData != null ? <div className='other_amount_input input-group mb-3'>
                                                                <span className='input-group-text'>$</span>
                                                                <input pattern="[0-9]*" value={cardAmount} onClick={(event) => onCardAmountInputFocus(event, defaultGCInputData)} onChange={(event) => onCardAmountInputChange(event, defaultGCInputData)} maxLength='5' min="0" max="10000" type='number' className='form-control form-control-lg' aria-label='Amount (to the nearest dollar)' placeholder='Type amount here' />
                                                            </div> : '' }
                                                    <div className='amount_btn_grp'>
                                                        {giftCardAmounts.map((value, i) => {
                                                            return defaultGCInputData != null && defaultGCInputData.surcharge_ID == value.surcharge_ID ? '' : <button className={`${selectedGCAmount.SurchargeId == value.surcharge_ID ? 'active' : ''}`} type='button' key={i} onClick={() => onSelectGCAmount(value)}>+{value.dblAmount}</button>;
                                                        })}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='col-lg-4'>
                                            <div className='order_summ_wrap'>
                                                <h5>Gift Card Preview</h5>
                                                <GiftCard amount={selectedGCAmount.Amount} brandLogo={GCLogo}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-12'>
                            <div className='d-flex justify-content-end mt-3 pt-3 border-top'>
                                <div className='btn_grps d-flex flex-wrap'>
                                    <Link to='/gift_card' className='back_btn icon_btn me-2'><span className='icon arrow_left'></span> Back</Link>
                                    <span onClick={() => continueToAddGCCheckout()} className='main_btn icon_btn cursor_pointer' {...(isProcessCheckout ? {disabled: 'disabled'} : {})} >{isProcessCheckout && <span className="spinner-border text-light btnLoader"></span>}Continue <span className='icon arrow_right_white'></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </Layout>
    );
};

export default AddGiftCard;
