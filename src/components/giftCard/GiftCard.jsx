import React from 'react';
import {isNumeric} from '../../utils/common-repo';


const GiftCard = (props) => {
    return (
        <div className="gift_card_preview gc_bg">
            <div className="gc_logo"><img src={props.brandLogo} className="img-fluid" alt="" /></div>
            <div className="gc_price">${isNumeric(props.amount) ? (props.amount).toFixed(2) : props.amount}</div>
        </div>
    );
};

export default GiftCard;
