import React, {useEffect} from 'react';
import Layout from './../../components/Layout';
import {Link, useNavigate} from 'react-router-dom';
import GiftCard from './GiftCard';
import GCLogo from '../../assets/images/logo_white.png';
import {useSelector, useDispatch} from 'react-redux';
import {setIsForMySelf} from '../../store/reducers/auth-reducer';

const GFDetails = (props) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const cardDetail = useSelector((state) => state.giftCard.selectedGiftCard);

    useEffect(() => {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    });
    return (
        <Layout>
            <div className='container'>
                <div className='row'>
                    <div className='col-12'>
                        <Link to='/gift_card' className='back_link icon_btn'><span className='icon arrow_left'></span> Back</Link>
                    </div>
                    <div className='col-12'>
                        <div className='d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap'>
                            <div className='main_title'>
                                <h2>Card Details</h2>
                                <p>Card Ending in {(cardDetail.sGiftCardNumber).slice(- 4)}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-lg-8'>
                        <div className='gcd_wrap'>
                            <div className='gc_info_box'>
                                <GiftCard amount={cardDetail.dblBalance} brandLogo={GCLogo} />
                                <div className='gc_info pt-3 ps-4'>
                                    <p>As of {cardDetail.dtCreated}</p>
                                    <span>Auto Reload (On)</span>
                                </div>
                                {/* <Link to='/summary' className='main_btn ms-auto'>Pay Now!</Link> */}
                            </div>
                            <div className='btn_grp mt-3'>
                                <button onClick={() => {
                                    dispatch(setIsForMySelf({isForMySelf: true}));
                                    navigate('/add-gift-card/add_value/'+ cardDetail.giftCardGuid);
                                }} className='main_btn me-2'>Add Value</button>
                                <Link to='/merge-gift-card' className='main_btn'>Merge Balance</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default GFDetails;
