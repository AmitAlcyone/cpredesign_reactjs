import React, {useEffect, useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import GiftCard from './GiftCard';
import GCLogo from '../../assets/images/logo_white.png';
import Layout from './../../components/Layout';
import {useSelector} from 'react-redux';
import {mergeGiftCard, giftCardList} from './../../services/gift-card-service';
import {toast} from 'react-toastify';
import {setGiftCards} from '../../store/reducers/gift-card-reducer';
// import {setIsLoading} from './../../store/reducers/global-reducer';
import {useDispatch} from 'react-redux';
import FullScreenLoader from '../../utils/loaders/FullScreenLoader';

const MergeGiftCard = (props) => {
    toast.configure();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const cardDetail = useSelector((state) => state.giftCard.selectedGiftCard);
    const userInfo = useSelector((state) => state.auth.userInfo);
    const giftCards = useSelector((state) => state.giftCard.giftCards);
    const [amount, setAmount] = useState(cardDetail.dblBalance);
    const [Loader, setLoader] = useState(false);

    const accessToken = userInfo.accessToken;

    const mergeCardBalance = async (param) => {
        dispatch(setGiftCards({giftCards: []}));
        // dispatch(setIsLoading({isLoading: true}));
        setAmount((amount + param.dblBalance));
        setLoader(true);
        const reqBody = {
            // eslint-disable-next-line camelcase
            fromGiftCard_Number: cardDetail.sGiftCardNumber,
            // eslint-disable-next-line camelcase
            toGiftCard_Number: param.sGiftCard_Number,
            CustomerIdentityID: userInfo.customerIdentityID,
            customerGUID: userInfo.customerGuid
        };
        const customerCode=localStorage.getItem('customerCode');
        const response = await mergeGiftCard(accessToken, reqBody, customerCode);
        if (response.status === 200) {
            // dispatch(setIsLoading(false));
            setLoader(false);
            toast.success('Card balance merged successfully.', {autoClose: 3000});
            mergeGCData(accessToken);
            return navigate('/gift_card');
        } else {
            // dispatch(setIsLoading(false));
            setLoader(false);
            toast.error('Something went wrong.', {autoClose: 3000});
        }
    };
    const mergeGCData = async (accessToken) => {
        const responce = await giftCardList(accessToken);
        if (responce.status == 200) {
            setTimeout(() => {
                dispatch(setGiftCards({giftCards: response.data}));
            }, 1000);
        }
    };

    useEffect(() => {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    });
    return (
        <>
            {Loader === true ? <FullScreenLoader /> : ''}
            <Layout>
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <Link to='/gift_card' className='back_link icon_btn'><span className='icon arrow_left'></span> Back</Link>
                        </div>
                        <div className='col-12'>
                            <div className='d-flex mb-4 mt-3 justify-content-between align-items-center  flex-wrap'>
                                <div className='main_title'>
                                    <h2>Merge Balance</h2>
                                    <p>Merge gift cards balance</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-lg-8'>
                            <div className='pay_method_list'>
                                <div className='table-responsive border-bottom'>
                                    <table className='table ccards_table  table-borderless'>
                                        <thead>
                                            <tr>
                                                <th>Gift card details</th>
                                                <th>Card Info</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {giftCards.length > 0 && giftCards.map((value, index) => {
                                                if (value.sGiftCard_Number !== cardDetail.sGiftCardNumber) {
                                                    return (
                                                        <tr key={index}>
                                                            <td><div className='gift_card_name'>${value.dblBalance} - Gift Card</div></td>
                                                            <td><div className='noc'>Ending in {value.sGiftCard_Number.substr(- 4)}</div></td>
                                                            <td><span to='#' className='view_btn' onClick={() => mergeCardBalance(value)}><span className='icon merge_icon' title="Merge Balance"></span></span></td>
                                                            {/* <td><Link to='#' className='view_btn' data-bs-toggle='modal' data-bs-target='#successModal' ><span className='icon merge_icon'></span></Link></td> */}
                                                        </tr>
                                                    );
                                                } else {
                                                    return '';
                                                }
                                            })}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div className='col-lg-4'>
                            <GiftCard amount={amount} brandLogo={GCLogo} />
                        </div>
                    </div>
                </div>
                <div className='modal fade custom_pop success_pop' id='successModal' data-bs-backdrop='static' data-bs-keyboard='false' aria-labelledby='successModalLabel' aria-hidden='true'>
                    <div className='modal-dialog  modal-dialog-centered'>
                        <div className='modal-content'>
                            <div className='modal-body'>
                                <h2>Success!</h2>
                                <p>Card Balance Merged Successfully done.</p>
                            </div>
                            <div className='modal-footer justify-content-center'>
                                <button type='button' className='main_btn' data-bs-dismiss='modal'>Okay</button>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        </>
    );
};

export default MergeGiftCard;
