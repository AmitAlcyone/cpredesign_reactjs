import Footer from './layout/BuyFooter';
import React from 'react';
import {useSelector} from 'react-redux';
import FullScreenLoader from './../utils/loaders/FullScreenLoader';
import Header from './layout/Header';

const Layout = (props) => {
    const isLoading = useSelector((state) => state.global.isLoading);
    return (
        <React.Fragment>
            {isLoading ? <FullScreenLoader /> : ''}
            <Header />
            <div className='main_wrap pt-4'>{props.children}</div>
            <Footer />
        </React.Fragment>
    );
};

export default Layout;
