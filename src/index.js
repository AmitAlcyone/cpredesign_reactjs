import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter, MemoryRouter, Route, Routes} from 'react-router-dom';
import store from './store/index';
import {Provider} from 'react-redux';
import Home from './containers/Home';
import DashboardBuy from './containers/buy-module-containers/DashboardBuy';
import TinyWalletBarcode from './containers/tiny/TinyWalletBarcode';
import ErrorPage from './containers/ErrorPage';
import BuyGiftCard from './components/buy-module-components/gift-card/BuyGiftCard';
import BuyUnlimitedPlan from './containers/buy-module-containers/BuyUnlimitedPlan';
import BuyPolicy from './components/buy-module-components/BuyPolicies';

import BuyCommonWashServices from './containers/buy-module-containers/BuyCommonWashServices';


const customerUrl = window.location.pathname.split('/').pop();
ReactDOM.render(
    <Provider store={store}>
        <React.StrictMode>
            {
                (customerUrl.length > 0 || window.location.pathname.toLowerCase()== '/buy/') ?
                    <BrowserRouter>
                        <Routes>
                            <Route path="/Wallet/Details/:id" element={<TinyWalletBarcode />} />
                            <Route path='/:id' element={<Home />} />
                            {/* <Route path='/buy/dashboard' element={<DashboardBuy />} /> */}
                            <Route path='/buy' element={<DashboardBuy />} />
                            <Route path='/buy/gift_card' element={<BuyGiftCard />} />
                            <Route path='/buy/buy_Policy' element={<BuyPolicy />} />
                            <Route path='/buy/prepaid_wash_service' element={<BuyCommonWashServices page="prepaid_service" pageTitle="Buy prepaid wash plan" pageSubTitle="Prepaid wash plan" pageHeading="Buy a prepaid wash" />} />
                            <Route path='/buy/detailing_services' element={<BuyCommonWashServices page="detail_service" pageTitle="Buy a detailing service plan" pageSubTitle="Detailing service plan" pageHeading="Buy a detailing service" />} />
                            <Route path='/buy/wash_services' element={<BuyCommonWashServices page="wash_service" pageTitle="Buy a wash service plan" pageSubTitle="Wash service plan" pageHeading="Buy a wash service" />} />
                            <Route path='/buy/unlimited_wash_club_box' element={<BuyUnlimitedPlan />} />
                            <Route path='/buy/:id' element={<Home page={'buy'} />} />
                            <Route path='*' exact={true} element={<ErrorPage />} />
                        </Routes>
                    </BrowserRouter> :
                    <MemoryRouter>
                        <App />
                    </MemoryRouter>
            }
        </React.StrictMode>
    </Provider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
