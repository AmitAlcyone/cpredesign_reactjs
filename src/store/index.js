import {configureStore} from '@reduxjs/toolkit';
import authSlice from './reducers/auth-reducer';
import giftCardSlice from './reducers/gift-card-reducer';
import customerInfoSlice from './reducers/user-info-reducer';
import prepaidWashSlice from './reducers/prepaid-wash-reducer';
import manageAccountSlice from './reducers/manage-account-reducer';
import globalSlice from './reducers/global-reducer';
import locationSlice from './reducers/location-reducer';
import walletSlice from './reducers/wallet-reducer';
import orderSlice from './reducers/order-reducer';
import buypolicyservices from './reducers/buy-reducer';
const store = configureStore({
    reducer: {
        auth: authSlice,
        giftCard: giftCardSlice,
        prepaidWash: prepaidWashSlice,
        customer: customerInfoSlice,
        manageAccount: manageAccountSlice,
        global: globalSlice,
        location: locationSlice,
        wallet: walletSlice,
        order: orderSlice,
        buypolicyservices: buypolicyservices
    }
});

export default store;
