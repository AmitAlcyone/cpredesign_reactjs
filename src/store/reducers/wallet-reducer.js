import {createSlice} from '@reduxjs/toolkit';

const walletSlice = createSlice({
    name: 'wallet',
    initialState: {
        services: [],
        selectedWallet: {}
    },
    reducers: {
        setwalletServices: (state, action) => {
            state.services = action.payload.services;
        },
        setSelectedWallet: (state, action) => {
            state.selectedWallet = action.payload.selectedWallet;
        }
    }
});
export const {setwalletServices, setSelectedWallet} = walletSlice.actions;
export default walletSlice.reducer;
