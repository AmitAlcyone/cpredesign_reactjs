import {createSlice} from '@reduxjs/toolkit';

const portalCustomizationsList = localStorage.getItem('customizePage') !== null && JSON.parse(localStorage.getItem('customizePage')).portalCustomizationsList;
const globalSlice = createSlice({
    name: 'global',
    initialState: {
        isLoading: false,
        portalCustomizationsList,
        barcodeString: '',
        buyServices: []
    },
    reducers: {
        setIsLoading: (state, action) => {
            state.isLoading = action.payload.isLoading;
        },
        setPortalCustomizationsList: (state, action) => {
            state.portalCustomizationsList = action.payload.portalCustomizationsList;
        },
        setBarcodeString: (state, action) => {
            state.barcodeString = action.payload.barcodeString;
        },
        setBuyServices: (state, action) => {
            state.buyServices = action.payload.buyServices;
        }
    }
});

export const {
    setIsLoading,
    setPortalCustomizationsList,
    setBarcodeString,
    setBuyServices
} = globalSlice.actions;
export default globalSlice.reducer;
