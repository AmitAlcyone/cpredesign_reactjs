import {createSlice} from '@reduxjs/toolkit';
const cancelSlice = createSlice({
    name: 'cancelMembership',
    initialState: {
        services: []
    },
    reducers: {
        setServices: (state, action) => {
            state.services = action.payload.services;
        }
    }
});
export const {setServices} = cancelSlice.actions;
export default cancelSlice.reducer;
