import {createSlice} from '@reduxjs/toolkit';

const prepaidWashSlice = createSlice({
    name: 'prepaidWash',
    initialState: {
        selectedPrepaidCard: {},
        services: []
    },
    reducers: {
        setSelectedPrepaidCard: (state, action) => {
            state.selectedPrepaidCard = action.payload.selectedPrepaidCard;
        },
        setServices: (state, action) => {
            state.services = action.payload.services;
        }
    }
});

export const {setSelectedPrepaidCard, setServices} = prepaidWashSlice.actions;
export default prepaidWashSlice.reducer;
