import {createSlice} from '@reduxjs/toolkit';

const orderSlice = createSlice({
    name: 'order',
    initialState: {
        orders: [],
        selectedPlan: {}
    },
    reducers: {
        setOrders: (state, action) => {
            state.orders = action.payload.orders;
        },
        setSelectedPlan: (state, action) => {
            state.selectedPlan= action.payload.selectedPlan;
        }
    }
});
export const {setOrders, setSelectedPlan} = orderSlice.actions;
export default orderSlice.reducer;
