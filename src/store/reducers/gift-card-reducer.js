import {createSlice} from '@reduxjs/toolkit';

const giftCardSlice = createSlice({
    name: 'giftCard',
    initialState: {
        giftCards: [],
        selectedGiftCard: {},
        giftCardAmounts: [],
        giftCardTabs: {
            isForMySelf: true,
            forOther: {
                FirstName: '',
                LastName: '',
                RecipientEmail: '',
                Message: ''
            }
        }
    },
    reducers: {
        setGiftCards: (state, action) => {
            state.giftCards = action.payload.giftCards;
        },
        setSelectedGiftCard: (state, action) => {
            state.selectedGiftCard = action.payload.selectedGiftCard;
        },
        setGiftCardAmounts: (state, action) => {
            state.giftCardAmounts = action.payload.giftCardAmounts;
        },
        setGiftCardTab: (state, action) => {
            state.giftCardTabs = action.payload.giftCardTabs;
        },
        setIsForMySelf: (state, action) => {
            state.giftCardTabs.isForMySelf = action.payload.isForMySelf;
            if (action.payload.isForMySelf) {
                state.giftCardTabs.forOther= {
                    FirstName: '',
                    LastName: '',
                    RecipientEmail: '',
                    Message: ''
                };
            }
        },
        setformOtherData: (state, action) => {
            state.giftCardTabs.forOther= action.payload.forOther;
        }
    }
});

export const {setGiftCards, setSelectedGiftCard, setGiftCardAmounts, setGiftCardTab, setIsForMySelf, setformOtherData} = giftCardSlice.actions;
export default giftCardSlice.reducer;
