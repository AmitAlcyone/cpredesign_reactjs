import {createSlice} from '@reduxjs/toolkit';

const customerInfo = JSON.parse(localStorage.getItem('customerInfo'));
const customerInfoSlice = createSlice({
    name: 'customer',
    initialState: {
        customersInfoByGuid: customerInfo !== null ? customerInfo : {}
    },
    reducers: {
        setCustomerInfo: (state, action) => {
            state.customersInfoByGuid = action.payload.customersInfoByGuid;
        }
    }
});

export const {setCustomerInfo, getCustomerInfo} = customerInfoSlice.actions;
export default customerInfoSlice.reducer;
