import * as Yup from 'yup';
export default Yup.object().shape({
    firstname: Yup.string().required('First Name is required.'),
    lastname: Yup.string().required('Last Name is required.'),
    email: Yup.string().email('Email must be a valid email.').required('Email is required.')
});
