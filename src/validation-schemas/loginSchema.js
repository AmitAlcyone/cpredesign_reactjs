import * as Yup from 'yup';
// const phoneRegExp = /^((\\+[1-9 ]{1,4}[ \\-]*)|(\\([0-9 ]{2,3}\\)[ \\-]*)|([0-9 ]{2,4})[ \\-]*)*?[0-9 ]{3,4}?[ \\-]*[0-9 ]{3,4}?$/;
const phoneRegExp = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;
export default Yup.object().shape({
    // phone: Yup.string().required('Phone number is required.').matches(phoneRegExp, 'Phone number is not valid.').min(10, 'Phone number must be 10 digits.').max(14, 'Phone number must be 10 digits.')
    phone: Yup.
        string()
        .test('valid', 'Phone number is not valid.', (val = '') => {
            const checkLength = val.replace(/\(|\)/g, '').length;
            if (checkLength === 0) {
                return true;
            }
            if (phoneRegExp.test(val)) {
                return true;
            }
            return false;
        })
        .test('empty', 'Phone number is required.', (val = '') => {
            const checkLength = val.replace(/\(|\)/g, '').length;
            return checkLength > 0;
        })
        .min(10, 'Phone number must be 10 digits.')
        .max(14, 'Phone number must be 10 digits.')
});
