import * as Yup from 'yup';
export default Yup.object().shape({
    barCode: Yup.string().required('Barcode or License plate # and CC* is required').max(20, 'CC Length must be 10 digits.'),
    ccNumber: Yup.string().required('CC is required.').min(4, 'CC must be 4 digits.').max(4, 'CC must be 4 digits.')
});
