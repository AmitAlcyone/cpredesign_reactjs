import * as Yup from 'yup';
const otpRegExp = /^[0-9\b]+$/;
export default Yup.object().shape({
    otp: Yup.string().required('OTP is required').matches(otpRegExp, 'OTP must be a number').min(6, 'OTP must be 6 digits').max(6, 'OTP must be 6 digits')
});
