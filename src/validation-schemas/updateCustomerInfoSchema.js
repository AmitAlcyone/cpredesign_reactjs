import * as Yup from 'yup';
// const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
// const phoneRegExp = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;
const nameRegExp = /^[a-zA-Z ]{2,40}$/;
export default Yup.object().shape({
    firstName: Yup.string().required('First name is required.').matches(nameRegExp, 'First name is not valid.').max(30, 'First Name  must have maximum 30 characters.'),
    lastName: Yup.string().required('Last name is required.').matches(nameRegExp, 'Last name is not valid.').max(30, 'Last Name  must have maximum 30 characters.'),
    email: Yup.string().email('Email must be a valid email.').required('Email is required.')
    // phoneNumber: Yup.string().required('Phone number is required.').matches(phoneRegExp, 'Phone number is not valid.').min(10, 'Phone number must be 10 digits.').max(14, 'Phone number must be 10 digits.')
});
