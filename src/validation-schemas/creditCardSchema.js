import * as Yup from 'yup';
export default Yup.object().shape({
    fName: Yup.string().required('Cardholder Name is required'),
    lName: Yup.string().required('Credit Card Number is required').max(16, 'CC must be 16 digits'),
    repEmail: Yup.string().required('Enter Expiry Date.'),
    message: Yup.string().required('Enter CVV.')
});
